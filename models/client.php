<?php

namespace model\Basics;

class Client extends \core\ModuleStandard {

    public function __construct(array $data = []) {
        parent::__construct();
        $this->data = [];
        $this->keys = ['code' => ['need' => 1], 'title' => ['need' => 0], 'description' => ['need' => 0], 'active' => ['need' => 0]];
        $this->data = (!$this->collectData($data)) ? null : $data;
    }

    public function newRole(array $data): bool {
        if (
                $data['code'] ?? null &&
                $data['title'] ?? null &&
                $data['description'] ?? null &&
                $data['active'] ?? null
        ) {
            $this->data = $data;
        } else
            $this->data = null;
        return ($this->data) ? true : false;
    }

    public function getTitle() {
        if (!$this->isValid())
            return null;
        return $this->data['title'] ?? null;
    }

    public function getCode() {
        if (!$this->isValid())
            return null;
        return $this->data['code'] ?? null;
    }

    public function getDescription() {
        if (!$this->isValid())
            return null;
        return $this->data['description'] ?? null;
    }

    public function isActive(): bool {
        if (!$this->isValid())
            return null;
        if (($this->data['active'] ?? null) && !($this->data['active'] ?? null))
            return true;
        return false;
    }

    public function isValid(): bool {
        return ($this->data) ? true : false;
    }

}
