<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;

class Report extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'report_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['report'] = [
            'fields' => [/*
              'es_1' => ['need' => 1, 'option' => null, 'type' => 'string'],
              'es_2' => ['need' => 1, 'option' => null, 'type' => 'datetime'],
              'es_3' => ['need' => 1, 'option' => null, 'type' => 'int'] */
            ],
            'fieldsAdd' => [/* 'es_1', 'es_2', 'es_3' */]
            ,
            'fieldsMod' => [/* 'es_1', 'es_2', 'es_3' */]
            ,
            'fieldsDel' => [/* 'es_1', 'es_2', 'es_3' */]
            ,
            'fieldsFilter' => [/* 'es_1', 'es_2', 'es_3' */]
        ];
    }

    public function getOrdersStatus() {
        $kField = 'report';
        $logKey = 'Report';
        $fields = [];
        $order_api = \core\Loader::loadDipendency('api', 'order', $this->Session);
        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $filters['data']['ordersstatus'] = 1;

        $data = $order_api->DB->getOrders($filters['data']);
        return $data;
    }

    public function modReport(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'report';
        $logKey = 'Report';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$data = $this->DB->getReport(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $dataDB = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        if (!$this->DB->modReport($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getReport(['id' => $id]);
    }

    public function delReport(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'report';
        $logKey = 'Report';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getReport(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modReport($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getReport(['id' => $id]);
    }

}
