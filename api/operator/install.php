<?php return array (
  'app_Operators' => 
  array (
    'fields' => 
    array (
      0 => 
      array (
        'Field' => 'id',
        'Type' => 'int(10) unsigned',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => 'auto_increment',
      ),
      1 => 
      array (
        'Field' => 'idUser',
        'Type' => 'int(10) unsigned',
        'Null' => 'NO',
        'Key' => 'MUL',
        'Default' => NULL,
        'Extra' => '',
      ),
      2 => 
      array (
        'Field' => 'denomination',
        'Type' => 'varchar(250)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      3 => 
      array (
        'Field' => 'address',
        'Type' => 'varchar(250)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      4 => 
      array (
        'Field' => 'cap',
        'Type' => 'varchar(7)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      5 => 
      array (
        'Field' => 'city',
        'Type' => 'varchar(25)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      6 => 
      array (
        'Field' => 'region',
        'Type' => 'varchar(5)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      7 => 
      array (
        'Field' => 'fiscalCode',
        'Type' => 'varchar(16)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      8 => 
      array (
        'Field' => 'ivaCode',
        'Type' => 'varchar(11)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      9 => 
      array (
        'Field' => 'phone1',
        'Type' => 'varchar(18)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      10 => 
      array (
        'Field' => 'phone2',
        'Type' => 'varchar(18)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      11 => 
      array (
        'Field' => 'phone3',
        'Type' => 'varchar(18)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      12 => 
      array (
        'Field' => 'fax',
        'Type' => 'varchar(18)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      13 => 
      array (
        'Field' => 'fax2',
        'Type' => 'varchar(18)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      14 => 
      array (
        'Field' => 'email',
        'Type' => 'varchar(100)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      15 => 
      array (
        'Field' => 'pec',
        'Type' => 'varchar(100)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      16 => 
      array (
        'Field' => 'note',
        'Type' => 'text',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      17 => 
      array (
        'Field' => 'dateInsert',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      18 => 
      array (
        'Field' => 'dateUpdate',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      19 => 
      array (
        'Field' => 'dateDelete',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      20 => 
      array (
        'Field' => 'deleted',
        'Type' => 'tinyint(1)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => '0',
        'Extra' => '',
      ),
    ),
    'indexes' => 
    array (
      0 => 
      array (
        'Table' => 'app_Operators',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '1',
        'Column_name' => 'id',
        'Collation' => 'A',
        'Cardinality' => '7',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      1 => 
      array (
        'Table' => 'app_Operators',
        'Non_unique' => '1',
        'Key_name' => 'idUser_idx',
        'Seq_in_index' => '1',
        'Column_name' => 'idUser',
        'Collation' => 'A',
        'Cardinality' => '7',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
    ),
    'references' => 
    array (
      0 => 
      array (
        'CONSTRAINT_CATALOG' => 'def',
        'CONSTRAINT_SCHEMA' => '_DB_NAME',
        'CONSTRAINT_NAME' => 'app_Operators_ibfk_1',
        'TABLE_CATALOG' => 'def',
        'TABLE_SCHEMA' => '_DB_NAME',
        'TABLE_NAME' => 'app_Operators',
        'COLUMN_NAME' => 'idUser',
        'ORDINAL_POSITION' => '1',
        'POSITION_IN_UNIQUE_CONSTRAINT' => '1',
        'REFERENCED_TABLE_SCHEMA' => '_DB_NAME',
        'REFERENCED_TABLE_NAME' => 'vks_Users',
        'REFERENCED_COLUMN_NAME' => 'id',
      ),
    ),
  ),
);