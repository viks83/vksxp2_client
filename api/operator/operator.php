<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;

class Operator extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'operator_001', 'dipendency' => [], 'error_encript' => true];
    private $defaults_api = null;

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['operator'] = [
            'fieldsuser' => [
                'user' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'password' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'passwordrepeat' => ['need' => 1, 'option' => null, 'type' => 'string'],
            ],
            'fields' => [
                'idUser' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'denomination' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'address' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'cap' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'city' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'region' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fiscalCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'ivaCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone1' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone2' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone3' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fax' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fax2' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'email' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'pec' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'note' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'comunication' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'telegramId' => ['need' => 0, 'option' => null, 'type' => 'string'],
            ],
            'fieldsAdd' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'note', 'comunication', 'telegramId', 'telegramChatId']
            ,
            'fieldsAddUser' => ['user', 'password', 'passwordrepeat']
            ,
            'fieldsModPassword' => ['password', 'passwordrepeat']
            ,
            'fieldsMod' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'note', 'comunication', 'dateUpdate']
            ,
            'fieldsDel' => []
            ,
            'fieldsFilter' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'note']
        ];
        $this->defaults_api = \core\Loader::loadDipendency('api', 'defaults', $this->Session);
    }

    public function addOperator() {
        $fields = [];
        $kField = 'operator';
        $logKey = 'Operator';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }


        /* Check User Name e Password */
        foreach ($this->Settings['data'][$kField]['fieldsAddUser'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fieldsuser'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $dataUser = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        if ($dataUser['data']['password'] != $dataUser['data']['passwordrepeat']) {
            $this->setError('fatal', 'error_password;');
            return false;
        }
        if (strlen($dataUser['data']['user']) < 4) {
            $this->setError('fatal', 'error_user;');
            return false;
        }

        if ($this->Session->Acl->isUserNameAlreadyExists($dataUser['data']['user'])) {
            $this->setError('fatal', 'error_user_exists;');
            return false;
        }

        $user = new \model\Acl\User();
        $user->newUser(['user' => $dataUser['data']['user'], 'password' => $dataUser['data']['password'], 'active' => 1, 'name' => $data['data']['denomination']]);
        $user->addRole(new \model\Acl\Role($this->Session->Acl->getRole($this->defaults_api->getValue('roles', 'operator'))));
        if (!$data['data']['idUser'] = $this->Session->Acl->addUser($user)) {
            $this->setError('fatal', 'error_user_create;');
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;
        $data['data']['comunication'] = $data['data']['comunication'] ?? '000'; //email Telegram Notice
        $data['data']['telegramId'] = substr(md5(microtime() . 'condom'), 0, 5);

        if (!$id = $this->DB->addOperator($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->Session->Acl->delUser($data['data']['idUser']);
            $this->setError('fatal', 'error_insert;');
            return false;
        }

        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($data['data'], ['new_password' => $dataUser['data']['password'], 'new_user' => $dataUser['data']['user']]);
        $comunication_api->sendByTrigger('new.user.inserted', $data, [$this->defaults_api->getValue('roles', 'operator') => [$data['data']['idUser']]]);

        return ['id' => $id];
    }

    public function getOperators() {
        $kField = 'operator';
        $logKey = 'Operator';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $data = $this->DB->getOperators($filters['data']);
        return $data;
    }

    public function modOperator(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'operator';
        $logKey = 'Operator';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$operator = $this->DB->getOperator(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if ($data['error'] && count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['comunication'] = $data['data']['comunication'] ?? '000'; //email Telegram Notice
        if (!$this->DB->modOperator($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);


        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($data['data'], []);
        $comunication_api->sendByTrigger('mod.user.data', $data, [$this->defaults_api->getValue('roles', 'operator') => [$operator['idUser']]]);

        return $this->DB->getOperator(['id' => $id]);
    }

    public function delOperator(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'operator';
        $logKey = 'Operator';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getOperator(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modOperator($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getOperator(['id' => $id]);
    }

    public function getOperator(array $filter = []) {
        $kField = 'operator';
        $logKey = 'Operator';

        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($filter['idUser'] ?? null)
            $filter['idUser'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else
            return [];
        $filter['deleted'] = 0;
        $filter['withUser'] = 1;
        $data = $this->DB->getOperator($filter);
        return $data;
    }

    public function modPasswordOperator() {
        $fields = [];
        $kField = 'operator';
        $logKey = 'OperatorModPassword';
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('o_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }

        /* Check User Name e Password */
        foreach ($this->Settings['data'][$kField]['fieldsModPassword'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fieldsuser'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'o_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }



        if ($data['data']['password'] != $data['data']['passwordrepeat']) {
            $this->setError('fatal', 'error_password;');
            return false;
        }

        if (!$operator = $this->getOperator(['id' => $id])) {
            $this->setError('fatal', 'error_operator;');
            return false;
        }

        if (!$this->Session->Acl->modPassword($data['data']['password'], $operator['idUser'])) {
            $this->setError('fatal', 'error_modify;');
            return false;
        }

        $this->Session->Event->add("Mod $logKey OK", 'log', 3);


        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($data['data'], ['new_password' => $data['data']['password'], 'new_user' => $data['data']['user']]);
        $comunication_api->sendByTrigger('mod.user.password', $data, [$this->defaults_api->getValue('roles', 'operator') => [$operator['idUser']]]);

        return true;
    }

}
