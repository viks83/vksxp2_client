<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è passibile per legge.
 */

namespace api\tokens;

class Tokens extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getTokens(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $fields[] = " `T`.*";
        if ($filter['ip'] ?? null)
            $outFilter['ip'] = '`T`.`ip` = :ip';
        if ($filter['tkn'] ?? null)
            $outFilter['tkn'] = '`T`.`tkn` = :tkn';
        if ($filter['idUser'] ?? null)
            $outFilter['idUser'] = '`T`.`idUser` = :idUser';

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';

        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Extend_Users_Tokens` `T` $joinFilter $outFilter", $filter))
            return [];
        return $dato;
    }

    public function getToken(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getTokens($filter))
            return $data[0];
        return null;
    }

    public function addToken(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("REPLACE INTO `app_Extend_Users_Tokens` ($parameters[0]) VALUES ($parameters[1])", $data, 1, 1);
    }

    public function install(): bool {
        return parent::_install(APP . 'api/tokens/');
    }

    public function export(): bool {
        return parent::_export(['app_Extend_Users_Tokens' => []], APP . 'api/tokens/');
    }

    public function update(): bool {
        return parent::_update(APP . 'api/tokens/');
    }

}
