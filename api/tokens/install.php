<?php return array (
  'app_Extend_Users_Tokens' => 
  array (
    'fields' => 
    array (
      0 => 
      array (
        'Field' => 'idUser',
        'Type' => 'int(10) unsigned',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => '',
      ),
      1 => 
      array (
        'Field' => 'tkn',
        'Type' => 'varchar(30)',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => '',
      ),
      2 => 
      array (
        'Field' => 'ip',
        'Type' => 'varchar(250)',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => '',
      ),
      3 => 
      array (
        'Field' => 'status',
        'Type' => 'varchar(50)',
        'Null' => 'NO',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
    ),
    'indexes' => 
    array (
      0 => 
      array (
        'Table' => 'app_Extend_Users_Tokens',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '1',
        'Column_name' => 'idUser',
        'Collation' => 'A',
        'Cardinality' => '0',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      1 => 
      array (
        'Table' => 'app_Extend_Users_Tokens',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '2',
        'Column_name' => 'tkn',
        'Collation' => 'A',
        'Cardinality' => '0',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      2 => 
      array (
        'Table' => 'app_Extend_Users_Tokens',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '3',
        'Column_name' => 'ip',
        'Collation' => 'A',
        'Cardinality' => '0',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
    ),
    'references' => false,
  ),
);