<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è passibile per legge.
 */

namespace api;

class Tokens extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'tokens_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['token'] = [
            'fields' => [
                'idUser' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'tkn' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'ip' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'status' => ['need' => 1, 'option' => null, 'type' => 'string'],
            ],
            'fieldsAdd' => ['idUser', 'tkn', 'ip', 'status']
            ,
            'fieldsMod' => ['status']
        ];
    }

    public function getStatus() {
        $filter = ['idUser' => $this->Session->Acl->User['id'], 'tkn' => $this->Session->Acl->User['tkntmp'], 'ip' => $this->Session->Dataservice->g('REMOTE_ADDR', 'SERVER', 'string')];
        if ($data = $this->DB->getToken($filter))
            return $data['status'];
        return false;
    }

    public function getUser() {
        if ($this->Session->Dataservice->getError($tkn = $this->Session->Dataservice->g('t', '', 'string')) || $this->Session->Dataservice->getError($ip = $this->Session->Dataservice->g('i', '', 'string')))
            return null;
        if (!$user = $this->Session->Acl->DB->getUserFromTemporaryToken(['tkn' => $tkn, 'ip' => $ip]))
            return null;
        if (!$user['permission'] = $this->Session->Acl->DB->getUserPermissions($user[0]))
            return null;
        return $user;
    }

    public function getPreferences() {
        return ['theme' => (($this->getStatus()) ? 'theme-red' : 'theme-green')];
    }

    public function setStatus($status) {
        if (!$this->DB->addToken(['idUser' => $this->Session->Acl->User['id'], 'tkn' => $this->Session->Acl->User['tkntmp'], 'ip' => $this->Session->Dataservice->g('REMOTE_ADDR', 'SERVER', 'string'), 'status' => $status], $this->Settings['data']['token']['fields'])) {
            return false;
        }
        return true;
    }

    public function delToken($id, $tkn) {
        return true;
        if (!$this->DB->delToken($id, $tkn, $this->Session->Dataservice->g('REMOTE_ADDR', 'SERVER', 'string'))) {
            return false;
        }
        return true;
    }

}
