<?php return array (
  'app_Dictionary' => 
  array (
    'fields' => 
    array (
      0 => 
      array (
        'Field' => 'id',
        'Type' => 'int(10) unsigned',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => 'auto_increment',
      ),
      1 => 
      array (
        'Field' => 'type',
        'Type' => 'varchar(15)',
        'Null' => 'NO',
        'Key' => 'MUL',
        'Default' => NULL,
        'Extra' => '',
      ),
      2 => 
      array (
        'Field' => 'code',
        'Type' => 'varchar(15)',
        'Null' => 'NO',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      3 => 
      array (
        'Field' => 'title',
        'Type' => 'varchar(255)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      4 => 
      array (
        'Field' => 'value',
        'Type' => 'int(11)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
    ),
    'indexes' => 
    array (
      0 => 
      array (
        'Table' => 'app_Dictionary',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '1',
        'Column_name' => 'id',
        'Collation' => 'A',
        'Cardinality' => '71',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      1 => 
      array (
        'Table' => 'app_Dictionary',
        'Non_unique' => '0',
        'Key_name' => 'UNIQUE',
        'Seq_in_index' => '1',
        'Column_name' => 'type',
        'Collation' => 'A',
        'Cardinality' => '35',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      2 => 
      array (
        'Table' => 'app_Dictionary',
        'Non_unique' => '0',
        'Key_name' => 'UNIQUE',
        'Seq_in_index' => '2',
        'Column_name' => 'code',
        'Collation' => 'A',
        'Cardinality' => '71',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
    ),
    'references' => false,
  ),
);