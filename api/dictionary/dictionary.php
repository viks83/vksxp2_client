<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;

class Dictionary extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'dictionary_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['dictionary'] = [
            'fields' => [
                'type' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'code' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'title' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'value' => ['need' => 1, 'option' => null, 'type' => 'int']
            ],
            'fieldsAdd' => ['type', 'code', 'title', 'value']
            ,
            'fieldsMod' => ['title', 'value']
            ,
            'fieldsDel' => ['title', 'value']
            ,
            'fieldsFilter' => ['type', 'code', 'title', 'value']
        ];
    }

    public function getNewValue($type) {
        if ($data = $this->DB->getDictionary(['lastOfType' => 1, 'type' => $type])) {
            return $data['value'] + 1;
        }
        return 1;
    }

    public function addDictionary($data = null) {
        $fields = [];
        $kField = 'dictionary';
        $logKey = 'Dictionary';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        if ($data)
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        else
            $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        if (!$id = $this->DB->addDictionary($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }
        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getDictionary(array $filter = []) {
        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        elseif (
                ($type = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('d_type', 'GET', 'string')) ? null : $data) &&
                ($code = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('d_code', 'GET', 'string')) ? null : $data)
        ) {
            $filter['type'] = $type;
            $filter['code'] = $code;
        } elseif (
                ($filter['type'] ?? null) &&
                ($filter['code'] ?? null)
        ) {
            
        } else
            return [];
        $filters['deleted'] = 0;
        $data = $this->DB->getDictionary($filter);
        return $data;
    }

    public function getDictionarys($data = false) {
        $kField = 'dictionary';
        $logKey = 'Dictionary';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        if ($data)
            $filters = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        else
            $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $data = $this->DB->getDictionarys($filters['data']);
        return $data;
    }

    public function modDictionary(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'dictionary';
        $logKey = 'Dictionary';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$data = $this->DB->getDictionary(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $dataDB = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        if (!$this->DB->modDictionary($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getDictionary(['id' => $id]);
    }

    public function _delDictionary(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'dictionary';
        $logKey = 'Dictionary';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getDictionary(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modDictionary($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getDictionary(['id' => $id]);
    }

    public function findInDictionary($dictionary, $type, $code) {
        if (!$dictionary) {
            $dictionary = $this->DB->getDictionarys(['type' => $type]);
        }
        if (!$dictionary)
            return false;
        foreach ($dictionary as $k => $v) {
            if (strtolower($v['type']) == strtolower($type) && strtolower($v['code']) == strtolower($code))
                return $v;
        }
        return false;
    }

}
