<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\dictionary;

class Dictionary extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getDictionarys(array $filter): array {
        $outFilter = $joinFilter = $fields = $order = [];
        $fields[] = " `C`.*";
        foreach (['id', 'type', 'code', 'title', 'value'] as $f)
            if ($filter[$f] ?? null)
                $outFilter[$f] = "`C`.`$f` = :$f";
            else
                unset($filter[$f]);
        if (!array_key_exists('order', $filter)) {
            $order[] = '`C`.`value` ASC';
        }
        if ($filter['lastOfType'] ?? null) {
            $order[] = '`C`.`value` DESC';
            unset($filter['lastOfType']);
        }
        if ($filter['typeIn'] ?? null) {
            $order[] = '`C`.`type` IN (\'' . implode('\',\'', $filter['typeIn']) . '\')';
            unset($filter['typeIn']);
        }
        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $order = (count($order)) ? ' ORDER BY ' . implode(', ', $order) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';
        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Dictionary` `C` $joinFilter $outFilter $order", $filter))
            return [];
        return $dato;
    }

    public function getDictionary(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getDictionarys($filter))
            return $data[0];
        return null;
    }

    public function addDictionary(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("REPLACE INTO `app_Dictionary` ($parameters[0]) VALUES ($parameters[1])", $data, 1);
    }

    public function modDictionary(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Dictionary` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        return true;
        return parent::_install(APP . 'api/dictionary/');
    }

    public function export(): bool {

        return parent::_export(['app_Dictionary' => []], APP . 'api/dictionary/');
    }

    public function update(): bool {
        // remove it after create db
        return true;
        return parent::_update(APP . 'api/dictionary/');
    }

}
