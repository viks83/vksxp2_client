<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
use \core\Files;

class Provider extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'provider_001', 'dipendency' => [], 'error_encript' => true];
    public $importFields = [1 => "type", "denomination", "surname", "name", "fiscalCode", "ivaCode", "address", "sector", "contact"];
    private $defaults_api = null;

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['provider'] = [
            'fieldsuser' => [
                'user' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'password' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'passwordrepeat' => ['need' => 1, 'option' => null, 'type' => 'string'],
            ],
            'fields' => [
                'idUser' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'denomination' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'referent' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'address' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'cap' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'city' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'region' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fiscalCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'ivaCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone1' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone2' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone3' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fax' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fax2' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'email' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'pec' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'note' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'string']
            ],
            'fieldsAdd' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'referent', 'note']
            ,
            'fieldsAddUser' => ['user', 'password', 'passwordrepeat']
            ,
            'fieldsMod' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'referent', 'note']
            ,
            'fieldsModPassword' => ['password', 'passwordrepeat']
            ,
            'fieldsDel' => []
            ,
            'fieldsFilter' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'referent', 'note']
        ];
        $this->defaults_api = \core\Loader::loadDipendency('api', 'defaults', $this->Session);
    }

    public function addProvider() {
        $fields = [];
        $kField = 'provider';
        $logKey = 'Provider';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }


        /* Check User Name e Password */
        foreach ($this->Settings['data'][$kField]['fieldsAddUser'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fieldsuser'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $dataUser = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        if ($dataUser['data']['password'] != $dataUser['data']['passwordrepeat']) {
            $this->setError('fatal', 'error_password;');
            return false;
        }
        if (strlen($dataUser['data']['user']) < 4) {
            $this->setError('fatal', 'error_user;');
            return false;
        }

        if ($this->Session->Acl->isUserNameAlreadyExists($dataUser['data']['user'])) {
            $this->setError('fatal', 'error_user_exists;');
            return false;
        }


        $user = new \model\Acl\User();
        $user->newUser(['user' => $dataUser['data']['user'], 'password' => $dataUser['data']['password'], 'active' => 1, 'name' => $data['data']['denomination']]);
        $user->addRole(new \model\Acl\Role($this->Session->Acl->getRole($this->defaults_api->getValue('roles', 'provider'))));
        if (!$data['data']['idUser'] = $this->Session->Acl->addUser($user)) {
            $this->setError('fatal', 'error_user_create;');
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;



        if (!$id = $this->DB->addProvider($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->Session->Acl->delUser($data['data']['idUser']);
            $this->setError('fatal', 'error_insert;');
            return false;
        }

        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getProviders() {
        $kField = 'provider';
        $logKey = 'Provider';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $data = $this->DB->getProviders($filters['data']);
        return $data;
    }

    public function modProvider(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'provider';
        $logKey = 'Provider';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$data = $this->DB->getProvider(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $dataDB = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        if (!$this->DB->modProvider($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getProvider(['id' => $id]);
    }

    public function delProvider(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'provider';
        $logKey = 'Provider';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getProvider(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modProvider($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getProvider(['id' => $id]);
    }

    public function getProvider(array $filter = []) {
        $kField = 'provider';
        $logKey = 'Provider';

        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($filter['idUser'] ?? null)
            $filter['idUser'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else
            return [];
        $filters['deleted'] = 0;
        $data = $this->DB->getProvider($filter);
        return $data;
    }

    public function importProviders() {
        if (!$importFile = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_file', 'FILE', 'file')) ? null : $data) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $kField = 'provider';
        $logKey = 'Provider';
        $uploadedFileName = Files::upload($importFile, ['folder' => 'upload/import/', 'staticName' => 'import_prov_' . date("d_m_Y_H_i_s", time())], ['extensionEnabled' => ['xls']]);

        if ($error = Files::getError($uploadedFileName)) {
            $this->setError('fatal', "error_upload;$error");
            return false;
        }
        require_once APP . 'vendors/Excel/oleread.php';
        require_once APP . 'vendors/Excel/reader.php';
        $data = new \Spreadsheet_Excel_Reader();
        $data->read($uploadedFileName);
        if (!$rows = $data->sheets[0]['numRows']) {
            $this->setError('fatal', "no_data;");
            return false;
        }

        $dataOut = [];
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $tmp = [];
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                $tmp[$this->importFields[$j]] = trim(utf8_encode($data->sheets[0]['cells'][$i][$j]));
            }
            $dataOut[] = $tmp;
        }
        $newOut = [];
        foreach ($dataOut as $v) {
            $new = ['denomination' => $v['denomination'], 'referent' => $v['surname'] . ' ' . $v['name'], 'fiscalCode' => $v['fiscalCode'], 'ivaCode' => $v['ivaCode'], 'note' => "Tipo: {$v['type']}, Settore: {$v['sector']}"];
            $address = explode(',', $v['address']);
            $cap = explode(' ', $address[1]);
            foreach ($cap as $kk => $vv) {
                if ($vv > 100) {
                    unset($cap[$kk]);
                    $new['cap'] = $vv;
                }
            }
            $new['city'] = trim(implode(' ', $cap));
            if (strlen($new['city']) < 4)
                $new['city'] = '';
            $new['address'] = $address[0];

            $tel = [];
            $cell = [];
            $fax = [];
            $email = [];
            if ($contacts = explode("\n", $v['contact']))
                foreach ($contacts as $vc) {
                    if (strpos($vc, 'Tel.:') === 0)
                        $tel[] = trim(str_replace('Tel.:', '', $vc));
                    if (strpos($vc, 'Email:') === 0)
                        $email[] = trim(str_replace('Email:', '', $vc));
                    if (strpos($vc, 'Cell.:') === 0)
                        $cell[] = trim(str_replace('Cell.:', '', $vc));
                    if (strpos($vc, 'Fax.:') === 0)
                        $fax[] = trim(str_replace('Fax.:', '', $vc));
                }
            if (count($tel)) {
                foreach ($tel as $ktel => $vtel) {
                    if ($ktel > 2) {
                        // $new['note'] .= "\n tel: $vtel";
                    } else
                        $new['phone' . ($ktel + 1)] = $vtel;
                }
            }
            if (count($cell))
                foreach ($cell as $ktel => $vtel)
                    $new['note'] .= "\n tel: $vtel";
            if (count($email)) {
                foreach ($email as $kemail => $vemail) {
                    if ($kemail > 0) {
                        // $new['note'] .= "\n email: $vemail";
                    } else
                        $new['email'] = $vemail;
                }
            }
            if (count($fax)) {
                $new['fax'] = $fax[0] ?? '';
                $new['fax2'] = $fax[1] ?? '';
            }
            $new['note'] .= $v['contact'];

            $newOut[] = $new;
        }



        $providers = $this->DB->getProviders([]);

        function findProvider($provider, $providers) {
            foreach ($providers as $k => $v) {
                if ($provider['ivaCode'] && $provider['ivaCode'] == $v['ivaCode']) {
                    error_log('same iva');
                    return $v;
                }
                if ($provider['fiscalCode'] && strtolower($provider['fiscalCode']) == strtolower($v['fiscalCode'])) {
                    error_log('same fiscal');

                    return $v;
                }
                if (strtolower($provider['denomination']) == strtolower($v['denomination'])) {
                    error_log('same denomination');

                    return $v;
                }
            }
            return false;
        }

        $now = new \DateTime();
        foreach ($newOut as $v) {
            if (!$find = findProvider($v, $providers)) {
                // create user
                $userName = ($v['email']) ? $v['email'] : null;
                if (!$userName)
                    $userName = (trim($v['fiscalCode'])) ? trim($v['fiscalCode']) : null;
                if (!$userName)
                    $userName = (trim($v['ivaCode'])) ? trim($v['ivaCode']) : null;
                if (!$userName)
                    $userName = (trim($v['denomination'])) ? trim($v['denomination']) : null;
                if (!$userName)
                    $userName = (trim($v['phone1'])) ? trim($v['phone1']) : null;
                if (!$userName)
                    $userName = md5(microtime());


                if ($this->Session->Acl->isUserNameAlreadyExists($userName)) {
                    //$this->setError('fatal', 'error_user_exists;');
                    //return false;
                    $userName = md5(microtime() . rand(0, 10000000) . rand(1, 9999));
                }

                $user = new \model\Acl\User();
                $user->newUser(['user' => $userName, 'password' => '123456789', 'active' => 1, 'name' => $v['denomination'] ?? $v['referent']]);
                $user->addRole(new \model\Acl\Role($this->Session->Acl->getRole($this->defaults_api->getValue('roles', 'provider'))));
                if (!$v['idUser'] = $this->Session->Acl->addUser($user)) {
                    $this->setError('fatal', 'error_user_create;');
                    return false;
                }


                $v['dateInsert'] = $now->format('Y-m-d H:i:s');
                $v['dateUpdate'] = $now->format('Y-m-d H:i:s');
                $v['deleted'] = 0;
            } else {
                error_log('trovato' . print_r($find, 1));
                $v['idUser'] = $find['idUser'];
            }
            $v['dateUpdate'] = $now->format('Y-m-d H:i:s');
            if (!$this->DB->addProvider($v, $this->Settings['data'][$kField]['fields'])) {
                $this->setError('fatal', "db_error;" . print_r($v, 1));
                return false;
            }
        }
        return $newOut;
    }

    public function modPasswordProvider() {
        $fields = [];
        $kField = 'provider';
        $logKey = 'ProviderModPassword';
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('p_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }

        /* Check User Name e Password */
        foreach ($this->Settings['data'][$kField]['fieldsModPassword'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fieldsuser'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'p_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }



        if ($data['data']['password'] != $data['data']['passwordrepeat']) {
            $this->setError('fatal', 'error_password;');
            return false;
        }

        if (!$provider = $this->getProvider(['id' => $id])) {
            $this->setError('fatal', 'error_provider;');
            return false;
        }

        if (!$this->Session->Acl->modPassword($data['data']['password'], $provider['idUser'])) {
            $this->setError('fatal', 'error_modify;');
            return false;
        }

        $this->Session->Event->add("Mod $logKey OK", 'log', 3);


        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($data['data'], ['new_password' => $data['data']['password'], 'new_user' => $data['data']['user']]);
        $comunication_api->sendByTrigger('mod.user.password', $data, [$this->defaults_api->getValue('roles', 'provider') => [$provider ['idUser']]]);

        return true;
    }

}
