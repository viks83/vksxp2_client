<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\provider;

class Provider extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getProviders(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $indexBy = null;
        $fields[] = " `C`.*";
        foreach (['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'referent', 'note'] as $f)
            if ($filter[$f] ?? null)
                $outFilter[$f] = "`C`.`$f` LIKE :$f";
            else
                unset($filter[$f]);
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['idUser'] ?? null)
            $outFilter['idUser'] = "`C`.`idUser` = :idUser";
        if ($filter['ids'] ?? null) {
            $outFilter['ids'] = "`C`.`id` IN ({$filter['ids']})";
            unset($filter['ids']);
        }
        if ($filter['idUsers'] ?? null) {
            $outFilter['ids'] = "`C`.`idUser` IN ({$filter['idUsers']})";
        }
        unset($filter['idUsers']);
        if ($filter['indexBy'] ?? null) {
            $indexBy = $filter['indexBy'];
            unset($filter['indexBy']);
        }

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';
        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Providers` `C` $joinFilter $outFilter", $filter, false, $indexBy))
            return [];
        return $dato;
    }

    public function getProvider(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getProviders($filter))
            return $data[0];
        return null;
    }

    public function addProvider(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Providers` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1);
    }

    public function modProvider(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Providers` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        
        return parent::_install(APP . 'api/provider/');
    }

    public function export(): bool {
        return parent::_export(['app_Providers' => []], APP . 'api/provider/');
    }

    public function update(): bool {
        // remove it after create db
        return true;
        return parent::_update(APP . 'api/provider/');
    }

}
