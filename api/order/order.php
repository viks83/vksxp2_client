<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

ini_set('max_execution_time', 0);

// External Library 
// use \core\Files;

class Order extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'order_001', 'dipendency' => [], 'error_encript' => true];
    private $defaults_api = null;

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['uploadFolder'] = 'upload/orders/';
        $this->Settings['data']['order'] = [
            'fields' => [
                'idOccupant' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'idCondominium' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idUser' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'idCategory' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idSubCategory' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'idStatus' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idPriority' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idActivity' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idType2' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idType3' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'referent' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateWorksStart' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateWorksEnd' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'code' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'description' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'title' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'note' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateRemind' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'default' => 0],
                'quotation' => ['need' => 0, 'option' => null, 'type' => 'float'],
                'extimate' => ['need' => 0, 'option' => null, 'type' => 'float'],
                'clientCost' => ['need' => 0, 'option' => null, 'type' => 'float'],
                'dateInsertFrom' => ['need' => 0, 'option' => null, 'type' => 'date'],
                'dateInsertTo' => ['need' => 0, 'option' => null, 'type' => 'date'],
                'idGenericUser' => ['need' => 0, 'option' => null, 'type' => 'int'],
            ],
            'fieldsAdd' => ['idOccupant', 'idCondominium', 'idUser', 'idCategory', 'idSubCategory', 'idStatus', 'idPriority', 'idActivity', 'idType2', 'idType3', 'referent', 'dateInsert', 'dateUpdate', 'dateDelete', 'code', 'description', 'title', 'note', 'dateRemind', 'deleted', 'quotation', 'extimate', 'clientCost']
            ,
            'fieldsMod' => ['idOccupant', 'idCondominium', 'idUser', 'idCategory', 'idSubCategory', 'idStatus', 'idPriority', 'idActivity', 'idType2', 'idType3', 'referent', 'dateInsert', 'dateUpdate', 'dateDelete', 'code', 'description', 'title', 'note', 'dateRemind', 'deleted', 'quotation', 'extimate', 'clientCost', 'dateWorksStart', 'dateWorksEnd']
            ,
            'fieldsDel' => ['deleted']
            ,
            'fieldsFilter' => ['idOccupant', 'idCondominium', 'idUser', 'idCategory', 'idSubCategory', 'idStatus', 'idPriority', 'idActivity', 'idType2', 'idType3', 'referent', 'code', 'description', 'title', 'note', 'dateInsertFrom', 'dateInsertTo', 'idGenericUser']
        ];
        $this->Settings['data']['orderjob'] = [
            'fields' => [
                'idOrder' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idSender' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'idDestination' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idOrderJob' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'senderRole' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'destinationRole' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idType' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'description' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'title' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateAssignment' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateLimit' => ['need' => 0, 'option' => null, 'type' => 'date'],
                'dateClose' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'boolean'],
                'accepted' => ['need' => 0, 'option' => null, 'type' => 'boolean'],
                'value' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'idReferer' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'refererRole' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'idActiveUsers' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateInsertFrom' => ['need' => 0, 'option' => null, 'type' => 'date'],
                'dateInsertTo' => ['need' => 0, 'option' => null, 'type' => 'date'],
                'idGenericUser' => ['need' => 0, 'option' => null, 'type' => 'int'],
            ],
            'fieldsAdd' => ['idOrder', 'idSender', 'senderRole', 'idDestination', 'destinationRole', 'idOrderJob', 'idType', 'title', 'description', 'dateInsert', 'dateUpdate', 'dateDelete', 'dateAssignment', 'deleted', 'value', 'refererRole', 'idReferer', 'dateLimit', 'dateClose']
            ,
            'fieldsMod' => ['idOrder', 'idSender', 'senderRole', 'idDestination', 'destinationRole', 'idOrderJob', 'idType', 'title', 'description', 'dateInsert', 'dateUpdate', 'dateDelete', 'dateAssignment', 'deleted', 'accepted', 'value', 'dateLimit', 'dateClose']
            ,
            'fieldsDel' => ['deleted']
            ,
            'fieldsFilter' => ['idOrder', 'idSender', 'senderRole', 'idDestination', 'destinationRole', 'idOrderJob', 'idType', 'title', 'description', 'dateInsert', 'dateUpdate', 'dateDelete', 'dateAssignment', 'dateInsertFrom', 'dateInsertTo', 'idGenericUser']
        ];
        $this->Settings['data']['orderjobmoduledecline'] = [
            'fields' => ['note' => ['need' => 0, 'option' => null, 'type' => 'string']]
        ];
        $this->Settings['data']['orderjobmodulefiles'] = [
            'fields' => [
                'note' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'type' => ['need' => 1, 'option' => null, 'type' => 'string']
            ]
        ];
        $this->Settings['data']['orderjobmodulecorrection'] = [
            'fields' => [
                'note' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'type' => ['need' => 1, 'option' => null, 'type' => 'string']
            ]
        ];
        $this->Settings['data']['orderjobmodulecompleted'] = [
            'fields' => [
                'note' => ['need' => 1, 'option' => null, 'type' => 'string'],
            ]
        ];
        $this->Settings['data']['orderjobmodulestasclose'] = [
            'fields' => [
                'type' => ['need' => 1, 'option' => null, 'type' => 'string']
            ]
        ];
        $this->Settings['data']['orderjobmodule'] = [
            'fields' => [
                'sin-bagn-1' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto danno da bagnamento da sopralluogo dopo la chiamata dei condòmini', 'check' => 1],
                'sin-bagn-2' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto durante intervento ricerca guasti', 'check' => 1],
                'sin-bagn-3' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto ripristino danno bagnamento', 'check' => 1],
                'sin-bagn-4' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Nota spese intervento (RG + Tinteggiatura)', 'check' => 1],
                'sin-elet-1' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto centralina/motore vecchio e nuovo', 'check' => 1],
                'sin-elet-2' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Nota spese intervento', 'check' => 1],
                'sin-atmo-1' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto copertura con tegole mosse', 'check' => 1],
                'sin-atmo-2' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto dopo intervento', 'check' => 1],
                'sin-atmo-3' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto bagnamento (se infiltrazione in alloggio sottostante)', 'check' => 1],
                'sin-atmo-4' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Nota spese intervento', 'check' => 1],
                'no-sin-1' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto prima dell\'inizio dei lavori', 'check' => 1],
                'no-sin-2' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto durante i lavori', 'check' => 1],
                'no-sin-3' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Foto dopo la fine dei lavori', 'check' => 1],
                'no-sin-4' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'text' => 'Nota spese intervento', 'check' => 1],
                'note' => ['need' => 0, 'option' => null, 'type' => 'string', 'text' => 'Note aggiuntive', 'check' => 0],
            ]
        ];

        $this->defaults_api = \core\Loader::loadDipendency('api', 'defaults', $this->Session);
    }

    public function addOrder($data = null, $avoidComunication = false) {
        $fields = [];
        $kField = 'order';
        $logKey = 'Order';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        if ($data)
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        else
            $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        $forDictionary = [/* 'idCategory' => 'ordercategory', 'idStatus' => 'orderstatus', 'idPriority' => 'orderpriority', */ 'idActivity' => 'orderactivity', 'idType2' => 'ordertype2', 'idType3' => 'ordertype3'];
        foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
            if (is_string($data['data'][$dictionaryKey]) && intval($data['data'][$dictionaryKey]) == 0) {
                $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                if (!$data['data'][$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey], 'title' => $data['data'][$dictionaryKey], 'value' => $newDictionaryValue])) {
                    $this->setError('fatal', 'error_insert_dictionary;');
                    return false;
                }
                $data['data'][$dictionaryKey] = $data['data'][$dictionaryKey]['id'];
                // $dict = $dictionary_api->getDictionary(['id' => $data['data'][$dictionaryKey]['id']]);
                // $data['data'][$dictionaryKey] = $dict['id'];
            }
        }



        $data['data']['idUser'] = $this->Session->Acl->User['id'];

        $now = new \DateTime();

        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;
        $data['data']['clientCost'] = 0;
        $data['data']['idOccupant'] = ((int) $data['data']['idOccupant'] > 0 ) ? $data['data']['idOccupant'] : null;

        if (!$id = $this->DB->addOrder($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }


        $uploader_api = \core\Loader::loadDipendency('api', 'uploader', $this->Session);
        $uploader_api->setToken('order', $id, [['folder' => "{$this->Settings['data']['uploadFolder']}$id/", 'sameName' => true], ['extensionEnabled' => false]]);

        $this->Session->Event->add("Add $logKey OK", 'log', 3);
        if (!$avoidComunication) {
            $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
            $data = array_merge($data['data'], $this->getComunicationPlaceHolder($id));
            $comunication_api->sendByTrigger('new.order.inserted', $data, [
                $this->defaults_api->getValue('roles', 'occupant') => [$data['idOccupant']],
                $this->Session->Acl->User['role'] => [$this->Session->Acl->User['id']]
            ]);
        }
        return ['id' => $id];
    }

    private function getComunicationPlaceHolder($id, $type = 'order') {
        switch ($type) {
            case 'orderjob':
                if ($order = $this->DB->getOrderJob(['id' => $id, 'richdata' => 1])) {

                    return [
                        'orderjob_id' => $id,
                        'orderjob_title' => $order['title'],
                        'orderjob_description' => $order['description'],
                        'orderjob_date_update' => $order['dateUpdateIT'],
                        'orderjob_date_delete' => $order['dateDeleteIT'],
                        'orderjob_date_assignment' => $order['dateAssignmentIT'],
                        'orderjob_date_limit' => $order['dateLimitIT'],
                        'orderjob_date_close' => $order['dateCloseIT'],
                        'orderjob_value' => $order['value'],
                        'orderjob_sender_name' => $order['senderName'],
                        'orderjob_sender_id' => $order['senderId'],
                        'orderjob_destination_name' => $order['destinationName'],
                        'orderjob_destination_id' => $order['destinationId'],
                    ];
                }
                break;
            default:
            case 'order':
                if ($order = $this->DB->getOrder(['id' => $id, 'richdata' => 1])) {
                    return [
                        'order_id' => $id,
                        'order_title' => $order['title'],
                        'order_description' => $order['description'],
                        'order_recall' => $order['dateRemindTS'],
                        'order_data_creation' => $order['dateInsertTS'],
                        'order_note' => $order['note'],
                        'order_category' => $order['idCategory_title'],
                        'order_subcategory' => $order['idSubCategory_title'],
                        'order_status' => $order['idStatus_title'],
                        'order_priority' => $order['idPriority_title'],
                        'order_activity' => $order['idActivity_title'],
                        'order_type2' => $order['idType2_title'],
                        'order_type3' => $order['idType3_title'],
                        'condominium_denomination' => $order['condominium'],
                        'condominium_address' => $order['condominium_address'],
                        'condominium_cap' => $order['condominium_cap'],
                        'condominium_city' => $order['condominium_city'],
                        'condominium_fiscal_code' => $order['condominium_fiscal_code'],
                        'occupant_denomination' => $order['occupant'],
                        'order_referent' => $order['referent'],
                    ];
                }
                break;
        }
        return [];
    }

    public function getOrders() {
        //$this->importFromOldData();
        //$this->elaborateOldData();
        //$this->deleteDuplicateFromOldData();
        //$this->importOldData();
        //$this->grabDataFromOldSystem();
        //$this->alignIdFromOldSystem();
        //$this->alignIdFromOldSystemToDB();
        $kField = 'order';
        $logKey = 'Order';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = '';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;


        if ($find = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('find', '', 'string')) ? null : $data) {
            $filters['data']['richdata'] = true;
            $filters['data']['find'] = '%' . $find . '%';
        }
        if ($notin = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('notin', '', 'string')) ? null : $data) {
            $filters['data']['idnotin'] = $notin;
        }

        if ($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin'))
            $filters['data']['limitJobFor'] = $this->Session->Acl->User['id'];


        if ($rich = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_rich', 'POST', 'int')) ? null : $data)
            $filters['data']['richdata'] = true;
        if ($rich = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_providersBase', 'POST', 'int')) ? null : $data)
            $filters['data']['richdataproviders'] = true; /**/
        if ($rich = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_lastJob', 'POST', 'int')) ? null : $data)
            $filters['data']['lastJob'] = true;
        if ($limit = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_limit', 'POST', 'string')) ? null : $data) {
            $limit = explode('|', $limit);
            $filters['data']['limit'] = $limit[1];
            $filters['data']['offset'] = $limit[0];
        } else {
            // Default limit
            $filters['data']['limit'] = 13000;
            $filters['data']['offset'] = 0;
        }
        if ($order = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_order', 'POST', 'string')) ? null : $data)
            $filters['data']['order'] = explode('|', preg_replace('/\:/i', ' ', $order));
        $filters['data']['richdataproviderswstatus'] = 1;

        if ($data = $this->DB->getOrders($filters['data'])) {
            /* foreach ($data as $k => $v) {
              $data[$k]['files'] = $this->getOrderFiles($v['id']);
              } */
            // RICH USERS
            $users = $this->Session->Acl->getUsers() ?? [];


            if ($filters['data']['richdataproviders'] ?? null) {
                $provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
                $providers = $provider_api->DB->getProviders(['deleted' => 0, 'indexBy' => 'idUser']);
                $operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
                $operators = $operator_api->DB->getOperators(['deleted' => 0, 'indexBy' => 'idUser']);
                foreach ($data as $k => $v) {
                    // RICH USERS
                    $data[$k]['userDenomination'] = $users[$data[$k]['idUser']]['name'] ?? '-';
                    // RICH USERS END
                    $isProviderPresentInOrder = $isOperatorPresentInOrder = false;
                    $data[$k]['providersData'] = [];
                    if ($v['providerswstatus']) {
                        $provs = explode(',', $v['providerswstatus']);
                        for ($i = 0; $i < count($provs); $i++) {
                            list($idProv, $dateClose) = explode('||', $provs[$i]);

                            if (array_key_exists($idProv/* $provs[$i] */, $providers)) {
                                $data[$k]['providersData'][$idProv] = ['id' => $providers[$idProv/* $provs[$i] */]['id'], 'denomination' => $providers[$idProv/* $provs[$i] */]['denomination'], 'type' => 'P', 'active' => $dateClose];
                            } else {
                                $data[$k]['providersData'][$idProv] = ['id' => $operators[$idProv/* $provs[$i] */]['id'], 'denomination' => $operators[$idProv/* $provs[$i] */]['denomination'], 'type' => 'O', 'active' => $dateClose];
                            }

                            //  $data[$k]['providersData'][] = ['id' => $providers[$provs[$i]]['id'], 'denomination' => $providers[$provs[$i]]['denomination']];
                            if ($providers[$idProv/* $provs[$i] */]['idUser'] == $this->Session->Acl->User['id']) {
                                $isProviderPresentInOrder = true;
                            } elseif ($operators[$idProv/* $provs[$i] */]['idUser'] == $this->Session->Acl->User['id']) {
                                $isOperatorPresentInOrder = true;
                            }
                        }
                        $data[$k]['providersData'] = array_values($data[$k]['providersData']);
                    }
                    if (in_array($this->Session->Acl->User['role'], [$this->defaults_api->getValue('roles', 'provider'), $this->defaults_api->getValue('roles', 'operator')])) {
                        if (!$v['providerswstatus']) {
                            unset($data[$k]);
                            continue;
                        }
                        if ($this->Session->Acl->User['role'] == $this->defaults_api->getValue('roles', 'provider') && !$isProviderPresentInOrder) {
                            unset($data[$k]);
                            continue;
                        }
                        if ($this->Session->Acl->User['role'] == $this->defaults_api->getValue('roles', 'operator') && !$isOperatorPresentInOrder) {
                            unset($data[$k]);
                            continue;
                        }
                    }
                }
            }
        }

        if ($find && $data) {
            foreach ($data as $k => $v) {
                $data[$k] = ['id' => $v['id'], 'text' => 'TKT-' . $v['id'] . ' ' . $v['title']];
            }
        }

        return $data;
    }

    private function getOrderFiles($idOrder) {
        if (!file_exists(APP . $this->Settings['data']['uploadFolder'])) {
            return false;
        }
        if (!file_exists(APP . $this->Settings['data']['uploadFolder'] . $idOrder)) {
            return false;
        }
        $files = [];
        $dir = APP . $this->Settings['data']['uploadFolder'] . $idOrder . '/';
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && is_file($dir . $entry) && !is_dir($dir . $entry)) {
                    $files[] = [$entry, base64_encode("orders|||$idOrder|||$entry")];
                }
            }
            closedir($handle);
        }
        return $files;
    }

    private function getOrderJobFiles($idOrder, $idOrderJob) {
        if (!file_exists(APP . $this->Settings['data']['uploadFolder'])) {
            return false;
        }
        if (!file_exists(APP . $this->Settings['data']['uploadFolder'] . $idOrder)) {
            return false;
        }
        if (!file_exists(APP . $this->Settings['data']['uploadFolder'] . $idOrder . '/' . $idOrderJob)) {
            return false;
        }
        $files = [];
        $dir = APP . $this->Settings['data']['uploadFolder'] . $idOrder . '/' . $idOrderJob . '/';
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && is_file($dir . $entry) && !is_dir($dir . $entry)) {
                    $files[] = [$entry, base64_encode("orders|||$idOrder/$idOrderJob|||$entry")];
                }
            }
            closedir($handle);
        }
        return $files;
    }

    public function getOrder(array $filter = []) {
        $kField = 'order';
        $logKey = 'Order';

        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else
            return [];
        $jobs = $this->Session->Dataservice->getError($jobs = $this->Session->Dataservice->g('jobs', 'GET', 'int')) ? null : $jobs;
        $richdata = $this->Session->Dataservice->getError($richdata = $this->Session->Dataservice->g('rich', 'GET', 'int')) ? null : $richdata;
        $richdataprov = $this->Session->Dataservice->getError($richdataprov = $this->Session->Dataservice->g('richproviders', 'GET', 'int')) ? null : $richdataprov;
        $order = $this->Session->Dataservice->getError($order = $this->Session->Dataservice->g('order', 'GET', 'string')) ? null : $order;
        $connected = $this->Session->Dataservice->getError($connected = $this->Session->Dataservice->g('connected', 'GET', 'boolean')) ? null : $order;

        if ($order) {
            $order = preg_replace('/\:/i', ' ', $order);
            $order = explode('|', $order);
            $filter['order'] = $order;
        }

        if ($connected)
            $filter['connected'] = $connected;
        $filter['deleted'] = 0;
        $filter['richdata'] = $richdata;
        $filter['richdataproviders'] = $richdataprov;

        // RICH USERS
        $users = $this->Session->Acl->getUsers() ?? [];

        if ($data = $this->DB->getOrder($filter)) {
            if (in_array($this->Session->Acl->User['role'], [$this->defaults_api->getValue('roles', 'provider'), $this->defaults_api->getValue('roles', 'operator')])) {
                if (!$data['providers']) {
                    $this->setError('fatal', 'unauthorized;');
                    return false;
                }
                $providers = explode(',', $data['providers']);
                if (!in_array($this->Session->Acl->User['id'], $providers)) {
                    $this->setError('fatal', 'unauthorized;');
                    return false;
                }
            }
            $data['files'] = $this->getOrderFiles($data['id']);
            if ($jobs) {
                $filterjobs = ['idOrder' => $data['id'], 'richdata' => 1];
                if ($order)
                    $filterjobs['order'] = $filter['order'];
                // Load Jobs
                if ($data['jobs'] = $this->DB->getOrderJobs($filterjobs)) {
                    foreach ($data['jobs'] as $kJ => $vJ) {
                        if (
                                in_array($this->Session->Acl->User['role'], [$this->defaults_api->getValue('roles', 'provider'), $this->defaults_api->getValue('roles', 'operator')]) &&
                                /* TODO:bloccato perchè poteva risultare un exploit per i less privileges che potevano vedere job non propri in console  !$vJ['idOrderJob'] && */
                                (
                                $vJ['idDestination'] != $this->Session->Acl->User['id'] &&
                                $vJ['idSender'] != $this->Session->Acl->User['id']
                                )
                        ) {
                            unset($data['jobs'][$kJ]);
                            continue;
                        }
                        // Load Job Files
                        $data['jobs'][$kJ]['files'] = $this->getOrderJobFiles($vJ['idOrder'], $vJ['id']);
                    }
                    $data['jobs'] = array_values($data['jobs']);
                }
            }
            if ($richdataprov) {
                $data['providersData'] = null;
                if ($data['providers']) {
                    $provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
                    $dataprov = $provider_api->DB->getProviders(['deleted' => 0, 'idUsers' => $data['providers'], 'indexBy' => 'idUser']);
                    $operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
                    $operators = $operator_api->DB->getOperators(['deleted' => 0, 'idUsers' => $data['providers'], 'indexBy' => 'idUser']);
                    $data[$k]['providersData'] = [];
                    foreach ($dataprov as $k => $v) {
                        $v['type'] = 'P';
                        $data['providersData'][$k] = $v;
                    }
                    foreach ($operators as $k => $v) {
                        $v['type'] = 'O';
                        $data['providersData'][$k] = $v;
                    }
                }
            }
            // RICH USERS
            $data['userDenomination'] = $users[$data['idUser']]['name'] ?? '-';
        }
        return $data;
    }

    public function modOrder($id = null, $data = null) {
        $id = $id ?? ($this->Session->Dataservice->getError($id = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $id);
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'order';
        $logKey = 'Order';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$previous = $this->DB->getOrder(array('id' => $id, 'deleted' => 0, 'statusValue' => 1))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        if ($previous['idStatus_value'] == 2) {
            $this->setError('fatal', 'unauthorized;');
            return false;
        }

        if (in_array($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin'))) {
            $this->setError('fatal', 'unauthorized;');
            return false;
        }


        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        if ($data) {
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        } else
            $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $data['data']['deleted'] = $data['data']['deleted'] ?? 0;

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateInsert'] = $previous['dateInsert'];
// adding other data
        // FIND DIFFERENCE
        $differences = [];
        foreach (['title' => 'Titolo', 'description' => 'Descrizione', 'idCategory' => 'Categoria', 'idType3' => 'Tipo 2', 'idType2' => 'Tipo 1', 'idPriority' => 'Priorita', 'idStatus' => 'Stato', 'idSubCategory' => 'Sotto categoria'] as $pk => $vk) {

            if (array_key_exists($pk, $data['data']) && $data['data'][$pk] != $previous[$pk]) {
                $differences[] = "Modificata $vk da {$previous[$pk]} a {$data['data'][$pk]} \n";
            }
        }
        $data['data'] = array_merge($previous, $data['data']);


        $this->DB->safeMode('on');

        if (!$this->DB->modOrder($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }

        if (count($differences)) {
            $this->Session->Dataservice->s('c_idOrder', $id, 'POST');
            $this->Session->Dataservice->d('c_title', 'POST');
            $this->Session->Dataservice->d('c_description', 'POST');

            if (!$idOrderJob = $this->addAtomicOrderJob('edit', 'Modifica dati Commessa ', implode("\n", $differences), null, null, 'editOrder', null, null)) {
                $this->DB->safeMode('cancel');
                return false;
            }

            if (!$this->DB->modOrderJob($idOrderJob['id'], $this->Settings['data']['orderjob']['fieldsMod'], ['dateClose' => $now->format('Y-m-d H:i:s')])) {
                $this->DB->safeMode('cancel');
                $this->setError('fatal', 'error_modify');
                return false;
            }
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        if ($previous['extimate'] != $data['data']['extimate']) {
            $this->Session->Dataservice->s('c_description', "Precedente: {$previous['extimate']}", 'POST');
            if (!$this->setOrderExtimate($id, $data['data']['extimate'], true))
                return false;
        }
        if ($previous['quotation'] != $data['data']['quotation']) {
            $this->Session->Dataservice->s('c_description', "Precedente: {$previous['quotation']}", 'POST');
            if (!$this->setOrderQuotation($id, $data['data']['quotation'], true))
                return false;
        }

        $uploader_api = \core\Loader::loadDipendency('api', 'uploader', $this->Session);
        $uploader_api->setToken('order', $id, [['folder' => "{$this->Settings['data']['uploadFolder']}$id/", 'sameName' => true], ['extensionEnabled' => false]]);
        $this->DB->safeMode('confirm');

        return $this->DB->getOrder(['id' => $id]);
    }

    public function delOrder(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'order';
        $logKey = 'Order';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getOrder(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (in_array($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin'))) {
            $this->setError('fatal', 'unauthorized;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modOrder($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getOrder(['id' => $id]);
    }

    public function addOrderJob($data = null) {
        $fields = [];
        $kField = 'orderjob';
        $logKey = 'OrderJob';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        if ($data)
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        else
            $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        $forDictionary = ['idType' => 'orderjobtype'];
        foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
            if (is_string($data['data'][$dictionaryKey]) && intval($data['data'][$dictionaryKey]) == 0) {
                if ($presentValue = $dictionary_api->getDictionarys(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey]])) {
                    $data['data'][$dictionaryKey] = $presentValue[0]['id'];
                } else {
                    $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                    if (!$data['data'][$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey], 'title' => $data['data'][$dictionaryKey], 'value' => $newDictionaryValue])) {
                        $this->setError('fatal', 'error_insert_dictionary;');
                        return false;
                    }
                    $data['data'][$dictionaryKey] = $data['data'][$dictionaryKey]['id'];
                }
            }
        }
        $data['data']['idSender'] = $data['data']['idSender'] ?? $this->Session->Acl->User['id'];
        $data['data']['senderRole'] = $data['data']['senderRole'] ?? $this->Session->Acl->User['role'];

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;

        if (!$id = $this->DB->addOrderJob($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }
        $uploader_api = \core\Loader::loadDipendency('api', 'uploader', $this->Session);
        $uploader_api->setToken('orderjob', $id, [['folder' => "{$this->Settings['data']['uploadFolder']}{$data['data']['idOrder']}/$id/", 'sameName' => true], ['extensionEnabled' => false]]);
        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getTableOrders() {
        return $this->getOrders();
    }

    public function getOrderJobs() {
        $kField = 'orderjob';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;

        //if ($rich = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('rich', 'POST', 'int')) ? null : $data)
        if ($rich = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('f_rich', '', 'int')) ? null : $data)
            $filters['data']['richdata'] = true;
        $richdataprov = $this->Session->Dataservice->getError($richdataprov = $this->Session->Dataservice->g('f_richproviders', '', 'int')) ? null : $richdataprov;
        if ($data = $this->DB->getOrderJobs($filters['data'])) {
            foreach ($data as $k => $v) {
                $data[$k]['files'] = $this->getOrderJobFiles($v['idOrder'], $v['id']);
            }
        }
        return $data;
    }

    public function setComplete(array $filter = []) {
        $kField = 'order';
        $logKey = 'Order';


        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        $this->Session->Event->add("SetComplete $logKey $id ..", 'log', 3);
        $filter['deleted'] = 0;

        // check if that user can close this order
        if (!$this->DB->getOrder($filter)) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        if (!$statusClose = $dictionary_api->findInDictionary(null, 'orderstatus', 'closed')) {
            $newDictionaryValue = $dictionary_api->getNewValue('orderstatus');
            if (!$statusClose = $dictionary_api->addDictionary(['type' => 'orderstatus', 'code' => 'closed', 'title' => 'closed', 'value' => $newDictionaryValue])) {
                $this->setError('fatal', 'error_insert_dictionary;');
                $this->Session->Event->add("SetComplete $logKey $id KO - Dictionary", 'log', 3);

                return false;
            }
        }

        $now = new \DateTime();
        if (!$this->DB->modOrder($id, $this->Settings['data'][$kField]['fieldsMod'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'idStatus' => $statusClose['id']])) {
            $this->Session->Event->add("SetComplete $logKey $id KO - DB", 'log', 3);

            $this->setError('fatal', 'error_modify');

            return false;
        }
        if (!$this->addOrderJob(['idOrder' => $id, 'idDestination' => $this->Session->Acl->User['id'], 'destinationRole' => $this->Session->Acl->User['role'], 'idType' => 'closing', 'title' => 'Chiusura commessa', 'description' => ''])) {
            $this->Session->Event->add("SetComplete $logKey $id KO - Adding OrderJob", 'log', 3);
            return false;
        }
        $this->Session->Event->add("SetComplete $logKey $id OK", 'log', 3);
        return true;
    }

    private function manageActiveUsers($idOrder, $destinationRole, $activeUser, $add = true) {
        if ($order = $this->DB->getOrder(['id' => $idOrder])) {
            if (!$idActiveUsers = $order['idActiveUsers'])
                $idActiveUsers = '{}';
            try {
                $idActiveUsers = json_decode($idActiveUsers, 1);

                if ($add) {
                    $idActiveUsers[$activeUser['id']] = ['denomination' => $activeUser['denomination'], 'role' => $destinationRole, 'from' => time()];
                } else {
                    unset($idActiveUsers[$activeUser['id']]);
                }
                if (!$this->DB->modOrder($order['id'], ['idActiveUsers'], ['idActiveUsers' => json_encode($idActiveUsers)])) {
                    $this->setError('fatal', 'error_modify');
                    return false;
                }

                return true;
            } catch (Exception $ex) {
                $this->setError('fatal', 'error_json');
            }
        }
        return false;
    }

    private function getUserFromRequest($destinationRole = null, $idDestination = null) {
        $destinationRole = $destinationRole ?? ($this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_destinationRole', 'POST', 'string')) ? null : $data);
        $idDestination = $idDestination ?? ($this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idDestination', 'POST', 'int')) ? null : $data);
        if ($destinationRole == 'c' || $destinationRole == $this->defaults_api->getValue('roles', 'operator')) {
            $operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
            $activeUser = $operator_api->DB->getOperator(['idUser' => $idDestination]);
            $destinationRole = $this->defaults_api->getValue('roles', 'operator');
        } elseif ($destinationRole == 'f' || $destinationRole == $this->defaults_api->getValue('roles', 'provider')) {
            $provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
            $destinationRole = $this->defaults_api->getValue('roles', 'provider');
            $activeUser = $provider_api->DB->getProvider(['idUser' => $idDestination]);
        } else {
            return false;
        }
        if (!$activeUser) {
            return false;
        }
        $activeUser['role'] = $destinationRole;
        return $activeUser;
    }

    public function addProviderToOrder($destinationRole = null, $idDestination = null, $idOrder = null) {
        // Assegnazione
        $destinationRole = $destinationRole ?? ($this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_destinationRole', 'POST', 'string')) ? null : $data);
        $idDestination = $idDestination ?? ($this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idDestination', 'POST', 'int')) ? null : $data);
        $idOrder = $idOrder ?? ($this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data);

        $activeUser = $this->getUserFromRequest($destinationRole, $idDestination);

        $destinationRole = $activeUser['role'];
        if (!$activeUser) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');
        if (!$this->addAtomicOrderJob('commitment', 'Affidamento commessa', null, $destinationRole, null, 'addProviderToOrder')) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_insert');
            return false;
        }

        // $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;



        if (!$this->manageActiveUsers($idOrder, $destinationRole, $activeUser, $add = true)) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_insert');
            return false;
        }
        $this->DB->safeMode('confirm');
        return true;
    }

    public function addProviderToOrderAndCloseJob($idOrderjob = null) {
        $kField = 'orderjob';
        $logKey = 'addProviderToOrderAndCloseJob';
        if (!$idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        if ($orderJob['dateClose']) {
            $this->setError('fatal', 'error_already_close;');
            return false;
        }
        if ($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin')) {
            if (!in_array($this->Session->Acl->User['id'], [$orderJob['idDestination'], $orderJob['idSender']])) {
                $this->setError('fatal', 'error_not_allowed;');
                return false;
            }
        }

        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

// DA VEDERE BENE
        /*      if ($orderJob['destinationRole'] == $this->Session->Acl->User['role'] && $orderJob['idDestination'] == $this->Session->Acl->User['id']) {
          // same destination && sender
          if (!$orderJobFather = $this->DB->getOrderJob(['id' => $orderJob['idOrderJob'], 'deleted' => 0])) {
          $this->setError('fatal', 'error_no_result;');
          return false;
          }

          $orderJob['destinationRole'] = $orderJobFather['senderRole'];
          $orderJob['idDestination'] = $orderJobFather['idSender'];
          }
         */
        $now = new \DateTime();
        $this->DB->safeMode('on');
        // Chiusura 
        if (!$ret = $this->addAtomicOrderJob('closejob', 'Chiusura task', null, $orderJob['destinationRole'], $orderJob['idDestination'], 'addProviderToOrderAndCloseJob', $idOrderjob)) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_insert');
            return false;
        }


        // Delete from activeUsers the previous user
        $activeUser = $this->getUserFromRequest($orderJob['destinationRole'], $orderJob['idDestination']);

        $destinationRole = $activeUser['role'];
        if (!$activeUser) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        if (!$this->manageActiveUsers($orderJob['idOrder'], $destinationRole, $activeUser, $add = false)) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_insert');
            return false;
        }

        $this->Session->Dataservice->d('c_idOrderJob', 'POST');
        if (!$this->addProviderToOrder()) {
            $this->DB->safeMode('cancel');
            return false;
        }

        /*
          // Assegnazione
          $destinationRole = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_destinationRole', 'POST', 'string')) ? null : $data;
          $this->Session->Dataservice->d('c_idOrderJob', 'POST');
          if ($destinationRole == 'c')
          $destinationRole = $this->defaults_api->getValue('roles', 'operator');
          elseif ($destinationRole == 'f')
          $destinationRole = $this->defaults_api->getValue('roles', 'provider');
          else {
          $this->setError('fatal', 'error_data;');
          return false;
          }

          if (!$ret = $this->addAtomicOrderJob('commitment', 'Affidamento commessa', null, $destinationRole, null, 'addProviderToOrder', null)) {
          $this->DB->safeMode('cancel');
          $this->setError('fatal', 'error_insert');
          return false;
          }
         */

        // Aggiornamento Ordine
        if (!$this->DB->modOrderJob($idOrderJob, $this->Settings['data'][$kField]['fieldsMod'], ['dateClose' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->DB->safeMode('confirm');


        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return $ret;
    }

    public function closeorderjob($idOrderjob = null) {
        $kField = 'orderjob';
        $logKey = 'closeorderjob';
        if (!$idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        if ($orderJob['dateClose']) {
            $this->setError('fatal', 'error_already_close;');
            return false;
        }
        if ($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin')) {
            if (!in_array($this->Session->Acl->User['id'], [$orderJob['idDestination'], $orderJob['idSender']])) {
                $this->setError('fatal', 'error_not_allowed;');
                return false;
            }
        }

        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

// DA VEDERE BENE
        /* $orderJobf = $orderJob;
          if ($orderJob['destinationRole'] == $this->Session->Acl->User['role'] && $orderJob['idDestination'] == $this->Session->Acl->User['id']) {
          // same destination && sender
          if (!$orderJobFather = $this->DB->getOrderJob(['id' => $orderJob['idOrderJob'], 'deleted' => 0])) {
          $this->setError('fatal', 'error_no_result;');
          return false;
          }

          $orderJob['destinationRole'] = $orderJobFather['senderRole'];
          $orderJob['idDestination'] = $orderJobFather['idSender'];
          } */

        $now = new \DateTime();
        $this->DB->safeMode('on');
        // Chiusura 
        if (!$ret = $this->addAtomicOrderJob('closejob', 'Chiusura task', null, $orderJob['destinationRole'], $orderJob['idDestination'], 'closeorderjob', $idOrderjob)) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_insert');
            return false;
        }


        // Delete from activeUsers the previous user
        $activeUser = $this->getUserFromRequest($orderJob['destinationRole'], $orderJob['idDestination']);
        if ($activeUser) {
            $destinationRole = $activeUser['role'];
            /* if (!$activeUser) {
              $this->setError('fatal', 'error_data;');
              return false;
              } */

            if (!$this->manageActiveUsers($orderJob['idOrder'], $destinationRole, $activeUser, $add = false)) {
                $this->DB->safeMode('cancel');
                $this->setError('fatal', 'error_insert');
                return false;
            }
        }
        /*
          // Assegnazione
          $destinationRole = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_destinationRole', 'POST', 'string')) ? null : $data;
          $this->Session->Dataservice->d('c_idOrderJob', 'POST');
          if ($destinationRole == 'c')
          $destinationRole = $this->defaults_api->getValue('roles', 'operator');
          elseif ($destinationRole == 'f')
          $destinationRole = $this->defaults_api->getValue('roles', 'provider');
          else {
          $this->setError('fatal', 'error_data;');
          return false;
          }

          if (!$ret = $this->addAtomicOrderJob('commitment', 'Affidamento commessa', null, $destinationRole, null, 'addProviderToOrder', null)) {
          $this->DB->safeMode('cancel');
          $this->setError('fatal', 'error_insert');
          return false;
          }
         */

        // Aggiornamento Ordine
        if (!$this->DB->modOrderJob($idOrderJob, $this->Settings['data'][$kField]['fieldsMod'], ['dateClose' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->DB->safeMode('confirm');


        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return $ret;
    }

    public function setOrderExtimate($idOrder = null, $extimate = null, $noUpdateOrder = false) {
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($extimate)
            $this->Session->Dataservice->s('c_extimate', $extimate, 'POST');
        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $extimate = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_extimate', 'POST', 'float')) ? null : $data;
        if (!$idOrder || !$extimate) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');
        if (!$ret = $this->addAtomicOrderJob('extimate', 'Stima Commessa ' . $extimate . '€', null, $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setOrderExtimate')) {
            $this->DB->safeMode('cancel');
            return false;
        }
        // update extimate in order
        if ($noUpdateOrder)
            return true;

        if (!$this->DB->modOrder($idOrder, $this->Settings['data']['order']['fieldsMod'], ['extimate' => $extimate])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->DB->safeMode('confirm');
        return true;
    }

    public function setOrderQuotation($idOrder = null, $quotation = null, $noUpdateOrder = false) {
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($quotation)
            $this->Session->Dataservice->s('c_quotation', $quotation, 'POST');
        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $quotation = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_quotation', 'POST', 'float')) ? null : $data;
        if (!$idOrder || !$quotation) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');

        // Destination would be the order creator or the previously assignment sender...
        if ($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin')) {
            if ($assignmentOrderJob = $this->DB->getOrderJobs(['idDestination' => $this->Session->Acl->User['id'], 'destinationRole' => $this->Session->Acl->User['role'], 'type' => $this->defaults_api->getValue('orderJobTypes', 'commitment')])) {
                $idDestination = null;
                $destinationRole = null;

                /*  foreach ($assignmentOrderJob as $k => $v) {
                  if (in_array($v['senderRole'], [$this->defaults_api->getValue('roles', 'provider'), $this->defaults_api->getValue('roles', 'operator')]) && $v['idSender'] != $this->Session->Acl->User['id']) {

                  $idDestination = $v['idSender'];
                  $destinationRole = $v['senderRole'];
                  }
                  } */
                if ($this->Session->Acl->User['role'] != $this->defaults_api->getValue('roles', 'admin')) {
                    foreach ($assignmentOrderJob as $k => $v) {
                        if ($v['senderRole'] != $this->Session->Acl->User['role'] && $v['idSender'] != $this->Session->Acl->User['id']) {
                            $idDestination = $v['idSender'];
                            $destinationRole = $v['senderRole'];
                            //TODO:sbloccare se si vuole fare in modo che ogni risposta rimanga in un flusso.. va però poi gestita la visualizzazione
                            //   $this->Session->Dataservice->s('c_idOrderJob', $v['id'], 'POST');
                        }
                    }
                }

                if (!$idDestination) {
                    $idDestination = $this->Session->Acl->User['id'];
                    $destinationRole = $assignmentOrderJob[0]['senderRole'];
                }
            }
        } else {
            $idDestination = $assignmentOrderJob[0]['idSender'];
            $destinationRole = $this->Session->Acl->User['role'];
        }
        /* error_log(print_r($assignmentOrderJob, 1));

          $this->DB->safeMode('cancel');
          return false; */

        if (!$ret = $this->addAtomicOrderJob('quotation', 'Quotazione Commessa ' . $quotation . '€', null, $destinationRole, $idDestination, 'setOrderQuotation', null, $quotation)) {
            $this->DB->safeMode('cancel');

            return false;
        }


        $this->DB->safeMode('confirm');

// update extimate in order
        if ($noUpdateOrder)
            return true;

        /*  if (!$this->DB->modOrder($idOrder, $this->Settings['data']['order']['fieldsMod'], ['quotation' => $quotation])) {
          $this->DB->safeMode('cancel');
          $this->setError('fatal', 'error_modify');
          return false;
          } */
        return true;
    }

    public function setOrderQuotationFor($idOrder = null, $quotation = null, $idDestination = null, $destinationRole = null, $noUpdateOrder = false) {
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($quotation)
            $this->Session->Dataservice->s('c_quotation', $quotation, 'POST');
        if ($idDestination)
            $this->Session->Dataservice->s('c_idDestination', $idDestination, 'POST');
        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $quotation = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_quotation', 'POST', 'float')) ? null : $data;
        // Provider/Operator
        $idSender = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idDestination', 'POST', 'int')) ? null : $data;
        $senderRole = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_destinationRole', 'POST', 'string')) ? null : $data;
        if ($senderRole == 'c')
            $senderRole = $this->defaults_api->getValue('roles', 'operator');
        elseif ($senderRole == 'f')
            $senderRole = $this->defaults_api->getValue('roles', 'provider');
        else {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        if (!$idOrder || !$quotation || $idDestination) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('quotation', '(admin) Quotazione Commessa ' . $quotation . '€', null, $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setOrderQuotation', null, $quotation, ['idSender' => $idSender, 'senderRole' => $senderRole, 'idReferer' => $this->Session->Acl->User['id'], 'senderReferer' => $this->Session->Acl->User['role']])) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $this->DB->safeMode('confirm');


// update extimate in order
        if ($noUpdateOrder)
            return true;

        /*  if (!$this->DB->modOrder($idOrder, $this->Settings['data']['order']['fieldsMod'], ['quotation' => $quotation])) {
          $this->DB->safeMode('cancel');
          $this->setError('fatal', 'error_modify');
          return false;
          } */

        return true;
    }

    public function addOrderNote($idOrder = null, $note = null) {
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($note)
            $this->Session->Dataservice->s('c_description', $note, 'POST');
        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $note = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_description', 'POST', 'string')) ? null : $data;
        if (!$idOrder || !$note) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('comment', 'Commento ', $note, $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setOrderQuotation')) {
            $this->DB->safeMode('cancel');
            return false;
        }

        $this->DB->safeMode('confirm');
        return true;
    }

    private function addAtomicOrderJob($type, $title, $description, $destinationRole, $idDestination, $logKey, $idOrderJob = null, $value = null, $override = null) {
        $fields = [];
        $kField = 'orderjob';

        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
            if ($field == 'idType' && $field == 'title')
                $fields[$field]['need'] = 0;
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        $data['data']['idType'] = $type;
        $data['data']['title'] = $title;
        $data['data']['description'] = $data['data']['description'] ?? $description;
        $data['data']['destinationRole'] = $destinationRole;
        $data['data']['value'] = $value;
        $data['data']['idDestination'] = $idDestination ?? $data['data']['idDestination'];
        if ($override) {
            $data['data'] = array_merge($data['data'], $override);
        }
        if ($idOrderJob)
            $data['data']['idOrderJob'] = $idOrderJob;

        if ($ret = $this->addOrderJob($data['data'])) {
            $this->Session->Event->add("Add $logKey OK", 'log', 3);
            $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
            $data = array_merge($this->getComunicationPlaceHolder($data['data']['idOrder'], 'order'), $this->getComunicationPlaceHolder($ret['id'], 'orderjob'));
            $comunication_api->sendByTrigger("orderjob.$type.inserted", $data, [
                $destinationRole => [$idDestination],
                $this->Session->Acl->User['role'] => [$this->Session->Acl->User['id']]
            ]);

            return $ret;
        }
        $this->Session->Event->add("Add $logKey KO", 'log', 3);
        return false;
    }

    public function addOrderJobResponse() {
        if (!$idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');


        if ($orderJob['destinationRole'] == $this->Session->Acl->User['role'] && $orderJob['idDestination'] == $this->Session->Acl->User['id']) {
            // same destination && sender
            if (!$orderJobFather = $this->DB->getOrderJob(['id' => $orderJob['idOrderJob'], 'deleted' => 0])) {
                $this->setError('fatal', 'error_no_result;');
                return false;
            }

            $orderJob['destinationRole'] = $orderJobFather['senderRole'];
            $orderJob['idDestination'] = $orderJobFather['idSender'];
        }

        if (!$ret = $this->addAtomicOrderJob('response', 'Risposta', null, $orderJob['destinationRole'], $orderJob['idDestination'], 'setOrderJobResponse')) {
            $this->setError('fatal', 'error_insert');
            return false;
        }


        $uploader_api = \core\Loader::loadDipendency('api', 'uploader', $this->Session);
        $uploader_api->setToken('orderjob', $id, [['folder' => "{$this->Settings['data']['uploadFolder']}{$ret['id']}/{$orderJob['idOrder']}/", 'sameName' => true], ['extensionEnabled' => false]]);

        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return $ret;
    }

    public function delOrderFile() {
        if (!$file = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_file', 'POST', 'string')) ? null : $data) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $data = base64_decode($file);
        list($fileType, $subFolder, $file) = explode('|||', $data);
        if (!$file || !$fileType) {
            $this->setError('fatal', 'error_data;' . $file . '--' . $fileType);
            return false;
        }

        $fileName = "{$this->Settings['data']['uploadFolder']}$subFolder/$file";
        if (!file_exists($fileName))
            return true;
        if (!@unlink($fileName)) {
            $this->setError('fatal', 'error_delete;');
            return false;
        }
        return true;
    }

    public function connectOrder($idOrderFrom = null, $idOrderTo = null, $note = false) {

        if ($idOrderFrom)
            $this->Session->Dataservice->s('c_idOrderFrom', $idOrderFrom, 'POST');
        if ($idOrderTo)
            $this->Session->Dataservice->s('c_idOrderTo', $idOrderTo, 'POST');
        if ($note)
            $this->Session->Dataservice->s('c_description', $note, 'POST');
        $idOrderFrom = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderFrom', 'POST', 'int')) ? null : $data;
        $idOrderTo = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderTo', 'POST', 'int')) ? null : $data;
        $note = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_description', 'POST', 'string')) ? null : $data;

        $this->Session->Dataservice->s('c_idOrder', $idOrderFrom, 'POST');

        if (!$idOrderTo || !$idOrderFrom) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');

        if (!$this->DB->addConnection($idOrderFrom, $idOrderTo, $note)) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        /* if (!$this->DB->modOrder($idOrder, $this->Settings['data']['order']['fieldsMod'], ['dateUpdate' => $quotation])) {
          $this->DB->safeMode('cancel');
          $this->setError('fatal', 'error_modify');
          return false;
          } */
        /*        if (!$ret = $this->addAtomicOrderJob('connection', 'Connessa Commessa ' . $idOrderTo, $idOrderFrom, $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'connectOrder')) {
          $this->DB->safeMode('cancel');
          return false;
          } */



        $this->DB->safeMode('confirm');
        return true;
    }

    public function disconnectOrder() {
        //return $this->addAtomicOrderJob('commitment', 'Affidamento commessa', null, $this->defaults_api->getValue('roles', 'provider'), null, 'addProviderToOrder');
    }

//setorderclientcost
    public function setOrderClientCost($idOrder = null, $quotation = null, $noUpdateOrder = false) {
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($quotation)
            $this->Session->Dataservice->s('c_clientCost', $quotation, 'POST');
        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $quotation = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_clientCost', 'POST', 'float')) ? null : $data;
        if (!$idOrder || !$quotation) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('quotation', 'Costo Commessa ' . $quotation . '€', null, $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setOrderClientCost')) {
            $this->DB->safeMode('cancel');
            return false;
        }
        // update extimate in order
        if ($noUpdateOrder)
            return true;

        if (!$this->DB->modOrder($idOrder, $this->Settings['data']['order']['fieldsMod'], ['clientCost' => $quotation])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->DB->safeMode('confirm');
        return true;
    }

    public function acceptQuotation($idOrderJob = null) {
        $kField = 'orderjob';
        $logKey = 'AcceptOrderJobQuotation';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_id', $idOrderJob, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }


        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');
        if (floatval($orderJob['value']) <= 0) {
            $this->setError('fatal', 'error_no_value;');
            return false;
        }

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('quot_accept', 'Accettazione Quotazione Commessa ' . floatval($orderJob['value']) . '€', 'Quotazione accettata', $orderJob['senderRole'], $orderJob['idSender']/* $orderJob['destinationRole'], $orderJob['idDestination'] */, 'acceptOrderQuotation', $idOrderJob, floatval($orderJob['value']))) {
            $this->DB->safeMode('cancel');
            return false;
        }

        $this->Session->Event->add("mod OrderJob $logKey ..", 'log', 3);

        if (!$this->DB->modOrderJob($idOrderJob, $this->Settings['data'][$kField]['fieldsMod'], ['accepted' => 1])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod OrderJob $logKey OK", 'log', 3);

        $total = floatval($orderJob['value']) + floatval($order['quotation']);
        $this->Session->Event->add("mod Order $logKey ..", 'log', 3);

        if (!$this->DB->modOrder($order['id'], $this->Settings['data']['order']['fieldsMod'], ['quotation' => $total])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');
        return true;
    }

    public function declineQuotation($idOrderJob = null) {
        $kField = 'orderjob';
        $logKey = 'DeclineOrderJobQuotation';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_id', $idOrderJob, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');
        if (floatval($orderJob['value']) <= 0) {
            $this->setError('fatal', 'error_no_value;');
            return false;
        }

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        /*        if (!$ret = $this->addAtomicOrderJob('quot_declined', 'Rifiuto Quotazione Commessa ' . floatval($orderJob['value']) . '€', 'Quotazione rifiutata', $orderJob['destinationRole'], $orderJob['idDestination'], 'acceptOrderQuotation', $idOrderJob, floatval($orderJob['value']))) { */
        if (!$ret = $this->addAtomicOrderJob('quot_declined', 'Rifiuto Quotazione Commessa ' . floatval($orderJob['value']) . '€', 'Quotazione rifiutata', $orderJob['senderRole'], $orderJob['idSender'], 'acceptOrderQuotation', $idOrderJob, floatval($orderJob['value']))) {
            $this->DB->safeMode('cancel');
            return false;
        }

        $this->Session->Event->add("mod OrderJob $logKey ..", 'log', 3);

        if (!$this->DB->modOrderJob($idOrderJob, $this->Settings['data'][$kField]['fieldsMod'], ['accepted' => 2])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod OrderJob $logKey OK", 'log', 3);

        $total = floatval($order['quotation']) - floatval($orderJob['value']);
        $this->Session->Event->add("mod Order $logKey ..", 'log', 3);

        if (!$this->DB->modOrder($order['id'], $this->Settings['data']['order']['fieldsMod'], ['quotation' => $total])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');
        return true;
    }

    public function setDataStartWorks($idOrder = null, $date = null) {
        $kField = 'order';
        $logKey = 'setDataStartWorks';
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($date)
            $this->Session->Dataservice->s('c_dateStart', $date, 'POST');

        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $date = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_dateStart', 'POST', 'date')) ? null : $data;

        if (!$idOrder) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        if (!$order = $this->DB->getOrder(['id' => $idOrder, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        /* if ($order['dateWorksStart']){

          } */

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('start_work', "Inizio Lavori " . \core\Utilitiesstring::convertDate($date, 'Y-m-d', 'd-m-Y'), 'Impostato avvio lavori', $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setDataStartWorks', null, $date, ['dateClose' => $date])) {
            $this->DB->safeMode('cancel');
            return false;
        }


        $this->Session->Event->add("mod Order $logKey ..", 'log', 3);

        if (!$this->DB->modOrder($order['id'], $this->Settings['data']['order']['fieldsMod'], ['dateWorksStart' => $date])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');
        return true;
    }

    public function setDataEndWorks($idOrder = null, $date = null) {
        $kField = 'order';
        $logKey = 'setDataEndWorks';
        if ($idOrder)
            $this->Session->Dataservice->s('c_idOrder', $idOrder, 'POST');
        if ($date)
            $this->Session->Dataservice->s('c_dateEnd', $date, 'POST');

        $idOrder = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrder', 'POST', 'int')) ? null : $data;
        $date = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_dateEnd', 'POST', 'date')) ? null : $data;

        if (!$idOrder) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        if (!$order = $this->DB->getOrder(['id' => $idOrder, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        if (!$order['dateWorksStart']) {
            $this->setError('fatal', 'error_no_data_start;');
            return false;
        }

        $from = \core\Utilitiesstring::convertDate($order['dateWorksStart'], 'Y-m-d H:i:s', null, true);
        $to = \core\Utilitiesstring::convertDate($date, 'Y-m-d', null, true);

        if ($to < $from) {
            $this->setError('fatal', 'error_data_less;' . $from->format('d/m/Y') . ' - ' . $to->format('d/m/Y'));
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('start_work', "Termine Lavori " . \core\Utilitiesstring::convertDate($date, 'Y-m-d', 'd-m-Y'), 'Impostato termine lavori', $this->Session->Acl->User['role'], $this->Session->Acl->User['id'], 'setDataEndWorks', null, $date, ['dateClose' => $date])) {
            $this->DB->safeMode('cancel');
            return false;
        }


        $this->Session->Event->add("mod Order $logKey ..", 'log', 3);

        if (!$this->DB->modOrder($order['id'], $this->Settings['data']['order']['fieldsMod'], ['dateWorksEnd' => $date])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');
        return true;
    }

    public function addOrderModule($idOrderJob = null) {
        $kField = 'orderjobmodule';
        $logKey = 'sendOrderModule';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_id', $idOrderJob, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob) {
            $this->setError('fatal', 'error_data;');
            return false;
        }


        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);



        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }


        $this->DB->safeMode('on');


        if (!$ret = $this->addAtomicOrderJob('order_module', 'Invio Modulo d\'ordine', 'Inviato Modulo d\'Ordine, note: ' . $data['data']['note'], $orderJob['senderRole'], $orderJob['idSender']/* $orderJob['destinationRole'], $orderJob['idDestination'] */, $logKey, $idOrderJob, json_encode($data['data']))) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();


        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);
        $this->DB->safeMode('confirm');


        //orderjob.sendordermodule
        //orderjob_module_list
        $orderjob_module_list = [];
        $comunicationData = ['orderjob_module_list' => '', 'orderjob_module_note' => $data['data']['note']];
        foreach ($fields as $k => $v) {
            if (array_key_exists($k, $data['data']) && $data['data'][$k] && $k != 'note')
                $orderjob_module_list[] = "<li>{$v['text']}</li>";
        }
        if (!count($orderjob_module_list)) {
            $orderjob_module_list[] = '<b>Nessun documento richiesto</b>';
        }
        $comunicationData['orderjob_module_list'] = implode("\n", $orderjob_module_list);

        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($comunicationData, $data['data'], $this->getComunicationPlaceHolder($orderJob['idOrder'], 'order'), $this->getComunicationPlaceHolder($ret['id'], 'orderjob'));
        $comunication_api->sendByTrigger('orderjob.sendordermodule', $data, [
            $orderJob['destinationRole'] => [$orderJob['idDestination']],
            $this->Session->Acl->User['role'] => [$this->Session->Acl->User['id']]
        ]);
        return true;
    }

    public function declineOrderModule($idOrderJob = null, $idOrderJobModule = null) {
        $kField = 'orderjobmoduledecline';
        $logKey = 'declineOrderModule';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        if ($idOrderJobModule)
            $this->Session->Dataservice->s('c_idOrderJobModule', $idOrderJobModule, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;
        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJobModule', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (!$orderJobModule = $this->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('decline_order_module', 'Annulamento Modulo d\'ordine', $data['data']['note'], $orderJob['destinationRole'], $orderJob['idDestination'], $logKey, $idOrderJob, json_encode($data['data']))) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();

        // Update OrderJobModule
        $orderJobModule['value'] = json_decode($orderJobModule['value'], 1);
        $orderJobModule['value']['declineNote'] = "{$data['data']['note']} - Annullato in data " . $now->format('d-m-Y H:i:s');
        $orderJobModule['value']['declined'] = true;
        $orderJobModule['value']['declineDate'] = $now->format('Y-m-d H:i:s');
        $orderJobModule['value']['declineId'] = $ret['id'];
        if (!$this->DB->modOrderJob($orderJobModule['id'], ['dateUpdate', 'value'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'value' => json_encode($orderJobModule['value'])])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify_job');
            return false;
        }


        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');
        return true;
    }

    public function addOrderModuleFiles($idOrderJob = null, $idOrderJobModule = null) {
        $kField = 'orderjobmodulefiles';
        $logKey = 'declineOrderModuleFiles';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        if ($idOrderJobModule)
            $this->Session->Dataservice->s('c_idOrderJobModule', $idOrderJobModule, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;
        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJobModule', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob || !$idOrderJobModule) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (!$orderJobModule = $this->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('load_order_module_files', 'Caricamento materiale richiesto', $data['data']['note'], $orderJob['destinationRole'], $orderJob['idDestination'], $logKey, $idOrderJob, json_encode($data['data']))) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();

        // Update OrderJobModule
        $orderJobModule['value'] = json_decode($orderJobModule['value'], 1);
        $orderJobModule['value']['lastUpload'] = $now->format('Y-m-d H:i:s');
        $orderJobModule['value']['lastUploadId'] = $ret['id'];
        if (!array_key_exists('uploadIds', $orderJobModule['value']))
            $orderJobModule['value']['uploadIds'] = [];
        $orderJobModule['value']['uploadIds'][] = $ret['id'];

        if (!$this->DB->modOrderJob($orderJobModule['id'], ['dateUpdate', 'value'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'value' => json_encode($orderJobModule['value'])])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify_job');
            return false;
        }

        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');

        $uploader_api = \core\Loader::loadDipendency('api', 'uploader', $this->Session);
        $uploader_api->setToken('orderjobmodulefiles', $ret['id'], [['folder' => "{$this->Settings['data']['uploadFolder']}{$data['data']['idOrder']}/{$orderJob['idOrder']}/{$ret['id']}/", 'sameName' => true], ['extensionEnabled' => false]]);
        $this->Session->Event->add("Add $logKey OK", 'log', 3);
        return ['id' => $ret['id']];
    }

    public function setOrderModulePartAsClose($idOrderJob = null, $idOrderJobModule = null) {
        $kField = 'orderjobmodulestasclose';
        $logKey = 'closeOrderModulePart';
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        if ($idOrderJobModule)
            $this->Session->Dataservice->s('c_idOrderJobModule', $idOrderJobModule, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;
        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJobModule', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob || !$idOrderJobModule) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (!$orderJobModule = $this->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('set_as_close_order_module', 'Termine caricamento materiali', 'Caricato tutto per: ' . $this->Settings['data']['orderjobmodule']['fields'][$data['data']['type']]['text'], $orderJob['destinationRole'], $orderJob['idDestination'], $logKey, $idOrderJob, json_encode($data['data']))) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();

        // Update OrderJobModule
        $orderJobModule['value'] = json_decode($orderJobModule['value'], 1);
        $orderJobModule['value']['lastUpload'] = $now->format('Y-m-d H:i:s');
        $orderJobModule['value']['lastUploadId'] = $ret['id'];

        // can't close without files

        if (!array_key_exists('uploadIds', $orderJobModule['value'])) {
            $this->setError('fatal', 'no_files_founds');
            $this->DB->safeMode('cancel');
            return false;
        }
        $orderJobModule['value']['uploadIds'][] = $ret['id'];
        $orderJobModule['value'][$data['data']['type'] . '_status'] = 'closed';

        // check if all required fields are ok
        if ($this->isOrderModuleFirstStepCompleted($orderJobModule))
            $orderJobModule['value']['globalStatus'] = 'check';
        else
            $orderJobModule['value']['globalStatus'] = '';


        if (!$this->DB->modOrderJob($orderJobModule['id'], ['dateUpdate', 'value'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'value' => json_encode($orderJobModule['value'])])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify_job');
            return false;
        }


        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');

        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        if ($orderJobModule['value']['globalStatus'] == 'check') {
            $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
            $data = array_merge($data['data'], $this->getComunicationPlaceHolder($id));
            // Uso orderjobmodule perchè viene inviato al provider, quindi avro in sender l'admin o l'operator e in destination il fornitore o l'operatore
            $comunication_api->sendByTrigger('orderjob.ordermodule.tobecheck', $data, [
                $this->defaults_api->getValueInverse('roles', $orderJobModule['senderRole']) => [$orderJobModule['senderId']],
                $this->defaults_api->getValue('roles', $orderJobModule['destinationRole']) => [$orderJobModule['destinationId']]
            ]);
        }

        return ['id' => $ret['id']];
    }

    // Find all order not closed, with all ordermodule completed
    private function checkOrdersReadyToTestInvoice() {
        if (!$orderJobs = $this->DB->getOrderJobs(['deleted' => 0, 'typeTitle' => 'order_module']))
            return null;
        $orders = [];

        foreach ($orderJobs as $orderJob) {
            $orderJob['value'] = (is_string($orderJob['value'])) ? json_decode($orderJob['value'], 1) : $orderJob['value'];
            if ($this->isOrderModuleFirstStepCompleted($orderJob)) {
                if ($orderJob['value']['globalStatus'] == 'check') {
                    if (!array_key_exists($orderJob['idOrder'], $orders))
                        $orders[$orderJob['idOrder']] = [];
                    $orders[$orderJob['idOrder']][] = $orderJob;
                }
            }
        }
        return $orders;
        //error_log(print_r($orders));
    }

    public function getOrdersReadyToTestInvoice() {
        $return = [];
// reorder data
        if (!$data = $this->checkOrdersReadyToTestInvoice())
            return $return;

        // Fields to search in value
        if (!$this->globalFieldsToCheck) {
            $this->globalFieldsToCheck = [];
            foreach ($this->Settings['data']['orderjobmodule']['fields'] as $k => $v) {
                if ($v['check'])
                    $this->globalFieldsToCheck[] = $k;
            }
        }

        foreach ($data as $kOrder => $order) {

            foreach ($order as $orderModules) {
                $tmpReturn = ['idOrder' => $kOrder, 'idOrderModule' => null, 'uploadIds' => null, 'lastUploadId' => null, 'globalStatus' => null, 'dateUpdateIT' => null, 'dateUpdateTS' => null, 'dateInsertIT' => null, 'dateInsertTS' => null, 'note' => null, 'orderList' => null, 'provider' => null, 'destinationType' => null, 'providerName' => null, 'providerId' => null, 'uploadsData' => null];
                $tmpReturn['idOrderModule'] = $orderModules['id'];
                $tmpReturn['dateUpdateIT'] = $orderModules['dateUpdateIT'];
                $tmpReturn['dateUpdateTS'] = $orderModules['dateUpdateTS'];
                $tmpReturn['dateInsertIT'] = $orderModules['dateInsertIT'];
                $tmpReturn['dateInsertTS'] = $orderModules['dateInsertTS'];
                $tmpReturn['provider'] = $orderModules['idDestination'];
                $tmpReturn['idOrderJobParent'] = $orderModules['idOrderJob'];

                $tmpReturn['destinationType'] = $this->defaults_api->getValueInverse('roles', $orderModules['destinationRole']);
                if (!$cacheDestinations)
                    $cacheDestinations = [];
                if (array_key_exists($orderModules['idDestination'] . '-' . $orderModules['destinationRole'], $cacheDestinations))
                    $user = $cacheDestinations[$orderModules['idDestination'] . '-' . $orderModules['destinationRole']];

                else {
                    switch ($orderModules['destinationRole']) {
                        case $this->defaults_api->getValue('roles', 'operator'):
                        case $this->defaults_api->getValue('roles', 'admin'):
                        case $this->defaults_api->getValue('roles', 'segretary'):
                            if (!$user) {
                                if (!$this->operator_api)
                                    $this->operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
                                if ($user = $this->operator_api->getOperator(['idUser' => $orderModules['idDestination']]))
                                    $cacheDestinations[$orderModules['idDestination'] . '-' . $orderModules['destinationRole']] = $user;
                            }
                            break;
                        case $this->defaults_api->getValue('roles', 'occupant'):
                            if (!$user) {
                                if (!$this->occupant_api)
                                    $this->occupant_api = \core\Loader::loadDipendency('api', 'occupant', $this->Session);
                                if ($user = $this->occupant_api->getOccupant(['id' => $orderModules['idDestination']]))
                                    $cacheDestinations[$orderModules['idDestination'] . '-' . $orderModules['destinationRole']] = $user;
                            }
                            break;
                        case $this->defaults_api->getValue('roles', 'provider'):
                            if (!$user) {
                                if (!$this->provider_api)
                                    $this->provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
                                if ($user = $this->provider_api->getProvider(['idUser' => $orderModules['idDestination']]))
                                    $cacheDestinations[$orderModules['idDestination'] . '-' . $orderModules['destinationRole']] = $user;
                            }
                            break;
                    }
                }

                $tmpReturn['providerId'] = $user['id'] ?? '';
                $tmpReturn['providerName'] = $user['denomination'] ?? '';

                $tmpReturn['orderList'] = [];

                $values = (is_string($orderModules['value'])) ? json_decode($orderModules['value'], 1) : $orderModules['value'];
                foreach ($this->globalFieldsToCheck as $field) {
                    if (is_array($values) && array_key_exists($field, $values) && $values[$field] == true)
                        $tmpReturn['orderList'][$field] = $values[$field . '_status'];
                }
                $tmpReturn['lastUploadId'] = $values['lastUploadId'];
                if ($tmpReturn['uploadIds'] = $values['uploadIds']) {
                    if ($tmpReturn['uploadsData'] = $this->DB->getOrderJobs(['idIn' => implode(',', $values['uploadIds'])])) {
                        foreach ($tmpReturn['uploadsData'] as $kUD => $vUD) {
                            $tmpReturn['uploadsData'][$kUD]['files'] = $this->getOrderJobFiles($vUD['idOrder'], $vUD['id']);
                        }
                    }
                }

                $tmpReturn['note'] = $values['note'];
                $tmpReturn['status'] = $values['globalStatus'];
                $return[] = $tmpReturn;
            }

            // clean results
            // get all order job connected  or only when show order module?
        }

        return $return;
    }

    // First Step: all required materials are sended -> set globalStatus to "check"
    // Second Step: order module with globalStatus == "check" -> set globalStatus to "checked" or "reject"

    function isOrderModuleFirstStepCompleted(&$orderJob) {
        if (!$this->globalFieldsToCheck) {
            $this->globalFieldsToCheck = [];
            foreach ($this->Settings['data']['orderjobmodule']['fields'] as $k => $v) {
                if ($v['check'])
                    $this->globalFieldsToCheck[] = $k;
            }
        }
        if (is_string($orderJob['value']))
            $data = json_decode($orderJob['value'], 1);
        else
            $data = $orderJob['value'];
        if ($data['declined'] ?? null)
            return false;

        foreach ($this->globalFieldsToCheck as $fieldName) {
            if ($data[$fieldName] == true)
                if (!array_key_exists($fieldName . '_status', $data) || $data[$fieldName . '_status'] != 'closed')
                    return false;
        }
        return true;
    }

    public function setAsCorrection($idOrderJob = null, $idOrderJobModule = null, $type = null) {
        $kField = 'orderjobmodulecorrection';
        $logKey = 'ordermoduleSetAsCorrection';
        if ($idOrderJobModule)
            $this->Session->Dataservice->s('c_idOrderJobModule', $idOrderJobModule, 'POST');
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        if ($type)
            $this->Session->Dataservice->s('c_type', $type, 'POST');

        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;
        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJobModule', 'POST', 'int')) ? null : $data;
        $type = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_type', 'POST', 'string')) ? null : $data;

        if (!$idOrderJob || !$idOrderJobModule || !$type) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (!$orderJobModule = $this->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
// needed from atomic method
        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('set_correction_to_module', 'Richiesta rettifica materiali', $data['data']['note'], $orderJobModule['destinationRole'], $orderJobModule['idDestination'], $logKey, $idOrderJob)) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();

        // Update OrderJobModule
        $orderJobModule['value'] = json_decode($orderJobModule['value'], 1);
        $orderJobModule['value']['lastCorrection'] = $now->format('Y-m-d H:i:s');
        $orderJobModule['value']['lastCorrectionId'] = $ret['id'];

        // can't close without files

        if (!array_key_exists('correctionIds', $orderJobModule['value'])) {
            $orderJobModule['value']['correctionIds'] = [];
        }
        $orderJobModule['value']['correctionIds'][] = $ret['id'];
        $orderJobModule['value'][$data['data']['type'] . '_status'] = 'correction';

        // check if all required fields are ok

        $orderJobModule['value']['globalStatus'] = 'correction';



        if (!$this->DB->modOrderJob($orderJobModule['id'], ['dateUpdate', 'value'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'value' => json_encode($orderJobModule['value'])])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify_job');
            return false;
        }


        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');

        $this->Session->Event->add("Add $logKey OK", 'log', 3);


        $comunication_api = \core\Loader::loadDipendency('api', 'comunication', $this->Session);
        $data = array_merge($data['data'], $this->getComunicationPlaceHolder($id));
        // Uso orderjobmodule perchè viene inviato al provider, quindi avro in sender l'admin o l'operator e in destination il fornitore o l'operatore
        $comunication_api->sendByTrigger('orderjob.ordermodule.tobecorrected', $data, [
            $this->defaults_api->getValueInverse('roles', $orderJobModule['senderRole']) => [$orderJobModule['senderId']],
            $this->defaults_api->getValue('roles', $orderJobModule['destinationRole']) => [$orderJobModule['destinationId']]
        ]);


        return ['id' => $ret['id']];
    }

    public function setAsCompleted($idOrderJob = null, $idOrderJobModule = null, $note = null) {
        $kField = 'orderjobmodulecompleted';
        $logKey = 'ordermoduleSetAsCompleted';
        if ($idOrderJobModule)
            $this->Session->Dataservice->s('c_idOrderJobModule', $idOrderJobModule, 'POST');
        if ($idOrderJob)
            $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        if ($note)
            $this->Session->Dataservice->s('c_note', $note, 'POST');


        $idOrderJob = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJob', 'POST', 'int')) ? null : $data;
        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_idOrderJobModule', 'POST', 'int')) ? null : $data;

        if (!$idOrderJob || !$idOrderJobModule) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        // Load Input Data
        $fields = [];
        foreach ($this->Settings['data'][$kField]['fields'] as $k => $field) {
            $fields[$k] = $field;
            $fields[$k]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);

        if (!$orderJob = $this->DB->getOrderJob(['id' => $idOrderJob, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        if (!$orderJobModule = $this->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
// needed from atomic method
        $this->Session->Dataservice->s('c_idOrderJob', $idOrderJob, 'POST');
        $this->Session->Dataservice->s('c_idOrder', $orderJob['idOrder'], 'POST');

        if (!$order = $this->DB->getOrder(['id' => $orderJob['idOrder'], 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $this->DB->safeMode('on');

        if (!$ret = $this->addAtomicOrderJob('order_module_checked', 'Modulo d\'ordine verificato', $data['data']['note'], $orderJobModule['destinationRole'], $orderJobModule['idDestination'], $logKey, $idOrderJob)) {
            $this->DB->safeMode('cancel');
            return false;
        }
        $now = new \DateTime();

        // Update OrderJobModule
        $orderJobModule['value'] = json_decode($orderJobModule['value'], 1);
        $orderJobModule['value']['acceptDate'] = $now->format('Y-m-d H:i:s');
        $orderJobModule['value']['acceptId'] = $ret['id'];

        $orderJobModule['value']['globalStatus'] = 'verified';
        if (!$this->DB->modOrderJob($orderJobModule['id'], ['dateUpdate', 'value'], ['dateUpdate' => $now->format('Y-m-d H:i:s'), 'value' => json_encode($orderJobModule['value'])])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify_job');
            return false;
        }


        if (!$this->DB->modOrder($order['id'], ['dateUpdate'], ['dateUpdate' => $now->format('Y-m-d H:i:s')])) {
            $this->DB->safeMode('cancel');
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("mod Order $logKey OK", 'log', 3);

        $this->DB->safeMode('confirm');

// Add as invoice
        $invoice_api = \core\Loader::loadDipendency('api', 'invoice', $this->Session);
        $invoice_api->addInvoice(['total' => $orderJob['value'], 'description' => $data['data']['note'], 'extra' => ''], 'invite', 'wait_for_send', $orderJobModule['id']);


        $this->Session->Event->add("Add $logKey OK", 'log', 3);
        return ['id' => $ret['id']];
    }

    private function importFromOldData() {
        //return;
        if (file_exists($uploadedFileName = 'upload/import/exportOrders.xls')) {
            require_once APP . 'vendors/Excel/oleread.php';
            require_once APP . 'vendors/Excel/reader.php';
            $data = new \Spreadsheet_Excel_Reader();
            $data->read($uploadedFileName);
            if (!$rows = $data->sheets[0]['numRows']) {
                $this->setError('fatal', "no_data;");
                return false;
            }
            $importFields = [1 => "dateInsert", "dateUpdate", "dateRemind", "code", "idPriority", "condominium", "idCategory", "idSubCategory", "description", "idStatus", "referent", "", ""];
            error_log("Ci sono {$data->sheets[0]['numRows']} elementi");
            for ($x = 0; $x <= 22; $x++) {
                $dataOut = [];
                error_log("inizio da  " . $x * 1000);

                for ($i = 0; $i <= 1000 /* $data->sheets[0]['numRows'] */; $i++) {

                    $ii = $i + ($x * 1000);
                    if (!array_key_exists($ii, $data->sheets[0]['cells']))
                        continue;
                    $tmp = [];
                    for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                        $tmp[$importFields[$j]] = trim(utf8_encode($data->sheets[0]['cells'][$ii][$j]));
                    }
                    $dataOut[] = $tmp;
                }
                error_log("finisco con  " . $ii);
                unlink("upload/import/exportOrders_$x.php");
                file_put_contents("upload/import/exportOrders_$x.php", '<?php return ' . var_export($dataOut, 1) . '; ?>');
            }
        }
    }

    private function elaborateOldData() {
        $condominium_api = \core\Loader::loadDipendency('api', 'condominium', $this->Session);
        $condominiums = $condominium_api->getCondominiums();

        function findCondominium($condominium, $condominiums) {
            $condominium = str_replace('(S)', '', $condominium);
            $condominium = str_replace('*', '', $condominium);
            $condominium = 'Condominio ' . $condominium;
            foreach ($condominiums as $v) {
                if (strtolower(trim($v['denomination'])) == strtolower(trim($condominium)))
                    return $v['id'];
            }
            return 0;
        }

        $idStatus = ['Chiuso' => 48, 'Aperto' => 36];
        $idPriority = ['Media' => 43, 'Alta' => 37, 'Bassa' => 31, 'Urgente' => 240];
        $idCategory = ['Amministrativo' => 35, 'Interventi' => 29];

        function managedate($date) {
            if (!$date)
                return null;
            $data = explode('/', $date);
            return "{$data[2]}-{$data[1]}-{$data[0]} 00:00:00";
        }

        for ($i = 0; $i <= 23; $i++) {
            error_log("elaboro $i");
            if (file_exists("upload/import/exportOrders_$i.php")) {
                $dataOut = require("upload/import/exportOrders_$i.php");
                if (is_array($dataOut)) {
                    $newOut = [];
                    error_log('implemento');
                    foreach ($dataOut as $v) {
                        $new = ['idOccupant' => null, 'idCondominium' => null, 'idUser' => null, 'idCategory' => null, 'idSubCategory' => null, 'idStatus' => null, 'idPriority' => null, 'idActivity' => null, 'idType2' => null, 'idType3' => null, 'referent' => null, 'dateInsert' => null, 'dateUpdate' => null, 'dateDelete' => null, 'code' => null, 'description' => null, 'title' => null, 'note' => null, 'dateRemind' => null, 'deleted' => 0, 'quotation' => 0, 'extimate' => 0, 'clientCost' => 0];
                        /* $now = \DateTime::createFromFormat('d/m/Y', $v['dateInsert']);
                          $new['dateInsert'] = $now->format('Y-m-d H:i:s');
                          $now = \DateTime::createFromFormat('d/m/Y', $v['dateUpdate']);
                          $new['dateUpdate'] = $now->format('Y-m-d H:i:s');
                          $now = \DateTime::createFromFormat('d/m/Y', $v['dateRemind']);
                          $new['dateRemind'] = $now->format('Y-m-d H:i:s');
                         */
                        $new['dateInsert'] = managedate($v['dateInsert']);
                        $new['dateUpdate'] = managedate($v['dateUpdate']);
                        $new['dateRemind'] = managedate($v['dateRemind']);

                        $new['deleted'] = 0;
                        $new['clientCost'] = 0;
                        $new['idOccupant'] = null;
                        $new['idCondominium'] = findCondominium($v['condominium'], $condominiums) ?? 0;
                        $new['idUser'] = 3; //system
                        $new['idCategory'] = $idCategory[ucwords(strtolower($v['idCategory']))] ?? ucwords(strtolower($v['idCategory']));


                        $new['idSubCategory'] = ucwords(strtolower($v['idSubCategory'])) ?? 0;
                        $new['idSubCategory'] = $new['idSubCategory'] ?? 0;
                        $new['idStatus'] = $idStatus[$v['idStatus']] ?? $v['idStatus'] ?? 0;
                        $new['idPriority'] = $idPriority[$v['idPriority']] ?? $v['idPriority'] ?? 0;
                        $new['referent'] = $v['referent'] ?? null;
                        $new['code'] = $v['code'] ?? null;
                        $new['description'] = $v['description'] ?? null;
                        $new['title'] = 'IMPORT - ' . $v['code'];
                        //$new['note'] = json_encode(['dateImport' => time(), 'id' => null]);
                        $new['note'] = 'toalign';
                        $newOut[] = $new;
                    }
                    error_log("salvo $i");
                    unlink("upload/import/exportOrders_$i.php");
                    file_put_contents("upload/import/exportOrders_$i.php", '<?php return ' . var_export($newOut, 1) . '; ?>');
                }
            }
        }
    }

    private function deleteDuplicateFromOldData() {


        $orders = $this->DB->getOrders(['onlyCode' => 1]);

        function findOrder($order, $orders) {
            foreach ($orders as $k => $v) {
                if ($order['code'] == $v['code']) {
                    return $v;
                }
            }
            return false;
        }

        for ($i = 0; $i <= 23; $i++) {
            error_log("elaboro $i");
            if (file_exists("upload/import/exportOrders_$i.php")) {
                $dataOut = require("upload/import/exportOrders_$i.php");
                if (is_array($dataOut)) {
                    $newOut = [];
                    error_log('implemento');
                    foreach ($dataOut as $v) {
                        if (!$find = findOrder($v, $orders)) {
                            $newOut[] = $v;
                        }
                    }
                    error_log("salvo $i");
                    unlink("upload/import/exportOrders_$i.php");
                    file_put_contents("upload/import/exportOrders_$i.php", '<?php return ' . var_export($newOut, 1) . '; ?>');
                }
            }
        }



        die;
        return; //$newOut;
    }

    private function importOldData() {

        $now = new \DateTime();
        $fields = [];
        $kField = 'order';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }


        for ($i = 0; $i <= 23; $i++) {
            error_log("importo $i");
            if (file_exists("upload/import/exportOrders_$i.php")) {
                $newOut = require("upload/import/exportOrders_$i.php");
                if ($newOut && is_array($newOut) && count($newOut)) {
                    foreach ($newOut as $k => $v) {
                        if (array_key_exists('load', $v) && $v['load'] == 'ok')
                            continue;
                        //    $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);
                        /* $forDictionary = ['idCategory' => 'ordercategory', 'idSubCategory' => 'ordercategory_1', 'idStatus' => 'orderstatus', 'idPriority' => 'orderpriority'];
                          foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
                          if ($v[$dictionaryKey] != null && is_string($v[$dictionaryKey]) && intval($v[$dictionaryKey]) == 0) {
                          $v[$dictionaryKey] =0;
                          $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                          error_log("inserisco nel db per $dictionaryKey $newDictionaryValue");
                          if (!$v[$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $v[$dictionaryKey], 'title' => $v[$dictionaryKey], 'value' => $newDictionaryValue])) {
                          $this->setError('fatal', 'error_insert_dictionary;');
                          return false;
                          }
                          $v[$dictionaryKey] = $v[$dictionaryKey]['id'];
                          error_log("eccolo " . print_r($v[$dictionaryKey], 1));
                          }
                          } */
                        $v['idSubCategory'] = ((int) $v['idSubCategory']) ? $v['idSubCategory'] : 0;
                        $v['idCondominium'] = ((int) $v['idCondominium']) ? $v['idCondominium'] : 1;
                        $v['idCategory'] = ((int) $v['idCategory']) ? $v['idCategory'] : 29;
                        $v['idStatus'] = ((int) $v['idStatus']) ? $v['idStatus'] : 48;
                        $v['idPriority'] = ((int) $v['idPriority']) ? $v['idPriority'] : 43;
                        if (!$id = $this->DB->addOrder($v, $this->Settings['data'][$kField]['fields'])) {
                            $newOut[$k]['load'] = 'err';
                            error_log('error' . print_r($v, 1));
                            //return false;
                        } else {
                            $newOut[$k]['load'] = 'ok';
                        }
                    }
                    error_log("salvo $i");
                    unlink("upload/import/exportOrders_$i.php");
                    file_put_contents("upload/import/exportOrders_$i.php", '<?php return ' . var_export($newOut, 1) . '; ?>');
                }
            }
        }
    }

    private function grabDataFromOldSystem() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://www.interventicondominio.com');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "Installazione=studiorecco&Username=andrea&Password=pocasale54&Tipo=Operatore");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/upload/tmp');  //could be empty, but cause problems on some hosts
        $answer = curl_exec($ch);
        if (curl_error($ch)) {
            error_log(curl_error($ch));
        }

//another request preserving the session

        curl_setopt($ch, CURLOPT_URL, 'www.interventicondominio.com/Operatore/ElencoJson?Studio=00000000-0000-0000-0000-000000000000&Stato=InCorso&Priorita=&TipoSegnalazione=&Operatore=00000000-0000-0000-0000-000000000000&Referente=&Fornitore=&Ricerca=&Condominio=00000000-0000-0000-0000-000000000000&Categoria=00000000-0000-0000-0000-000000000000&DaData=2015-05-27+00%3A00%3A00&AData=&TipoFiltroData=Inserimento');
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "");
        $answer = curl_exec($ch);
        if (curl_error($ch)) {
            error_log(curl_error($ch));
        }
        $data = json_decode($answer, 1);
        $return = [];
        foreach ($data as $v) {
            $return[$v['Codice']] = $v['Id'];
        }
        file_put_contents("upload/import/exportOrdersID.php", '<?php return ' . var_export($return, 1) . '; ?>');
    }

    private function alignIdFromOldSystem() {

        function findOrderCode($code, $orders) {
            foreach ($orders as $k => $v) {
                if ($code == $v['code']) {
                    return $v;
                }
            }
            return false;
        }

        if (file_exists("upload/import/exportOrdersID.php")) {
            $dataOut = require("upload/import/exportOrdersID.php");
            if (is_array($dataOut)) {
                error_log('start');
                $orders = $this->DB->getOrders(['fields' => ['`C`.`code`', '`C`.`id`', '`C`.`note`']]);
                $newOut = [];
                foreach ($dataOut as $code => $id) {
                    if ($find = findOrderCode($code, $orders)) {
                        $find['newId'] = $id;
                        $newOut[] = $find;
                    }
                }
                error_log('endf');
            }
            unlink("upload/import/exportOrdersIDCode.php");
            file_put_contents("upload/import/exportOrdersIDCode.php", '<?php return ' . var_export($newOut, 1) . '; ?>');
        }
    }

    private function alignIdFromOldSystemToDB() {
        if (file_exists("upload/import/exportOrdersIDCode.php")) {
            $dataOut = require("upload/import/exportOrdersIDCode.php");
            if (is_array($dataOut)) {
                error_log('start');

                foreach ($dataOut as $v) {
                    if (!$this->DB->modOrder($v['id'], ['note'], ['note' => json_encode(['id' => $v['newId']])])) {
                        error_log('err');

                        return false;
                    }
                }
                error_log('end');
            }
        }
    }

}
