<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\order;

class Order extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getOrders(array $filter): array {
        $outFilter = $joinFilter = $fields = $groups = [];
        $indexBy = null;
        $fields[] = " `C`.*";
        $fields[] = " DATE_FORMAT(`C`.`dateInsert`, '%d/%m/%Y %H:%i') AS `dateInsertIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateUpdate`, '%d/%m/%Y %H:%i') AS `dateUpdateIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateDelete`, '%d/%m/%Y %H:%i') AS `dateDeleteIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateRemind`, '%d/%m/%Y %H:%i') AS `dateRemindIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateWorksStart`, '%d/%m/%Y') AS `dateWorksStartIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateWorksEnd`, '%d/%m/%Y') AS `dateWorksEndIT`";

        $fields[] = " UNIX_TIMESTAMP(`C`.`dateInsert`) AS `dateInsertTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateUpdate`) AS `dateUpdateTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateDelete`) AS `dateDeleteTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateRemind`) AS `dateRemindTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateWorksStart`) AS `dateWorksStartTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateWorksEnd`) AS `dateWorksEndTS`";

        foreach (['idOccupant', 'idCondominium', 'idUser', 'idCategory', 'idSubCategory', 'idStatus', 'idPriority', 'idActivity', 'idType2', 'idType3', 'referent', 'code', 'description', 'title', 'note'] as $f)
            if ($filter[$f] ?? null)
                $outFilter[$f] = "`C`.`$f` LIKE :$f";
            else
                unset($filter[$f]);

        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['idOccupant'] ?? null)
            $outFilter['idOccupant'] = "`C`.`idOccupant` = :idOccupant";
        if ($filter['idCondominium'] ?? null)
            $outFilter['idCondominium'] = "`C`.`idCondominium` = :idCondominium";
        if ($filter['idCategory'] ?? null)
            $outFilter['idCategory'] = "`C`.`idCategory` = :idCategory";
        if ($filter['idUser'] ?? null)
            $outFilter['idUser'] = "`C`.`idUser` = :idUser";

        if ($filter['idStatus'] ?? null)
            $outFilter['idStatus'] = "`C`.`idStatus` = :idStatus";
        if ($filter['idPriority'] ?? null)
            $outFilter['idPriority'] = "`C`.`idPriority` = :idPriority";
        if ($filter['find'] ?? null) {
            $outFilter['find'] = "(`C`.`id` LIKE :find OR `C`.`title` LIKE :find )";
        }
        if ($filter['idnotin'] ?? null) {
            $outFilter['idnotin'] = "`C`.`id` NOT IN (:idnotin)";
        }
        if ($filter['idIn'] ?? null) {
            $outFilter['idIn'] = "`C`.`id` IN (:idin)";
        }
        if ($filter['lastJob'] ?? null) {
            unset($filter['lastJob']);
            if ($filter['limitJobFor'] ?? null)
                $fields['lastJob'] = " ( SELECT CONCAT(COALESCE(`OJLJ`.`title`,''), ' - ',COALESCE(`OJLJ`.`description`,''))   FROM `app_OrderJobs` `OJLJ` WHERE `OJLJ`.`idOrder` = `C`.`id` AND (`OJLJ`.`idDestination` = {$filter['limitJobFor']} OR `OJLJ`.`idSender` = {$filter['limitJobFor']}) ORDER BY `OJLJ`.`dateInsert` DESC LIMIT 1 ) as `lastJobDescription`";
            else
                $fields['lastJob'] = " ( SELECT CONCAT(COALESCE(`OJLJ`.`title`,''), ' - ',COALESCE(`OJLJ`.`description`,''))   FROM `app_OrderJobs` `OJLJ` WHERE `OJLJ`.`idOrder` = `C`.`id` ORDER BY `OJLJ`.`dateInsert` DESC LIMIT 1 ) as `lastJobDescription`";
        }
        if ($filter['statusValue'] ?? null) {
            $fields['idStatusv'] = " `DidStatus`.`value` as idStatus_value";
            $joinFilter['idStatus'] = 'LEFT JOIN `app_Dictionary` `DidStatus` ON `DidStatus`.`id` = `C`.`idStatus` ';
        }

        if ($filter['richdata'] ?? null) {
            unset($filter['richdata']);
            foreach (['idCategory' => 'ordercategory', 'idSubCategory' => 'ordercategory_%', 'idStatus' => 'orderstatus', 'idPriority' => 'orderpriority', 'idActivity' => 'orderactivity', 'idType2' => 'ordertype2', 'idType3' => 'ordertype3'] as $f => $k) {
                $fields[$f . 't'] = " `D$f`.`title` as {$f}_title";
                $fields[$f . 'v'] = " `D$f`.`value` as {$f}_value";
                //$joinFilter[$f] = 'LEFT JOIN `app_Dictionary` `D' . $f . '` ON `D' . $f . '`.`value` = `C`.`' . $f . '` AND `D' . $f . '`.`type` = "' . $k . '"';
                $joinFilter[$f] = 'LEFT JOIN `app_Dictionary` `D' . $f . '` ON `D' . $f . '`.`id` = `C`.`' . $f . '` ';
            }
            $joinFilter['condominiums'] = 'LEFT JOIN `app_Condominiums` `COND` ON `COND`.`id` = `C`.`idCondominium`';
            $fields[] = " `COND`.`denomination` as `condominium`";
            $fields[] = " `COND`.`address` as `condominium_address`";
            $fields[] = " `COND`.`cap` as `condominium_cap`";
            $fields[] = " `COND`.`city` as `condominium_city`";
            $joinFilter['occupants'] = 'LEFT JOIN `app_Occupants` `OCCUP` ON `OCCUP`.`id` = `C`.`idOccupant`';
            $fields[] = " `OCCUP`.`denomination` as `occupant`";
            $fields['countProviders'] = " ( SELECT COUNT(DISTINCT `OJ`.`idDestination`) FROM `app_OrderJobs` `OJ` WHERE `OJ`.`idOrder` = `C`.`id` AND `OJ`.`idType` = (SELECT `DidType`.`id` FROM `app_Dictionary` `DidType` WHERE `DidType`.`type` = 'orderjobtype' AND `DidType`.`code` = 'commitment' LIMIT 1) ) as `countProviders`";
        }
        if ($filter['richdataproviders'] ?? null) {
            unset($filter['richdataproviders']);
            $fields['providers'] = " ( SELECT GROUP_CONCAT(DISTINCT `OJ`.`idDestination`) FROM `app_OrderJobs` `OJ` WHERE `OJ`.`idOrder` = `C`.`id` AND `OJ`.`idType` = (SELECT `DidType`.`id` FROM `app_Dictionary` `DidType` WHERE `DidType`.`type` = 'orderjobtype' AND `DidType`.`code` = 'commitment' LIMIT 1)  ORDER BY `OJ`.`dateClose` DESC) as `providers`";
        }

        if ($filter['richdataproviderswstatus'] ?? null) {
            unset($filter['richdataproviderswstatus']);
            $fields['providersWStatus'] = " ( SELECT GROUP_CONCAT(DISTINCT CONCAT(`OJ`.`idDestination`,'||',IF(`OJ`.`dateClose` > 0 ,'1','0'))) FROM `app_OrderJobs` `OJ` WHERE `OJ`.`idOrder` = `C`.`id` AND `OJ`.`idType` = (SELECT `DidType`.`id` FROM `app_Dictionary` `DidType` WHERE `DidType`.`type` = 'orderjobtype' AND `DidType`.`code` = 'commitment' LIMIT 1) ORDER BY `OJ`.`dateClose` DESC) as `providerswstatus`";
        }
        if ($filter['connected'] ?? null) {
            unset($filter['connected']);
            $fields['connectedFrom'] = " ( SELECT GROUP_CONCAT(DISTINCT CONCAT(`OC`.`idOrderTo`,'||',COALESCE(`OC`.`note`,''))) FROM `app_OrderConnections` `OC` WHERE `OC`.`idOrderFrom` = `C`.`id`) as `connectedTo`";
            $fields['connectedTo'] = " ( SELECT GROUP_CONCAT(DISTINCT CONCAT(`OC`.`idOrderFrom`,'||',COALESCE(`OC`.`note`,''))) FROM `app_OrderConnections` `OC` WHERE `OC`.`idOrderTo` = `C`.`id`) as `connectedFrom`";
        }
        if ($filter['ordersstatus'] ?? null) {
            $fields = [" COUNT(`C`.`id`) as `tot` ", "`D`.`title` AS `status`", "`D`.`value` "];
            //$joinFilter['idStatus'] = 'LEFT JOIN `app_Dictionary` `D` ON `D`.`value` = `C`.`idStatus` AND `D`.`type` = "orderstatus"';
            $joinFilter['idStatus'] = 'LEFT JOIN `app_Dictionary` `D` ON `D`.`id` = `C`.`idStatus`';
            $groups[] = "`D`.`value`";
            unset($filter['ordersstatus']);
        }

        if ($filter['dateInsertFrom'] ?? null)
            $outFilter['dateInsertFrom'] = "`C`.`dateInsert` >= :dateInsertFrom";
        if ($filter['dateInsertTo'] ?? null)
            $outFilter['dateInsertFrom'] = "`C`.`dateInsert` >= :dateInsertTo";

        if ($filter['idGenericUser'] ?? null) {
            $outFilter['idGenericUser'] = "`C`.`idUser` = :idGenericUser";
        }
        if ($filter['indexBy'] ?? null) {
            $indexBy = $filter['indexBy'];
            unset($filter['indexBy']);
        }
        if ($filter['onlyId'] ?? null) {
            $fields = ['`C`.`id`'];
            unset($filter['onlyId']);
        }
        if ($filter['onlyCode'] ?? null) {
            $fields = ['`C`.`code`'];
            unset($filter['onlyCode']);
        }
        if ($filter['fields'] ?? null) {
            $fields = $filter['fields'];
            unset($filter['fields']);
        }

        unset($filter['richdata']);

        $queryParams = $this->S->getQueryParams($filter, $fields, $outFilter, $joinFilter, $groups);
        if (!$dato = $this->S->getRows("SELECT {$queryParams['fields']} FROM `app_Orders` `C` {$queryParams['join']} {$queryParams['outFilters']} {$queryParams['group']} {$queryParams['order']}", $queryParams['filters'], false, $indexBy))
            return [];
        return $dato;
    }

    public function getOrder(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getOrders($filter))
            return $data[0];
        return null;
    }

    public function addOrder(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Orders` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1);
    }

    public function addConnection($idOrderFrom, $idOrderTo, $note) {

        return $this->S->i("INSERT INTO `app_OrderConnections` (`idOrderFrom`,`idOrderTo`,`note`) VALUES (:idOrderFrom,:idOrderTo,:note)", ['idOrderFrom' => $idOrderFrom, 'idOrderTo' => $idOrderTo, 'note' => $note], 1, 1);
    }

    public function addOrderJob(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_OrderJobs` ($parameters[0]) VALUES ($parameters[1])", $data, 1);
    }

    public function modOrder(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModData($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $parameters['data']['id'] = $id;
        return $this->S->i("UPDATE `app_Orders` SET {$parameters['parameters']} WHERE `id` = :id", $parameters['data']);
    }

    public function getOrderJobs(array $filter): array {
        $outFilter = $joinFilter = $fields = $groups = $having = [];
        $indexBy = null;

        $fields[] = " `C`.*";
        $fields[] = " DATE_FORMAT(`C`.`dateInsert`, '%d/%m/%Y %H:%i') AS `dateInsertIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateUpdate`, '%d/%m/%Y %H:%i') AS `dateUpdateIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateDelete`, '%d/%m/%Y %H:%i') AS `dateDeleteIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateLimit`, '%d/%m/%Y %H:%i') AS `dateLimitIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateClose`, '%d/%m/%Y %H:%i') AS `dateCloseIT`";

        $fields[] = " UNIX_TIMESTAMP(`C`.`dateInsert`) AS `dateInsertTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateUpdate`) AS `dateUpdateTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateDelete`) AS `dateDeleteTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateLimit`) AS `dateLimitTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateClose`) AS `dateCloseTS`";
        /* foreach (['idOccupant', 'idCondominium', 'idUser', 'idCategory', 'idStatus', 'idPriority', 'referent', 'code', 'description', 'title', 'note'] as $f)
          if ($filter[$f] ?? null)
          $outFilter[$f] = "`C`.`$f` LIKE :$f";
          else
          unset($filter[$f]); */
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['idOrder'] ?? null)
            $outFilter['idOrder'] = "`C`.`idOrder` = :idOrder";
        if ($filter['idOrderJob'] ?? null)
            $outFilter['idOrderJob'] = "`C`.`idOrderJob` = :idOrderJob";
        if ($filter['idType'] ?? null)
            $outFilter['idType'] = "`C`.`idType` = :idType";
        if ($filter['idSender'] ?? null)
            $outFilter['idSender'] = "`C`.`idSender` = :idSender";
        if ($filter['senderRole'] ?? null)
            $outFilter['senderRole'] = "`C`.`senderRole` = :senderRole";
        if ($filter['idDestination'] ?? null)
            $outFilter['idDestination'] = "`C`.`idDestination` = :idDestination";
        if ($filter['destinationRole'] ?? null)
            $outFilter['destinationRole'] = "`C`.`destinationRole` = :destinationRole";


        if ($filter['dateInsertFrom'] ?? null)
            $outFilter['dateInsertFrom'] = "`C`.`dateInsert` >= :dateInsertFrom";
        if ($filter['dateInsertTo'] ?? null)
            $outFilter['dateInsertFrom'] = "`C`.`dateInsert` >= :dateInsertTo";

        if ($filter['idGenericUser'] ?? null) {
            $outFilter['idGenericUser'] = "(`C`.`idSender` = :idGenericUser OR `C`.`idDestination` = :idGenericUser)";
        }



        if ($filter['type'] ?? null) {
            foreach (['idType' => 'orderjobtype'] as $f => $k) {
                $fields["D{$f}t"] = " `D$f`.`title` as {$f}_title";
                $fields["D{$f}v"] = " `D$f`.`value` as {$f}_value";
                $joinFilter[$f] = 'LEFT JOIN `app_Dictionary` `D' . $f . '` ON `D' . $f . '`.`id` = `C`.`' . $f . '` ';
            }
            $having['type'] = "`idType_value` = {$filter['type']}";
            unset($filter['type']);
        }
        if ($filter['typeTitle'] ?? null) {
            foreach (['idType' => 'orderjobtype'] as $f => $k) {
                $fields["D{$f}t"] = " `D$f`.`title` as {$f}_title";
                $fields["D{$f}v"] = " `D$f`.`value` as {$f}_value";
                $joinFilter[$f] = 'LEFT JOIN `app_Dictionary` `D' . $f . '` ON `D' . $f . '`.`id` = `C`.`' . $f . '` ';
            }
            $having['type'] = "`idType_title` = '{$filter['typeTitle']}'";
            unset($filter['type']);
        }
        if ($filter['richdata'] ?? null) {
            unset($filter['richdata']);
            foreach (['idType' => 'orderjobtype'] as $f => $k) {
                $fields["D{$f}t"] = " `D$f`.`title` as {$f}_title";
                $fields["D{$f}v"] = " `D$f`.`value` as {$f}_value";
                $joinFilter[$f] = 'LEFT JOIN `app_Dictionary` `D' . $f . '` ON `D' . $f . '`.`id` = `C`.`' . $f . '` ';
            }
            $joinFilter['sender'] = 'LEFT JOIN `vks_Users` `US` ON `US`.`id` = `C`.`idSender`';
            $fields[] = " `US`.`name` as `senderName`";
            $joinFilter['destination'] = 'LEFT JOIN `vks_Users` `UD` ON `UD`.`id` = `C`.`idDestination`';
            $fields[] = " `UD`.`name` as `destinationName`";
        }

        if ($filter['idIn'] ?? null) {
            $outFilter['idIn'] = "`C`.`id` IN ({$filter['idIn']})";
            unset($filter['idIn']);
        }

        unset($filter['richdata']);
        if ($filter['indexBy'] ?? null) {
            $indexBy = $filter['indexBy'];

            unset($filter['indexBy']);
        }

        $queryParams = $this->S->getQueryParams($filter, $fields, $outFilter, $joinFilter, $groups, $having);
        // return ['filters' => $keys, 'fields' => $fields, 'outFilters' => $outFilter, 'join' => $joinFilter, 'group' => $groups];

        if (!$dato = $this->S->getRows("SELECT {$queryParams['fields']} FROM `app_OrderJobs` `C` {$queryParams['join']} {$queryParams['outFilters']} {$queryParams['order']} {$queryParams['group']} {$queryParams['having']}", $queryParams['filters'], false, $indexBy))
            return [];
        return $dato;
    }

    public function getOrderJob(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getOrderJobs($filter))
            return $data[0];
        return null;
    }

    public function modOrderJob(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModData($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $parameters['data']['id'] = $id;
        return $this->S->i("UPDATE `app_OrderJobs` SET {$parameters['parameters']} WHERE `id` = :id", $parameters['data']);
    }

    public function install(): bool {

        return parent::_install(APP . 'api/order/');
    }

    public function export(): bool {

        return parent::_export(['app_Orders' => [], 'app_OrderJobs' => [], 'app_OrderConnections' => []], APP . 'api/order/');
    }

    public function update(): bool {
        // remove it after create db
        return true;
        return parent::_update(APP . 'api/order/');
    }

}
