<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è passibile per legge.
 */

namespace api;

use \core\Files;

class Comunication extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'comunication_001', 'dipendency' => [], 'error_encript' => true, 'settings' => ['template_dir' => APP . 'data/mail_template/']];
    private $cacheDestinations = [];

    public function __construct() {
        parent::__construct();

        $this->Settings['db_fields'] = [
            'senderId' => ['need' => 1, 'option' => null, 'type' => 'int', 'method' => 'POST'],
            'senderRole' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'destinationId' => ['need' => 1, 'option' => null, 'type' => 'int', 'method' => 'POST'],
            'destinationRole' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'method' => ['need' => 1, 'option' => null, 'type' => 'int', 'method' => 'POST'],
            'object' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'body' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'dateInsert' => ['need' => 1, 'option' => null, 'type' => 'datetime', 'method' => 'POST'],
            'dateRead' => ['need' => 0, 'option' => null, 'type' => 'datetime', 'method' => 'POST'],
            'dateLastShow' => ['need' => 0, 'option' => null, 'type' => 'datetime', 'method' => 'POST'],
            'dateFirstShow' => ['need' => 0, 'option' => null, 'type' => 'datetime', 'method' => 'POST']
        ];
        $this->Settings['fields'] = [
            'category' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'name' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'mail_object' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'mail_body' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'notify' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'telegram' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'triggers' => ['need' => 0, 'option' => null, 'type' => 'string', 'method' => 'POST'],
            'roles' => ['need' => 0, 'option' => null, 'type' => 'notest', 'method' => 'POST']
        ];
        $this->Settings['fields_get'] = ['category', 'name'];
        $this->Settings['fields_add'] = ['category', 'name'];
        $this->Settings['fields_mod'] = ['category', 'name', 'mail_object', 'mail_body', 'notify', 'telegram', 'triggers', 'roles'];
        $this->Settings['senderAccount'] = ['name' => 'Condominium 1.0', 'email' => 'info@vikstech.it'];
        $this->checkStart();
    }

    public function getNotices() {
        if (!$comunications = $this->DB->getComunications(['method' => 'n', 'destinationId' => $this->Session->Acl->User['id'], 'notRead' => true]))
            return [];
        $output = [];
        foreach ($comunications as $v) {
            $output[] = ['description' => $this->replaceSpecialTag($v['body']), 'dateInsert' => $v['dateInsertIT'], 'id' => $v['id']];
        }
        return $output;
    }

    public function setNoticeAsRead($id = null) {
        if (!$id) {
            if (!$id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'POST', 'int')) ? null : $data) {
                $this->setError('fatal', 'error_data;');
                return false;
            }
        }
        if (!$notice = $this->DB->getComunication(['id' => $id])) {
            $this->setError('fatal', 'error_no_data;');
            return false;
        }
        $now = new \DateTime();

        $insertData = ['dateRead' => $now->format('Y-m-d H:i:s')];

        if (!$this->DB->modComunication($id, array_keys($insertData), $insertData)) {
            $this->setError('fatal', 'error_modify;');
            return false;
        }
        return true;
    }

    public function setNoticeAsShow($id = null) {
        if (!$id) {
            if (!$id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'POST', 'int')) ? null : $data) {
                $this->setError('fatal', 'error_data;');
                return false;
            }
        }
        if (!$notice = $this->DB->getComunication(['id' => $id])) {
            $this->setError('fatal', 'error_no_data;');
            return false;
        }
        $now = new \DateTime();

        $insertData = ['dateLastShow' => $now->format('Y-m-d H:i:s')];

        if (!$notice['dateFirstShow']) {
            $insertData['dateFirstShow'] = $insertData['dateLastShow'];
        }

        if (!$this->DB->modComunication($id, array_keys($insertData), $insertData)) {
            $this->setError('fatal', 'error_modify;');
            return false;
        }
        return true;
    }

    private function checkStart() {
        if (file_exists($this->Settings['settings']['template_dir']))
            return true;
        if (mkdir($this->Settings['settings']['template_dir'], 0777, true))
            return true;
        return false;
    }

    private function paringTelegram($data) {
        $message = explode(' ', strtolower($data['text']));
        if (count($message) != 3) {
            $messagetmp = trim(strtolower($data['text']));
            $message = [];
            $message[0] = substr($messagetmp, 0, 9);
            $message[1] = substr($messagetmp, 8, 5);
            $message[2] = substr($messagetmp, 13);
        }
        if ($message[0] != 'collega') {
            $sim = similar_text($message[0], 'collega', $perc);
            if ($sim < 5) {
                return false;
            }
        }
        $code = $message[1];
        $user = trim($message[2]);
        $this->operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
        if (!$operator = $this->operator_api->DB->getOperator(['deleted' => 0, 'withUser' => 1, 'telegramId' => $code]))
            return 'Codice errato';
        if (strtolower($operator['user']) != strtolower($user))
            return 'Utente errato';
        if (!$this->operator_api->DB->modOperator($operator['id'], ['telegramChatId'], ['telegramChatId' => $data['senderId']]))
            return 'Errore interno, riprova tra qualche minuto';
        return true;
    }

    public function manageTelegram() {
        $data = $this->Session->Dataservice->IncomingData['INPUT'];
        if (!$data)
            return;
        $data = json_decode($data, 1);
        if (json_last_error())
            return;

        if (empty($data['message']) || empty($data['message']['text']))
            return;

        $parsedData = [
            'messageId' => $data['message']['message_id'],
            'time' => $data['message']['date'],
            'senderId' => $data['message']['chat']['id'],
            'text' => $data['message']['text'],
            'type' => 'telegram'
        ];
        $returnMessage = '';
        if ($status = $this->paringTelegram($parsedData)) {
            if ($status === true)
                $returnMessage = 'Perfetto ho connesso il tuo account. Ora potrai ricevere le notifiche via Telegram.';
            else
                $returnMessage = $status;
        }

        $this->Session->Comunication->use('telegram');
        $this->Session->Comunication->send($returnMessage, $data['message']['chat']['id']);
        return;
    }

    private function getTemplateData($template, $strict = false) {
        $template = explode('.', strtolower($template));
        $file = $this->Settings['settings']['template_dir'] . implode('/', $template) . '.php';
        if (!file_exists($file)) {
            return ($strict) ? false : '';
        }
        $template = include ($file);
        return $template;
    }

    private function modTemplateData($template, $data) {
        $template = explode('.', $template);
        $file = $this->Settings['settings']['template_dir'] . implode('/', $template) . '.php';
        if (!file_exists($file))
            return false;
        return file_put_contents($file, '<?php return ' . var_export($data, 1) . '; ?>');
    }

    public function replaceTemplateKeyWithData($template, $data, $randomize = false) {
        foreach ($template as $k => $v) {
            $template[$k] = $v;
            if (in_array($k, ['mail_body', 'mail_object', 'notify', 'telegram'])) {
                $template[$k] = base64_decode($v);
                preg_match_all('/\{([a-z0-9\_]+)\}/', $template[$k], $matches);

                if ($matches) {
                    foreach ($matches[1] as $k2) {
                        if ($randomize)
                            $dato = 'DEMO_' . substr(md5(microtime()), 0, 6);
                        else
                            $dato = $data[$k2] ?? '';
                        $template[$k] = str_replace('{' . $k2 . '}', $dato, $template[$k]);
                    }
                }

                // replace VKSTAGS
                $template[$k] = $this->replaceSpecialTag($template[$k]);
            }
        }

        return $template;
    }

    private function replaceSpecialTag($str) {
        $re = '/\[([\w\d]+)\=\"([\w\d\.\\\\\/\=\:\?\-]+)\"\]([\w\d\.\\\\\/\=\-\:]+)\[\/([\w\d\.\\\\\/\=]+)\]/m';

        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
// Print the entire match result

        if (count($matches)) {
            foreach ($matches as $v) {
                $toReplace = $v[0];
                $replaceWith = '';
                $key = $v[1];
                $attr = $v[2];
                $value = $v[3];
                if ($key == 'link')
                    $replaceWith = '<a href="' . $attr . '">' . $value . '</a>';
                $str = str_replace($toReplace, $replaceWith, $str);
            }
        }
        return $str;
    }

    private function sendTelegram($body, $destinationChatId) {
        $this->Session->Comunication->use('telegram');
        $this->Session->Comunication->send($body, $destinationChatId);
    }

    private function sendNotice($body, $data) {
        $dataJson = ['id' => $data['id'], 'description' => $body, "dateInsert" => $data['dateInsert'], 'senderId' => $data['senderId'], "senderRole" => $data['senderRole'], "destinationId" => $data['destinationId'], "destinationRole" => $data['destinationRole']];
        $tokens = $this->Session->Acl->getUserLastTemporaryTokens($data['destinationId'], null/* $data['destinationRole'] */);
        if ($tokens) {
            $data = base64_encode(json_encode($dataJson));
            foreach ($tokens as $v) {
                $dataEncoded = \core\Utilitiesstring::CryptoJSAesEncrypt($v['tkn'], $data);
                $dataOut = base64_encode($dataEncoded);
                $messageToSend = ['t' => 'n', 'd' => $dataOut];
                $this->Session->Comunication->use('mqtt');
                $this->Session->Comunication->send('cond_notify', json_encode($messageToSend));
            }
        }
    }

    private function sendMail($object, $body, $destinationName, $destinationMail, $senderName, $senderMail) {
        $this->Session->Comunication->use('mail');
        $this->Session->Comunication->isSMTP();
        $this->Session->Comunication->setFrom($senderMail, $senderName);
        $this->Session->Comunication->addAddress($destinationMail, $destinationName);
        $this->Session->Comunication->addReplyTo($senderMail, $senderName);
        $this->Session->Comunication->isHTML(true);
        $this->Session->Comunication->Subject($object);
        $this->Session->Comunication->Body($body);
        $this->Session->Comunication->AltBody('Abilita la visualizzazione HTML per vedere la mail.');
        return $this->Session->Comunication->send();
    }

    private function getUserDataFromRole($idUser, $idRole) {
        if (!$this->defaults_api)
            $this->defaults_api = \core\Loader::loadDipendency('api', 'defaults', $this->Session);
        $user = null;
        if (array_key_exists($idUser . '-' . $idRole, $this->cacheDestinations))
            $user = $this->cacheDestinations[$idUser . '-' . $idRole];
        switch ($idRole) {
            case $this->defaults_api->getValue('roles', 'operator'):
            case $this->defaults_api->getValue('roles', 'admin'):
            case $this->defaults_api->getValue('roles', 'segretary'):
                if (!$user) {
                    if (!$this->operator_api)
                        $this->operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
                    if (!$user = $this->operator_api->getOperator(['idUser' => $idUser]))
                        return false;
                    $cacheDestinations[$idUser . '-' . $idRole] = $user;
                }
                return ['name' => $user['denomination'], 'email' => $user['email'], 'phone' => $user['phone'], 'method_mail' => $user['comunication'][0], 'method_telegram' => $user['comunication'][1], 'method_notice' => $user['comunication'][2], 'telegramChatId' => $user['telegramChatId']];
                break;
            case $this->defaults_api->getValue('roles', 'occupant'):
                if (!$user) {
                    if (!$this->occupant_api)
                        $this->occupant_api = \core\Loader::loadDipendency('api', 'occupant', $this->Session);
                    if (!$user = $this->occupant_api->getOccupant(['id' => $idUser]))
                        return false;
                    $cacheDestinations[$idUser . '-' . $idRole] = $user;
                }
                return ['name' => $user['denomination'], 'email' => $user['email'], 'phone' => $user['phone1'], 'method_mail' => true, 'method_telegram' => false, 'method_notice' => false, 'telegramChatId' => false];
                break;
            case $this->defaults_api->getValue('roles', 'provider'):
                if (!$user) {
                    if (!$this->provider_api)
                        $this->provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
                    if (!$user = $this->provider_api->getProvider(['idUser' => $idUser]))
                        return false;
                    $cacheDestinations[$idUser . '-' . $idRole] = $user;
                }
                return ['name' => $user['denomination'], 'email' => $user['email'], 'phone' => $user['phone1'], 'method_mail' => true, 'method_telegram' => false, 'method_notice' => false, 'telegramChatId' => false];
                break;
        }
        return false;
    }

    public function send($template, $data, $destinations, $randomContent = false) {
        if (!$templateData = $this->getTemplateData($template)) {
            return null;
        }
        // get users by role accepted by template
        if (!($templateData['roles'] ?? null))
            return null;
        $comunications = [];
        $now = new \DateTime();

        $senderData = $this->getUserDataFromRole($this->Session->Acl->User['id'], $this->Session->Acl->User['role']);
        foreach ($destinations as $idRole => $idsUser) {
            if (array_key_exists($idRole, $templateData['roles'])) {
                // get user data
                foreach ($idsUser as $idUser) {
                    if ($user = $this->getUserDataFromRole($idUser, $idRole)) {
                        $actData = array_merge($data, ['destination_name' => $user['name'], 'destination_mail' => $user['email'], 'destination_phone' => $user['phone']]);
                        $actData = array_merge($actData, ['sender_name' => $senderData['name'], 'sender_mail' => $senderData['email'], 'sender_phone' => $senderData['phone']]);
                        $tmp = [];
                        $tmp['data'] = $this->replaceTemplateKeyWithData($templateData, $actData, $randomContent);
                        $tmp['methods'] = ['mail' => $user['method_mail'], 'telegram' => $user['method_telegram'], 'notice' => $user['method_notice']];

                        if (!$tmp['data']['notify'])
                            $tmp['methods']['notice'] = false;
                        if (!$tmp['data']['telegram'] || !$user['telegramChatId'])
                            $tmp['methods']['telegram'] = false;
                        if (!$tmp['data']['mail_object'] || !$tmp['data']['mail_body'] || !$user['email'])
                            $tmp['methods']['mail'] = false;

                        $tmp['sender'] = $this->Settings['senderAccount'];
                        $tmp['destination'] = ['name' => $user['name'], 'email' => $user['email'], 'telegram' => $user['telegramChatId']];
                        $tmp['db'] = ['senderId' => $this->Session->Acl->User['id'], 'senderRole' => $this->Session->Acl->User['role'], 'destinationId' => $idUser, 'destinationRole' => $idRole, 'dateInsert' => $now->format('Y-m-d H:i:s'), 'senderName' => $this->Session->Acl->User['name']];
                        $comunications[] = $tmp;
                    }
                }
            }
        }

        foreach ($comunications as $comunication) {
            if ($comunication['methods']['mail']) {
                $this->sendMail($comunication['data']['mail_object'], $comunication['data']['mail_body'], $comunication['destination']['name'], $comunication['destination']['email'], $comunication['sender']['name'], $comunication['sender']['email']);
                $dbData = array_merge($comunication['db'], ['object' => $comunication['data']['mail_object'], 'body' => $comunication['data']['mail_body'], 'method' => 'm']);
                $this->DB->addComunication($dbData, $this->Settings['db_fields']);
            }
            if ($comunication['methods']['notice']) {
                $dbData = array_merge($comunication['db'], ['object' => '', 'body' => $comunication['data']['notify'], 'method' => 'n']);

                $idIns = $this->DB->addComunication($dbData, $this->Settings['db_fields']);
                $dbDataTmp = $dbData;
                $dbDataTmp['id'] = $idIns;
                $this->sendNotice($comunication['data']['notify'], $dbDataTmp);
            }
            if ($comunication['methods']['telegram']) {
                $this->sendTelegram($comunication['data']['telegram'], $comunication['destination']['telegram']);
                $dbData = array_merge($comunication['db'], ['object' => '', 'body' => $comunication['data']['telegram'], 'method' => 't']);
                $this->DB->addComunication($dbData, $this->Settings['db_fields']);
            }
        }
        // TODO: evaluate error reporting
        return 1;
    }

    public function sendByTrigger($trigger, $data, $destinations) {
        $templates = $this->getTemplates(true);
        $templateToSend = [];
        foreach ($templates as $v) {
            if (is_array($v) &&
                    array_key_exists('triggers', $v) &&
                    is_array($v['triggers']) &&
                    in_array($trigger, $v['triggers']))
                $templateToSend[] = $v['category'] . '.' . $v['name'];
        }

        $results = ['ok' => 0, 'ko' => 0, $total => count($templateToSend)];
        foreach ($templateToSend as $v) {
            if ($this->send($v, $data, $destinations))
                $results['ok'] ++;
            else
                $results['ko'] ++;
        }
        return $results;
    }

    public function createTemplate($template, $data) {
        $template = explode('.', $template);
        $templateDir = $template;
        unset($templateDir[count($templateDir) - 1]);
        $dir = $this->Settings['settings']['template_dir'] . implode('/', $templateDir);
        $file = $this->Settings['settings']['template_dir'] . implode('/', $template) . '.php';
        if (file_exists($file))
            return true;
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                return false;
            }
        }
        $data = '<?php return ' . var_export($data, 1) . '; ?>';
        return file_put_contents($file, $data);
    }

    public function getTemplates($completeData = false) {
        $return = [];
        if ($files = Files::getAllInDir($this->Settings['settings']['template_dir'], true)) {
            foreach ($files as $k => $v) {
                if ($v['type'] == 'file') {
                    $cat = explode('/', $v['dir']);
                    $cat = $cat[count($cat) - 1];
                    if ($completeData) {
                        $temp = include($v['src']);
                        $return[] = ['category' => $cat, 'name' => str_replace('.php', '', $v['file']), 'triggers' => explode(',', $temp['triggers']), 'roles' => $temp['roles']];
                    } else
                        $return[] = ['category' => $cat, 'name' => str_replace('.php', '', $v['file'])];
                }
            }
        }
        return $return;
    }

    public function addTemplate() {
        $collection = [];
        foreach ($this->Settings['fields_get'] as $k)
            $collection[$k] = $this->Settings['fields'][$k];
        $data = $this->Session->Dataservice->getCollection($collection, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $data['data']['category'] = trim(strtolower($data['data']['category']));
        $data['data']['name'] = trim(strtolower($data['data']['name']));
        if (count($actualTemplates = $this->getTemplates())) {
            foreach ($actualTemplates as $k => $v) {
                if ($v['category'] == $data['data']['category'] && $v['name'] == $data['data']['name']) {
                    $this->setError('fatal', 'already_exists;');
                    return false;
                }
            }
        }
        $dataToInsert = [];
        foreach ($this->Settings['fields_mod'] as $k) {
            $dataToInsert[$k] = '';
        }
        $dataToInsert['name'] = $data['data']['name'];
        $dataToInsert['category'] = $data['data']['category'];
        return $this->createTemplate($data['data']['category'] . '.' . $data['data']['name'], $dataToInsert);
    }

    public function getTemplate() {

        $collection = [];
        foreach ($this->Settings['fields_get'] as $k)
            $collection[$k] = $this->Settings['fields'][$k];
        $data = $this->Session->Dataservice->getCollection($collection, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        if (($template = $this->getTemplateData($data['data']['category'] . '.' . $data['data']['name'], true)) === false) {
            $this->setError('fatal', 'template_file_not_exists;' . $data['data']['body']);
            return false;
        }
        $roles = $this->Session->Acl->getRoles(['indexby' => 'code']) ?? [];
        $data['data']['mail_object'] = $template['mail_object'] ?? '';
        $data['data']['mail_body'] = $template['mail_body'] ?? '';
        $data['data']['telegram'] = $template['telegram'] ?? '';
        $data['data']['notify'] = $template['notify'] ?? '';
        $data['data']['triggers'] = $template['triggers'] ?? '';
        $data['data']['roles'] = $template['roles'] ?? [];
        $data['data']['roles'] = (is_array($data['data']['roles'])) ? $data['data']['roles'] : [];
        foreach ($roles as $k => $v) {
            if (!array_key_exists($k, $data['data']['roles']))
                $data['data']['roles'][$k] = ['status' => false, 'title' => $v['title']];
            else
                $data['data']['roles'][$k] = ['status' => true, 'title' => $v['title']];
        }
        return $data['data'];
    }

    public function modTemplate() {
        $collection = [];
        foreach ($this->Settings['fields_mod'] as $k)
            $collection[$k] = $this->Settings['fields'][$k];
        $data = $this->Session->Dataservice->getCollection($collection, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $roles = [];
        if ($data['data']['roles']) {
            foreach ($data['data']['roles'] as $v) {
                $roles[$v] = true;
            }
        }
        return $this->modTemplateData($data['data']['category'] . '.' . $data['data']['name'], ['mail_object' => base64_encode($data['data']['mail_object']), 'mail_body' => $data['data']['mail_body'], 'telegram' => $data['data']['telegram'], 'notify' => $data['data']['notify'], 'triggers' => $data['data']['triggers'], 'roles' => $roles]);
    }

    public function sendToMe() {
        /* $this->sendByTrigger('new.user.registered', 1, 1);

          return true; */
        $data = $this->Session->Dataservice->getCollection(['category' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST'], 'name' => ['need' => 1, 'option' => null, 'type' => 'string', 'method' => 'POST']], 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        if (!$this->send($data['data']['category'] . '.' . $data['data']['name'], $this->Session->Acl->User, [$this->Session->Acl->User['role'] => $this->Session->Acl->User['id']], true)) {
            $this->setError('fatal', 'not_send;');
            return false;
        }
        return true;
    }

}
