<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\comunication;

class Comunication extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getComunications(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $indexBy = null;
        $fields[] = " `C`.*";
        $fields[] = " DATE_FORMAT(`C`.`dateInsert`, '%d/%m/%Y %H:%i') AS `dateInsertIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateRead`, '%d/%m/%Y %H:%i') AS `dateReadIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateLastShow`, '%d/%m/%Y %H:%i') AS `dateLastShowIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateFirstShow`, '%d/%m/%Y %H:%i') AS `dateFirstShowIT`";
        /*  foreach (['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'fax2', 'email', 'pec', 'note'] as $f)
          if ($filter[$f] ?? null)
          $outFilter[$f] = "`C`.`$f` LIKE :$f";
          else
          unset($filter[$f]); */
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['idUser'] ?? null)
            $outFilter['idUser'] = "`C`.`idUser` = :idUser";
        if ($filter['telegramId'] ?? null)
            $outFilter['telegramId'] = "`C`.`telegramId` = :telegramId";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['method'] ?? null)
            $outFilter['method'] = "`C`.`method` = :method";
        if ($filter['destinationId'] ?? null)
            $outFilter['destinationId'] = "`C`.`destinationId` = :destinationId";
        if ($filter['notRead'] ?? null) {
            $outFilter['dateRead'] = "`C`.`dateRead` IS NULL";
            unset($filter['notRead']);
        }
        if ($filter['withUser'] ?? null) {
            $joinFilter[] = " JOIN `vks_Users` `U` ON `C`.`idUser` = `U`.`id`";
            $fields[] = '`U`.`user`';
            unset($filter['withUser']);
        }
        if ($filter['ids'] ?? null) {
            $outFilter['ids'] = "`C`.`id` IN ({$filter['ids']})";
            unset($filter['ids']);
        }
        if ($filter['idUsers'] ?? null) {
            $outFilter['ids'] = "`C`.`idUser` IN ({$filter['idUsers']})";
        }
        unset($filter['idUsers']);
        if ($filter['indexBy'] ?? null) {
            $indexBy = $filter['indexBy'];
            unset($filter['indexBy']);
        }

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';

        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Comunications` `C` $joinFilter $outFilter", $filter, false, $indexBy))
            return [];
        return $dato;
    }

    public function getComunication(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getComunications($filter))
            return $data[0];
        return null;
    }

    public function addComunication(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Comunications` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1);
    }

    public function modComunication(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Comunications` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        return true;

        return parent::_install(APP . 'api/comunication/');
    }

    public function export(): bool {

        return parent::_export(['app_Comunications' => []], APP . 'api/comunication/');
    }

    public function update(): bool {

        return true;
        return parent::_update(APP . 'api/comunication/');
    }

}
