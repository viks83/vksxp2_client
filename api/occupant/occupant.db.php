<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\occupant;

class Occupant extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getOccupants(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $fields[] = " `C`.*";
        foreach (['idCondominium', 'building', 'unitGroup', 'progressive', 'idFloor', 'internal', 'code', 'idBuildingType', 'idRole', 'balance', 'denomination', 'idTitleAcronim', 'address', 'cap', 'city', 'region', 'nation', 'shippingDenomination', 'shippingAddress', 'shippingCap', 'shippingCity', 'shippingRegion', 'shippingNation', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'pec', 'note'] as $f)
            if (isset($filter[$f]) && $filter[$f])
                $outFilter[$f] = "`C`.`$f` LIKE :$f";
            else
                unset($filter[$f]);
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['idCondominium'])
            $outFilter['idCondominium'] = "`C`.`idCondominium` = :idCondominium";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";


        $queryParams = $this->S->getQueryParams($filter, $fields, $outFilter, $joinFilter, $groups = [], $having = []);
        
        // return ['filters' => $keys, 'fields' => $fields, 'outFilters' => $outFilter, 'join' => $joinFilter, 'group' => $groups];

        if (!$dato = $this->S->getRows("SELECT {$queryParams['fields']} FROM `app_Occupants` `C` {$queryParams['join']} {$queryParams['outFilters']} {$queryParams['order']} {$queryParams['group']} {$queryParams['having']}", $queryParams['filters']))
            return [];
        return $dato;
        
        

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';

        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Occupants` `C` $joinFilter $outFilter", $filter))
            return [];
        return $dato;
    }

    public function getOccupant(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getOccupants($filter))
            return $data[0];
        return null;
    }

    public function addOccupant(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Occupants` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1);
    }

    public function modOccupant(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Occupants` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        return parent::_install(APP . 'api/occupant/');
    }

    public function export(): bool {
        return parent::_export(['app_Occupants' => []], APP . 'api/occupant/');
    }

    public function update(): bool {
        return true;
        return parent::_update(APP . 'api/occupant/');
    }

}
