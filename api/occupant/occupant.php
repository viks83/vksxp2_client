<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
use \core\Files;

class Occupant extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'occupant_001', 'dipendency' => ['dictionary_001'], 'error_encript' => true];
    public $importFields = [1 => 'building', 'unitGroup', 'progressive', 'idFloor', 'internal', 'code', 'idBuildingType', 'idRole', 'balance', 'denomination', 'idTitleAcronim', 'address', 'cap', 'city', 'region', 'nation', 'shippingDenomination', 'shippingAddress', 'shippingCap', 'shippingCity', 'shippingRegion', 'shippingNation', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'pec', 'sub', 'note'];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['occupant'] = [
            'fields' => [
                'idCondominium' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'building' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'unitGroup' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'progressive' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'idFloor' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'internal' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'code' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'idBuildingType' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idRole' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'balance' => ['need' => 0, 'option' => null, 'type' => 'float'],
                'denomination' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idTitleAcronim' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'address' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'cap' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'city' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'region' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'nation' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingDenomination' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingAddress' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingCap' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingCity' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingRegion' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'shippingNation' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fiscalCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'ivaCode' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone1' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone2' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'phone3' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'fax' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'email' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'pec' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'note' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'string']
            ],
            'fieldsAdd' => ['idCondominium', 'building', 'unitGroup', 'progressive', 'idFloor', 'internal', 'code', 'idBuildingType', 'idRole', 'balance', 'denomination', 'idTitleAcronim', 'address', 'cap', 'city', 'region', 'nation', 'shippingDenomination', 'shippingAddress', 'shippingCap', 'shippingCity', 'shippingRegion', 'shippingNation', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'pec', 'note']
            ,
            'fieldsMod' => ['idCondominium', 'building', 'unitGroup', 'progressive', 'idFloor', 'internal', 'code', 'idBuildingType', 'idRole', 'balance', 'denomination', 'idTitleAcronim', 'address', 'cap', 'city', 'region', 'nation', 'shippingDenomination', 'shippingAddress', 'shippingCap', 'shippingCity', 'shippingRegion', 'shippingNation', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'pec', 'note', 'dateUpdate']
            ,
            'fieldsDel' => ['dateDelete', 'deleted']
            ,
            'fieldsFilter' => ['idCondominium', 'building', 'unitGroup', 'progressive', 'idFloor', 'internal', 'code', 'idBuildingType', 'idRole', 'balance', 'denomination', 'idTitleAcronim', 'address', 'cap', 'city', 'region', 'nation', 'shippingDenomination', 'shippingAddress', 'shippingCap', 'shippingCity', 'shippingRegion', 'shippingNation', 'fiscalCode', 'ivaCode', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'pec', 'note']
        ];
    }

    public function addOccupant($data = null) {
        $fields = [];
        $kField = 'occupant';
        $logKey = 'Occupant';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        if ($data)
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        else
            $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        $forDictionary = ['idFloor' => 'floor', 'idBuildingType' => 'buildingType', 'idRole' => 'role', 'idTitleAcronim' => 'titleAcronim'];
        foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
            if (is_string($data['data'][$dictionaryKey]) && intval($data['data'][$dictionaryKey]) == 0) {
                $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);

                if (!$data['data'][$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey], 'title' => $data['data'][$dictionaryKey], 'value' => $newDictionaryValue])) {
                    $this->setError('fatal', 'error_insert_dictionary;');
                    return false;
                }
            }
        }
        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;

        if (!$id = $this->DB->addOccupant($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }
        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getOccupants() {
        $kField = 'occupant';
        $logKey = 'Occupant';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);

        $filters['data']['deleted'] = 0;
        $filters['data']['order'] = ['`C`.`denomination` ASC'];
        $data = $this->DB->getOccupants($filters['data']);
        
        return $data;
    }

    public function getOccupant(array $filter = []) {
        $kField = 'occupant';
        $logKey = 'Occupant';

        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else
            return [];
        $filters['deleted'] = 0;
        $data = $this->DB->getOccupant($filter);
        return $data;
    }

    public function modOccupant(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'occupant';
        $logKey = 'Occupant';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getOccupant(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }
        $forDictionary = ['idFloor' => 'floor', 'idBuildingType' => 'buildingType', 'idRole' => 'role', 'idTitleAcronim' => 'titleAcronim'];
        foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
            if (is_string($data['data'][$dictionaryKey]) && intval($data['data'][$dictionaryKey]) == 0) {
                $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);

                if (!$data['data'][$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey], 'title' => $data['data'][$dictionaryKey], 'value' => $newDictionaryValue])) {
                    $this->setError('fatal', 'error_insert_dictionary;');
                    return false;
                }
            }
        }
        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        if (!$this->DB->modOccupant($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getOccupant(['id' => $id]);
    }

    public function delOccupant(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'occupant';
        $logKey = 'Occupant';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getOccupant(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modOccupant($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getOccupant(['id' => $id]);
    }

    public function importOccupants() {

        $kField = 'occupant';
        $logKey = 'Occupant';
        if (!$importFile = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_file', 'FILE', 'file')) ? null : $data) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        $id = 0;
        $this->Session->Event->add("Import $logKey $id ..", 'log', 3);
        $condominium_api = \core\Loader::loadDipendency('api', 'condominium', $this->Session);
        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        $uploadTempDir = 'upload/import/tmpexp_' . time() . '/';
        if (!Files::createDir($uploadTempDir)) {
            $this->setError('fatal', "error_create_folder;");
            return false;
        }
        $uploadedCondominiumsFileName = Files::upload($importFile, ['folder' => 'upload/import/', 'staticName' => 'import_occup_' . date("d_m_Y_H_i_s", time())], ['extensionEnabled' => ['zip']]);
        if ($error = Files::getError($uploadedCondominiumsFileName)) {
            $this->setError('fatal', "error_upload;$error");
            return false;
        }

        $zip = new \ZipArchive;
        $res = $zip->open($uploadedCondominiumsFileName);
        if ($res === TRUE) {
            $zip->extractTo($uploadTempDir);
            $zip->close();
        } else {
            Files::rmdir($uploadTempDir);
            Files::rmfile($uploadedCondominiumsFileName);
            $this->setError('fatal', "error_unzip;" . $zip->getStatusString());
            return false;
        }

        if (!$files = Files::getFilesInDir($uploadTempDir)) {
            $this->setError('fatal', "error_no_files_in_zip;");
            return false;
        }
        $files_clean = [];

        $re = '/(\\[0-9a-z]{3})/mi';
        foreach ($files as $file) {
            $condominium_name = str_replace('.xls', '', basename($file));
            $condominium_name_conv = iconv('ASCII', 'UTF-8//IGNORE', $condominium_name); // preg_replace($re, '%', $condominium_name);

            if (!$cond = $condominium_api->DB->getCondominium(array('code' => "$condominium_name_conv", 'deleted' => 0))) {
                $this->setError('fatal', 'error_no_condominium_result;' . $condominium_name_conv);
                return false;
            }
            $files_clean[$condominium_name] = ['id' => $cond['id'], 'file' => $file];
        }

        $forDictionary = ['idFloor' => 'floor', 'idBuildingType' => 'buildingType', 'idRole' => 'role', 'idTitleAcronim' => 'titleAcronim'];
        $allDictionaries = $dictionary_api->DB->getDictionarys(['typeIn' => $forDictionary]);

        require_once APP . 'vendors/Excel/oleread.php';
        require_once APP . 'vendors/Excel/reader.php';

        foreach ($files_clean as $condominium_data) {
            $dataOut = $this->importOccupantsForSingleCondominium($condominium_data['file'], $forDictionary, $allDictionaries, $dictionary_api, $condominium_data['id']);
            foreach ($dataOut as $v) {
                if (!$this->DB->addOccupant($v, $this->Settings['data'][$kField]['fields'])) {
                    $this->setError('fatal', "db_error;" . print_r($v, 1));
                    return false;
                }
            }
        }

        //error_log(print_r($dataOut, 1));


        return $dataOut;
    }

    private function importOccupantsForSingleCondominium($filename, $forDictionary, $allDictionaries, $dictionary_api, $idCondominium) {
        $data = new \Spreadsheet_Excel_Reader();
        $data->read($filename);
        if (!$rows = $data->sheets[0]['numRows']) {
            $this->setError('fatal', "no_data;");
            return false;
        }

        $dataOut = [];

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $tmp = [];
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                $val = utf8_encode($data->sheets[0]['cells'][$i][$j]);
                if (array_key_exists($this->importFields[$j], $forDictionary)) {
                    $val = ($val && strlen($val)) ? $val : 'notset';
                    $dictionaryType = $forDictionary[$this->importFields[$j]];
                    if ($dictionaryVal = $dictionary_api->findInDictionary($allDictionaries, $dictionaryType, $val)) {
                        $val = $dictionaryVal['id'];
                    } else {
                        $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                        if (!$newval = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $val, 'title' => $val, 'value' => $newDictionaryValue])) {
                            $this->setError('fatal', 'error_insert_dictionary;');
                            return false;
                        }
                        $allDictionaries[$newval['id']] = ['type' => $dictionaryType, 'code' => $val, 'title' => $val, 'value' => $newDictionaryValue];
                        $val = $newval['id'];
                    }
                }
                if ($this->importFields[$j] && $this->importFields[$j] != 'sub')
                    $tmp[$this->importFields[$j]] = $val;
            }

            $tmp['idCondominium'] = $idCondominium;
            $now = new \DateTime();
            $tmp['dateInsert'] = $now->format('Y-m-d H:i:s');
            $tmp['dateUpdate'] = $now->format('Y-m-d H:i:s');
            $tmp['deleted'] = 0;


            $dataOut[] = $tmp;
        }
        return $dataOut;
    }

    public function _importOccupants() {

        $kField = 'occupant';
        $logKey = 'Occupant';
        if (!$importFile = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_file', 'FILE', 'file')) ? null : $data) {
            $this->setError('fatal', 'error_data;');
            return false;
        }

        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }

        $this->Session->Event->add("Import $logKey $id ..", 'log', 3);

        $condominium_api = \core\Loader::loadDipendency('api', 'condominium', $this->Session);
        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);

        if (!$condominium_api->DB->getCondominium(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }
        $forDictionary = ['idFloor' => 'floor', 'idBuildingType' => 'buildingType', 'idRole' => 'role', 'idTitleAcronim' => 'titleAcronim'];
        $allDictionaries = $dictionary_api->DB->getDictionarys(['typeIn' => $forDictionary]);

        $uploadedFileName = Files::upload($importFile, ['folder' => 'upload/import/', 'staticName' => 'import_occup_' . date("d_m_Y_H_i_s", time())], ['extensionEnabled' => ['xls']]);

        if ($error = Files::getError($uploadedFileName)) {
            $this->setError('fatal', "error_upload;$error");
            return false;
        }
        require_once APP . 'vendors/Excel/oleread.php';
        require_once APP . 'vendors/Excel/reader.php';
        $data = new \Spreadsheet_Excel_Reader();
        $data->read($uploadedFileName);
        if (!$rows = $data->sheets[0]['numRows']) {
            $this->setError('fatal', "no_data;");
            return false;
        }

        $dataOut = [];

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $tmp = [];
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                $val = utf8_encode($data->sheets[0]['cells'][$i][$j]);
                if (array_key_exists($this->importFields[$j], $forDictionary)) {
                    $val = ($val && strlen($val)) ? $val : 'notset';
                    $dictionaryType = $forDictionary[$this->importFields[$j]];
                    if ($dictionaryVal = $dictionary_api->findInDictionary($allDictionaries, $dictionaryType, $val)) {
                        $val = $dictionaryVal['id'];
                    } else {
                        $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                        if (!$newval = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $val, 'title' => $val, 'value' => $newDictionaryValue])) {
                            $this->setError('fatal', 'error_insert_dictionary;');
                            return false;
                        }
                        $allDictionaries[$newval['id']] = ['type' => $dictionaryType, 'code' => $val, 'title' => $val, 'value' => $newDictionaryValue];
                        $val = $newval['id'];
                    }
                }
                if ($this->importFields[$j] && $this->importFields[$j] != 'sub')
                    $tmp[$this->importFields[$j]] = $val;
            }

            $tmp['idCondominium'] = $id;
            $now = new \DateTime();
            $tmp['dateInsert'] = $now->format('Y-m-d H:i:s');
            $tmp['dateUpdate'] = $now->format('Y-m-d H:i:s');
            $tmp['deleted'] = 0;


            $dataOut[] = $tmp;
        }


        //error_log(print_r($dataOut, 1));

        foreach ($dataOut as $v) {
            if (!$this->DB->addOccupant($v, $this->Settings['data'][$kField]['fields'])) {
                $this->setError('fatal', "db_error;" . print_r($v, 1));
                return false;
            }
        }
        return $dataOut;
    }

}
