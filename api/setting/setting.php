<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;
use model\Acl\Permission;

class Setting extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'setting_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['permissions'] = [
            'fields' => [
                'rif' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'name' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'typeRif' => ['need' => 0, 'option' => null, 'type' => 'string']
            ],
            'fieldsAdd' => ['es_1', 'es_2', 'es_3']
            ,
            'fieldsMod' => ['es_1', 'es_2', 'es_3']
            ,
            'fieldsDel' => ['es_1', 'es_2', 'es_3']
            ,
            'fieldsFilter' => ['rif', 'name', 'typeRif']
        ];
    }

    public function getPermissions() {
        $kField = 'permissions';

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);


        if (!$permissions = $this->Session->Acl->getPermissions())
            return false;

        $roles = $this->Session->Acl->getRoles(['indexby' => 'code']) ?? [];

        foreach ($permissions as $k => $v) {
            foreach ($filters['data'] as $kf => $vf) {
                if ($vf) {
                    if (!\core\Utilitiesstring::selectLikeMatches($v[$kf], $vf) !== false) {
                        unset($permissions[$k]);
                        continue 2;
                    }
                }
            }

            $v['roles'] = explode(',', $v['roles']);
            $roleValues = [];
            foreach ($v['roles'] as $vr) {
                $roleValues[$vr] = $roles[$vr];
            }
        }
        return ['roles' => $roles, 'permissions' => $permissions];
    }

    public function addSetting() {
        $fields = [];
        $kField = 'setting';
        $logKey = 'Setting';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;

        if (!$id = $this->DB->addSetting($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }
        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getSettings() {
        $kField = 'setting';
        $logKey = 'Setting';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $data = $this->DB->getSettings($filters['data']);
        return $data;
    }

    public function modPermission(/* array $data */) {

        $idPermission = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('p_idPermission', 'POST', 'int')) ? null : $data;
        $idRole = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('p_idRole', 'POST', 'string')) ? null : $data;
        $status = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('p_status', 'POST', 'int')) ? null : $data;
        if (!$idRole || !$idPermission) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $kField = 'setting';
        $logKey = 'Privileges';
        $this->Session->Event->add("Mod $logKey $idPermission -> $idRole -> $status ..", 'log', 3);

        $permission = new Permission($this->Session->Acl->DB->getPermissionById($idPermission));

        if (!$permission->isValid()) {
            $this->setError('fatal', 'no_valid_permission');
            return false;
        }

        if ($status) {
            error_log('add');
            if (!$this->Session->Acl->addPermissionRole($permission, $idRole)) {
                $this->setError('fatal', 'error_add_permission_role_api;' . implode(',', $data['error']));
                return false;
            }
        } else {
            error_log('del');

            if (!$this->Session->Acl->delPermissionRole($permission, $idRole)) {
                $this->setError('fatal', 'error_del_permission_role_api;' . implode(',', $data['error']));
                return false;
            }
        }
        $this->Session->Event->add("Mod $logKey  $idPermission -> $idRole -> $status OK", 'log', 3);

        return true;
    }

    public function delSetting(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'setting';
        $logKey = 'Setting';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getSetting(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modSetting($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getSetting(['id' => $id]);
    }

}
