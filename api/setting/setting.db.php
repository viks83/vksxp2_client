<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\setting;

class Setting extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getSettings(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $fields[] = " `C`.*";
        foreach (['es_1', 'es_2', 'es_3'] as $f)
            if ($filter[$f] ?? null)
                $outFilter[$f] = "`C`.`$f` LIKE :$f";
            else
                unset($filter[$f]);
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';
        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Settings` `C` $joinFilter $outFilter", $filter))
            return [];
        return $dato;
    }

    public function getSetting(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getSettings($filter))
            return $data[0];
        return null;
    }

    public function addSetting(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Settings` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1);
    }

    public function modSetting(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Settings` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        return parent::_install(APP . 'api/setting/');
    }

    public function export(): bool {
        return true;
        return parent::_export(['app_Settings' => []], APP . 'api/setting/');
    }

    public function update(): bool {
        // remove it after create db
        return true;
        return parent::_update(APP . 'api/setting/');
    }

}
