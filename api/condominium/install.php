<?php return array (
  'app_Condominiums' => 
  array (
    'fields' => 
    array (
      0 => 
      array (
        'Field' => 'id',
        'Type' => 'int(10) unsigned',
        'Null' => 'NO',
        'Key' => 'PRI',
        'Default' => NULL,
        'Extra' => 'auto_increment',
      ),
      1 => 
      array (
        'Field' => 'denomination',
        'Type' => 'varchar(250)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      2 => 
      array (
        'Field' => 'address',
        'Type' => 'varchar(250)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      3 => 
      array (
        'Field' => 'cap',
        'Type' => 'varchar(7)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      4 => 
      array (
        'Field' => 'city',
        'Type' => 'varchar(25)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      5 => 
      array (
        'Field' => 'region',
        'Type' => 'varchar(5)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      6 => 
      array (
        'Field' => 'fiscalCode',
        'Type' => 'varchar(16)',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      7 => 
      array (
        'Field' => 'dateInsert',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      8 => 
      array (
        'Field' => 'dateUpdate',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => 'CURRENT_TIMESTAMP',
        'Extra' => '',
      ),
      9 => 
      array (
        'Field' => 'dateDelete',
        'Type' => 'timestamp',
        'Null' => 'YES',
        'Key' => '',
        'Default' => NULL,
        'Extra' => '',
      ),
      10 => 
      array (
        'Field' => 'deleted',
        'Type' => 'tinyint(4)',
        'Null' => 'NO',
        'Key' => '',
        'Default' => '0',
        'Extra' => '',
      ),
      11 => 
      array (
        'Field' => 'code',
        'Type' => 'int(11)',
        'Null' => 'YES',
        'Key' => 'UNI',
        'Default' => NULL,
        'Extra' => '',
      ),
    ),
    'indexes' => 
    array (
      0 => 
      array (
        'Table' => 'app_Condominiums',
        'Non_unique' => '0',
        'Key_name' => 'PRIMARY',
        'Seq_in_index' => '1',
        'Column_name' => 'id',
        'Collation' => 'A',
        'Cardinality' => '139',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => '',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
      1 => 
      array (
        'Table' => 'app_Condominiums',
        'Non_unique' => '0',
        'Key_name' => 'code',
        'Seq_in_index' => '1',
        'Column_name' => 'code',
        'Collation' => 'A',
        'Cardinality' => '139',
        'Sub_part' => NULL,
        'Packed' => NULL,
        'Null' => 'YES',
        'Index_type' => 'BTREE',
        'Comment' => '',
        'Index_comment' => '',
      ),
    ),
    'references' => false,
  ),
);