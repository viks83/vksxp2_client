<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è passibile per legge.
 */

namespace api;

use \core\Files;

class Condominium extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'condominium_001', 'dipendency' => [], 'error_encript' => true];
    public $importFields = [1 => "code", "denomination", "address", "cap", "city", "region", "fiscalCode"];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['condominium'] = [
            'fields' => [
                'denomination' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'address' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'cap' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'city' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'region' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'fiscalCode' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'int'],
                'code' => ['need' => 0, 'option' => null, 'type' => 'int']
            ],
            'fieldsAdd' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode']
            ,
            'fieldsMod' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'dateUpdate']
            ,
            'fieldsDel' => ['dateDelete', 'deleted']
            ,
            'fieldsFilter' => ['denomination', 'address', 'cap', 'city', 'region', 'fiscalCode', 'deleted']
        ];
    }

    public function addCondominium() {
        $fields = [];
        $kField = 'condominium';
        $logKey = 'Condominium';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;

        if (!$id = $this->DB->addCondominium($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }
        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getCondominiums() {
        $kField = 'condominium';
        $logKey = 'Condominium';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $data = $this->DB->getCondominiums($filters['data']);
        return $data;
    }

    public function getCondominium(array $filter = []) {
        $kField = 'condominium';
        $logKey = 'Condominium';

        if ($filter['id'] ?? null)
            $id = $filter['id'];
        elseif ($id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('id', 'GET', 'int')) ? null : $data)
            $filter['id'] = $id;
        else
            return [];
        $filters['deleted'] = 0;
        $data = $this->DB->getCondominium($filter);
        return $data;
    }

    public function modCondominium(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'condominium';
        $logKey = 'Condominium';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$appointment = $this->DB->getCondominium(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $data = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        if (!$this->DB->modCondominium($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getCondominium(['id' => $id]);
    }

    public function delCondominium(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'condominium';
        $logKey = 'Condominium';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$appointment = $this->DB->getCondominium(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modCondominium($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getCondominium(['id' => $id]);
    }

    public function importCondominiums() {
        if (!$importFile = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_file', 'FILE', 'file')) ? null : $data) {
            $this->setError('fatal', 'error_data;');
            return false;
        }
        $kField = 'condominium';
        $logKey = 'Condominium';
        $uploadedFileName = Files::upload($importFile, ['folder' => 'upload/import/', 'staticName' => 'import_' . date("d_m_Y_H_i_s", time())], ['extensionEnabled' => ['xls']]);

        if ($error = Files::getError($uploadedFileName)) {
            $this->setError('fatal', "error_upload;$error");
            return false;
        }
        require_once APP . 'vendors/Excel/oleread.php';
        require_once APP . 'vendors/Excel/reader.php';
        $data = new \Spreadsheet_Excel_Reader();
        $data->read($uploadedFileName);
        if (!$rows = $data->sheets[0]['numRows']) {
            $this->setError('fatal', "no_data;");
            return false;
        }

        $dataOut = [];

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            $tmp = [];
            for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                $tmp[$this->importFields[$j]] = utf8_encode($data->sheets[0]['cells'][$i][$j]);
            }
            $dataOut[] = $tmp;
        }
        foreach ($dataOut as $v) {
            if (!$this->DB->addCondominium($v, $this->Settings['data'][$kField]['fields'])) {
                $this->setError('fatal', "db_error;" . $v['cod']);
                return false;
            }
        }
        return $dataOut;
    }

}
