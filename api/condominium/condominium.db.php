<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è passibile per legge.
 */

namespace api\condominium;

class Condominium extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getCondominiums(array $filter): array {
        $outFilter = $joinFilter = $fields = [];
        $fields[] = " `C`.*";
        foreach (['denomination', 'address', 'city', 'cap', 'region', 'fiscalCode'] as $f)
            if ($filter[$f] ?? null)
                $outFilter[$f] = "`C`.`$f` LIKE :$f";
            else
                unset($filter[$f]);
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['code'] ?? null)
            $outFilter['code'] = "`C`.`code` = :code";

        $fields = (count($fields)) ? implode(', ', $fields) : '*';
        $outFilter = (count($outFilter)) ? ' WHERE ' . implode(' AND ', $outFilter) : '';
        $joinFilter = (count($joinFilter)) ? implode(' ', $joinFilter) : '';
        if (!$dato = $this->S->getRows("SELECT $fields FROM `app_Condominiums` `C` $joinFilter $outFilter", $filter))
            return [];
        return $dato;
    }

    public function getCondominium(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getCondominiums($filter))
            return $data[0];
        return null;
    }

    public function addCondominium(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        //return $this->S->i("REPLACE INTO `app_Condominiums` ($parameters[0]) VALUES ($parameters[1])", $data, 1);
        return $this->S->i("INSERT INTO `app_Condominiums` ($parameters[0]) VALUES ($parameters[1]) ON DUPLICATE KEY UPDATE $parameters[3]", $data, 1, 1);
    }

    public function modCondominium(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Condominiums` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {

        return parent::_install(APP . 'api/condominium/');
    }

    public function export(): bool {
        return parent::_export(['app_Condominiums' => []], APP . 'api/condominium/');
    }

    public function update(): bool {
        return true;

        return parent::_update(APP . 'api/condominium/');
    }

}
