<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;

class Defaults extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'defaults_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();
        $this->Settings['data']['roles'] = [
            'admin' => '001',
            'system' => '002',
            'segretary' => '003',
            'provider' => '004',
            'operator' => '005',
            'social' => '006',
            'occupant' => '060',
        ];
        $this->Settings['data']['orderJobTypes'] = [
            'commitment' => 1,
            'closejob' => 2,
            'extimate' => 3,
            'response' => 4,
            'quotation' => 5,
            'comment' => 6,
            'connection' => 7,
            'quot_accept' => 8,
            'quot_declined' => 9
        ];
    }

    public function getValue($scope, $name) {
        if (!array_key_exists($scope, $this->Settings['data']))
            return null;
        if (!array_key_exists($name, $this->Settings['data'][$scope]))
            return null;
        return $this->Settings['data'][$scope][$name];
    }

    public function getValueInverse($scope, $name) {
        if (!array_key_exists($scope, $this->Settings['data']))
            return null;
        if (!in_array($name, $this->Settings['data'][$scope]))
            return null;
        return array_search($name, $this->Settings['data'][$scope]);
    }

    public function getValues($scope) {
        if (!array_key_exists($scope, $this->Settings['data']))
            return null;
        return $this->Settings['data'][$scope];
    }

}
