<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api;

// External Library 
// use \core\Files;

class Invoice extends \core\ApiStandard {

    public $Settings = ['name' => null, 'version' => '1.0.0', 'code' => 'invoice_001', 'dipendency' => [], 'error_encript' => true];

    public function __construct() {
        parent::__construct();

        $this->Settings['data']['uploadFolder'] = 'upload/invoices/invite/';


        $this->Settings['data']['invoice'] = [
            'fields' => [
                'idOrder' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idOrderJobModule' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'idStatus' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idType' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'dateInsert' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateUpdate' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateSended' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'dateDelete' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'datePayed' => ['need' => 0, 'option' => null, 'type' => 'datetime'],
                'idDestination' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'destinationRole' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'idSender' => ['need' => 1, 'option' => null, 'type' => 'int'],
                'senderRole' => ['need' => 1, 'option' => null, 'type' => 'string'],
                'total' => ['need' => 1, 'option' => null, 'type' => 'float'],
                'description' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'extra' => ['need' => 0, 'option' => null, 'type' => 'string'],
                'deleted' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'default' => 0],
                'payed' => ['need' => 0, 'option' => null, 'type' => 'boolean', 'default' => 0]
            ],
            'fieldsAdd' => ['idOrder', 'idOrderJobModule', 'idStatus', 'idType', 'dateInsert', 'dateSended', 'idDestination', 'destinationRole', 'idSender', 'senderRole', 'total', 'description', 'extra']
            ,
            'fieldsMod' => ['es_1', 'es_2', 'es_3']
            ,
            'fieldsDel' => ['es_1', 'es_2', 'es_3']
            ,
            'fieldsFilter' => ['idOrder', 'idStatus', 'idType']
        ];
    }

    public function addInvoiceInvite($data = null) {
        if ($data) {
            $data['idType'] = 'invite';
            $data['idStatus'] = 'pending';
        }
        $this->Session->Dataservice->s('i_idType', 'invite', 'POST');
        $this->Session->Dataservice->s('i_idStatus', 'pending', 'POST');

        $idOrderJobModule = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('i_idOrderJobModule', 'POST', 'int')) ? null : $data;
        return $this->addInvoice(null, 'invite', 'pending', $idOrderJobModule);
    }

    public function addInvoice($data = null, $idType, $idStatus, $idOrderJobModule) {
        $fields = [];
        $kField = 'invoice';
        $logKey = 'Invoice';
        foreach ($this->Settings['data'][$kField]['fieldsAdd'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }
        $order_api = \core\Loader::loadDipendency('api', 'order', $this->Session);

        if (!$idOrderJobModule || !$orderJobModule = $order_api->DB->getOrderJob(['id' => $idOrderJobModule, 'deleted' => 0])) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $senderRole = $this->Session->Acl->User['role'];
        $idSender = $this->Session->Acl->User['id'];
        $destinationRole = $orderJobModule['destinationRole'];
        $idDestination = $orderJobModule['idDestination'];

        if ($data) {
            $data['destinationRole'] = $destinationRole;
            $data['idDestination'] = $idDestination;
            $data['senderRole'] = $senderRole;
            $data['idSender'] = $idSender;
            $data['idOrderJobModule'] = $idOrderJobModule;
            $data['idOrder'] = $orderJobModule['idOrder'];
            $data['idStatus'] = $idStatus;
            $data['idType'] = $idType;
            $data = $this->Session->Dataservice->getCollectionFromData($fields, $data);
        } else {
            $this->Session->Dataservice->s('i_destinationRole', $destinationRole, 'POST');
            $this->Session->Dataservice->s('i_idDestination', $idDestination, 'POST');
            $this->Session->Dataservice->s('i_senderRole', $senderRole, 'POST');
            $this->Session->Dataservice->s('i_idSender', $idSender, 'POST');
            $this->Session->Dataservice->s('i_idOrderJobModule', $idOrderJobModule, 'POST');
            $this->Session->Dataservice->s('i_idOrder', $orderJobModule['idOrder'], 'POST');
            $this->Session->Dataservice->s('i_idStatus', $idStatus, 'POST');
            $this->Session->Dataservice->s('i_idType', $idType, 'POST');
            $data = $this->Session->Dataservice->getCollection($fields, 'i_', false);
        }
        $this->Session->Event->add("Add $logKey " . print_r($data, 1) . ' ..', 'log', 3);

        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }



        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);
        $forDictionary = ['idType' => 'invoicetype', 'idStatus' => 'invoicestatus'];
        foreach ($forDictionary as $dictionaryKey => $dictionaryType) {
            if (is_string($data['data'][$dictionaryKey]) && intval($data['data'][$dictionaryKey]) == 0) {
                $newDictionaryValue = $dictionary_api->getNewValue($dictionaryType);
                if (!$data['data'][$dictionaryKey] = $dictionary_api->addDictionary(['type' => $dictionaryType, 'code' => $data['data'][$dictionaryKey], 'title' => $data['data'][$dictionaryKey], 'value' => $newDictionaryValue])) {
                    $this->setError('fatal', 'error_insert_dictionary;');
                    return false;
                }
                $data['data'][$dictionaryKey] = $data['data'][$dictionaryKey]['id'];
            }
        }

        $now = new \DateTime();
        $data['data']['dateInsert'] = $now->format('Y-m-d H:i:s');
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        $data['data']['deleted'] = 0;


        if (!$id = $this->DB->addInvoice($data['data'], $this->Settings['data'][$kField]['fields'])) {
            $this->setError('fatal', 'error_insert;');
            return false;
        }

        $this->Session->Event->add("Add $logKey OK", 'log', 3);

        return ['id' => $id];
    }

    public function getPassiveInvoices() {
        $kField = 'invoice';
        $logKey = 'Invoice';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;

        $dictionary_api = \core\Loader::loadDipendency('api', 'dictionary', $this->Session);
        $status_waitforsend = $dictionary_api->getDictionary(['type' => 'invoicestatus', 'code' => 'wait_for_send']);
        $type_waitforsend = $dictionary_api->getDictionary(['type' => 'invoicestatus', 'code' => 'invite']);


        $filters['data']['deleted'] = 0;
        $filters['data']['withDictionary'] = 1;
        $data = $this->DB->getInvoices($filters['data']);
        $return = [];
        $this->defaults_api = $this->defaults_api ?? \core\Loader::loadDipendency('api', 'defaults', $this->Session);

        foreach ($data as $invoice) {
            $tmpReturn = [
                'idOrder' => $invoice['idOrder'],
                'idOrderJobModule' => $invoice['idOrderJobModule'],
                'dateUpdateIT' => $invoice['dateUpdateIT'],
                'dateUpdateTS' => $invoice['dateUpdateTS'],
                'dateInsertIT' => $invoice['dateInsertIT'],
                'dateInsertTS' => $invoice['dateInsertTS'],
                'dateSendedIT' => $invoice['dateSendedIT'],
                'dateSendedTS' => $invoice['dateSendedTS'],
                'datePayedIT' => $invoice['datePayedIT'],
                'datePayedTS' => $invoice['datePayedTS'],
                'description' => $invoice['description'],
                'extra' => $invoice['extra'],
                'total' => $invoice['total'],
                'type' => $invoice['type'],
                'status' => $invoice['status'],
                'payed' => $invoice['payed'],
                'destinationType' => null,
                'destinationName' => null,
                'destinationId' => null,
                'senderName' => null,
                'senderId' => null,
                'uploadsData' => null];

            $tmpReturn['destinationType'] = $this->defaults_api->getValueInverse('roles', $invoice['destinationRole']);
            if (!$cacheDestinations)
                $cacheDestinations = [];
            if (array_key_exists($invoice['idDestination'] . '-' . $invoice['destinationRole'], $cacheDestinations))
                $user = $cacheDestinations[$invoice['idDestination'] . '-' . $invoice['destinationRole']];

            else {
                foreach ([['idDestination', 'destinationRole', 'destinationName', 'destinationId'], ['idSender', 'senderRole', 'senderName', 'senderId']] as $vt) {
                    $user = [];
                    switch ($invoice[$vt[1]]) {
                        case $this->defaults_api->getValue('roles', 'operator'):
                        case $this->defaults_api->getValue('roles', 'admin'):
                        case $this->defaults_api->getValue('roles', 'segretary'):
                            if (!$user) {
                                if (!$this->operator_api)
                                    $this->operator_api = \core\Loader::loadDipendency('api', 'operator', $this->Session);
                                if ($user = $this->operator_api->getOperator(['idUser' => $invoice[$vt[0]]]))
                                    $cacheDestinations[$invoice[$vt[0]] . '-' . $invoice[$vt[1]]] = $user;
                            }
                            break;
                        case $this->defaults_api->getValue('roles', 'occupant'):
                            if (!$user) {
                                if (!$this->occupant_api)
                                    $this->occupant_api = \core\Loader::loadDipendency('api', 'occupant', $this->Session);
                                if ($user = $this->occupant_api->getOccupant(['id' => $invoice[$vt[0]]]))
                                    $cacheDestinations[$invoice[$vt[0]] . '-' . $invoice[$vt[1]]] = $user;
                            }
                            break;
                        case $this->defaults_api->getValue('roles', 'provider'):
                            if (!$user) {
                                if (!$this->provider_api)
                                    $this->provider_api = \core\Loader::loadDipendency('api', 'provider', $this->Session);
                                if ($user = $this->provider_api->getProvider(['idUser' => $invoice[$vt[0]]]))
                                    $cacheDestinations[$invoice[$vt[0]] . '-' . $invoice[$vt[1]]] = $user;
                            }
                            break;
                    }
                    $tmpReturn[$vt[2]] = $user['denomination'] ?? '';
                    $tmpReturn[$vt[3]] = $invoice[$vt[0]];
                }
            }
            $return[] = $tmpReturn;
        }

        return $return;
    }

    public function getInvoices() {
        $kField = 'invoice';
        $logKey = 'Invoice';
        $fields = [];

        foreach ($this->Settings['data'][$kField]['fieldsFilter'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'GET';
        }
        $filters = $this->Session->Dataservice->getCollection($fields, 'f_', false);
        $filters['data']['deleted'] = 0;
        $filters['data']['withDictionary'] = 1;
        $data = $this->DB->getInvoices($filters['data']);
        return $data;
    }

    public function modInvoice(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'invoice';
        $logKey = 'Invoice';
        $this->Session->Event->add("Mod $logKey $id ..", 'log', 3);

        if (!$data = $this->DB->getInvoice(array('id' => $id, 'deleted' => 0))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $fields = [];
        foreach ($this->Settings['data'][$kField]['fieldsMod'] as $field) {
            $fields[$field] = $this->Settings['data'][$kField]['fields'][$field];
            $fields[$field]['method'] = 'POST';
        }

        $dataDB = $this->Session->Dataservice->getCollection($fields, 'c_', false);
        if (count($data['error'])) {
            $this->setError('fatal', 'error_data;' . implode(',', $data['error']));
            return false;
        }

        $now = new \DateTime();
        $data['data']['dateUpdate'] = $now->format('Y-m-d H:i:s');
        if (!$this->DB->modInvoice($id, $this->Settings['data'][$kField]['fieldsMod'], $data['data'])) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Mod $logKey $id OK", 'log', 3);

        return $this->DB->getInvoice(['id' => $id]);
    }

    public function delInvoice(/* array $data */) {
        $id = $this->Session->Dataservice->getError($data = $this->Session->Dataservice->g('c_id', 'POST', 'int')) ? null : $data;
        if (!$id) {
            $this->setError('fatal', 'error_id;');
            return false;
        }
        $kField = 'invoice';
        $logKey = 'Invoice';
        $this->Session->Event->add("Del $logKey $id ..", 'log', 3);

        if (!$dataDB = $this->DB->getInvoice(array('id' => $id))) {
            $this->setError('fatal', 'error_no_result;');
            return false;
        }

        $now = new \DateTime();
        $data = [];
        $data['dateDelete'] = $now->format('Y-m-d H:i:s');
        $data['deleted'] = 1;

        if (!$this->DB->modInvoice($id, $this->Settings['data'][$kField]['fieldsDel'], $data)) {
            $this->setError('fatal', 'error_modify');
            return false;
        }
        $this->Session->Event->add("Del $logKey $id OK", 'log', 3);

        return $this->DB->getInvoice(['id' => $id]);
    }

}
