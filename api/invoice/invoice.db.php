<?php

/*
 *  Copyright Vittorio Domenico Padiglia.
 *  Se non hai pagato per l'uso o la modifica di questi sorgenti, hai il dovere di cancellarli.
 *  Il possesso e l'uso, o la copia, di questo codice non consentito è punibile per legge.
 */

namespace api\invoice;

class Invoice extends \core\apiStandardDB {

    public function __construct(&$Session) {
        parent::__construct($Session);
    }

    public function getInvoices(array $filter): array {
        $outFilter = $joinFilter = $fields = $groups = $having = [];
        $fields[] = " `C`.*";
        /* foreach (['es_1', 'es_2', 'es_3'] as $f)
          if ($filter[$f] ?? null)
          $outFilter[$f] = "`C`.`$f` LIKE :$f";
          else
          unset($filter[$f]); */
        $fields[] = " DATE_FORMAT(`C`.`dateUpdate`, '%d/%m/%Y %H:%i') AS `dateUpdateIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateDelete`, '%d/%m/%Y %H:%i') AS `dateDeleteIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateInsert`, '%d/%m/%Y %H:%i') AS `dateInsertIT`";
        $fields[] = " DATE_FORMAT(`C`.`datePayed`, '%d/%m/%Y %H:%i') AS `datePayedIT`";
        $fields[] = " DATE_FORMAT(`C`.`dateSended`, '%d/%m/%Y %H:%i') AS `dateSendedIT`";

        $fields[] = " UNIX_TIMESTAMP(`C`.`dateInsert`) AS `dateInsertTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateUpdate`) AS `dateUpdateTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateDelete`) AS `dateDeleteTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`datePayed`) AS `datePayedTS`";
        $fields[] = " UNIX_TIMESTAMP(`C`.`dateSended`) AS `dateSendedTS`";
        if (isset($filter['deleted']))
            $outFilter['deleted'] = "`C`.`deleted` = :deleted";
        if ($filter['id'] ?? null)
            $outFilter['id'] = "`C`.`id` = :id";
        if ($filter['withDictionary'] ?? null) {
            $fields['idStatusV'] = " `DidStatus`.`value` as idStatus_value";
            $fields['idStatusT'] = " `DidStatus`.`title` as idStatus_title";
            $joinFilter['idStatus'] = 'LEFT JOIN `app_Dictionary` `DidStatus` ON `DidStatus`.`id` = `C`.`idStatus` ';
            $fields['idTypeV'] = " `DidType`.`value` as idType_value";
            $fields['idTypeT'] = " `DidType`.`title` as idType_title";
            $joinFilter['idType'] = 'LEFT JOIN `app_Dictionary` `DidType` ON `DidType`.`id` = `C`.`idType` ';
            unset($filter['withDictionary']);
        }

        $queryParams = $this->S->getQueryParams($filter, $fields, $outFilter, $joinFilter, $groups, $having);

        if (!$return = $this->S->getRows("SELECT {$queryParams['fields']} FROM `app_Invoices` `C` {$queryParams['join']} {$queryParams['outFilters']} {$queryParams['order']} {$queryParams['group']} {$queryParams['having']}", $queryParams['filters']))
            return [];
        return $return;
    }

    public function getInvoice(array $filter) {
        $filter['limit'] = 1;
        $filter['offset'] = 0;

        if ($data = $this->getInvoices($filter))
            return $data[0];
        return null;
    }

    public function addInvoice(array $data, array $avaiableFields) {
        if (!$parameters = $this->S->getAvaiableAddParameters($data, $avaiableFields))
            return false;
        return $this->S->i("INSERT INTO `app_Invoices` ($parameters[0]) VALUES ($parameters[1])", $data, 1);
    }

    public function modInvoice(int $id, array $avaiableFields, array $data = array()) {
        if (!$parameters = $this->S->getAvaiableModParameters($data, $avaiableFields)) {
            return false;
        }
        if (!$id) {
            return false;
        }
        $data['id'] = $id;
        return $this->S->i("UPDATE `app_Invoices` SET $parameters WHERE `id`=:id", $data);
    }

    public function install(): bool {
        // remove it after create db{
        return true;
        return parent::_install(APP . 'api/invoice/');
    }

    public function export(): bool {
        // remove it after create db
        return true;
        return parent::_export(['app_Invoices' => []], APP . 'api/invoice/');
    }

    public function update(): bool {
        // remove it after create db
        return true;
        return parent::_update(APP . 'api/invoice/');
    }

}
