<?php

/* version=(2.0.1) */
error_reporting(E_ALL ^ E_NOTICE);
session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: origin, x-requested-with, content-type, Cache-Control');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

date_default_timezone_set('Europe/Rome');
$production = true; 
//define('DEFAULT_WEB_VAR', '/var/www/');
define('APPNAME', 'CondominiumApi');
define('VKSXP', '/var/www/development/vksxp2/');
define('APP', __DIR__ . '/');
define('PROTOCOL', stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://');
define('ABSOLUTE_PATH', PROTOCOL . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/');

set_include_path(
        VKSXP . 'core/' . PATH_SEPARATOR .
        VKSXP . 'include/' . PATH_SEPARATOR .
        VKSXP . 'plugin/' . PATH_SEPARATOR .
        getcwd() . '/demons/' . PATH_SEPARATOR .
        getcwd() . '/plugin/' . PATH_SEPARATOR .
        getcwd() . 'api/'
);


try {
    include('starter.php');
    $Starter = new core\Starter();
    $Starter->boot();
} catch (Throwable $t) {
    if (!$production) {
        echo '<style>pre{
  line-height:1.2em;
  background:linear-gradient(180deg,#ccc 0,#ccc 1.2em,#eee 0);
  background-size:2.4em 2.4em;
  background-origin:content-box;
  
  /* some extra styles*/
  padding:0 20px;
  text-align:justify;
  font-family:calibri,arial,sans-serif;
}</style>';
        echo '<pre>';
        echo '<h3> THROW ' . $t->getMessage() . '</h1>';
        echo '<p>Linea ' . $t->getLine() . '</p>';
        echo '<p>File ' . $t->getFile() . '</p>';
        echo '<p>Code ' . $t->getCode() . '</p>';
        echo '<p>Trace ' . $t->getTraceAsString() . '</p>';
        echo '</pre>';
    }
} catch (Exception $e) {
    if (!$production)
        echo 'ex' . $e->getMessage();
}
