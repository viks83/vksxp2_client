<?php

return [
    'site_name' => APPNAME,
    'production' => false,
    'cores' => [
        'Output' => [
            'file' => 'output.php',
            'alias' => 'O',
            'settings' => []
        ],
        'Dataservice' => [
            'file' => 'dataservice.php',
            'alias' => 'DS',
            'settings' => [
                'xml_input' => [
                    'status' => false,
                    'put_in' => ['post']
                ],
                'encrypt' => [
                    'status' => false,
                    'call_key' => '',
                    'key' => '',
                    'v' => '',
                ],
                'b64encode' => [
                    'status' => true,
                    'call_key' => 'b',
                    'key' => 'banana',
                ]
            ]
        ],
        'Sql' => [
            'file' => 'sql.php',
            'settings' => [
                'production' => [
                    'create_db' => true,
                    'user' => '',
                    'password' => '',
                    'server' => 'localhost',
                    'port' => '3306',
                    'db_name' => '',
                ],
                'development' => [
                    'create_db' => true,
                    'user' => 'root',
                    'password' => 'root',
                    'server' => 'mariadb',
                    'port' => '3306',
                    'db_name' => 'condominium',
                ]
            ]
        ],
        'Translate' => [
            'file' => 'translate.php',
            'alias' => 'T',
            'settings' => [
                'cache_time' => 360,
                'language' => 'IT',
                'languages' => array('IT', 'DE', 'EN', 'ES', 'FR', 'RU'),
            ]
        ],
        'Acl' => [
            'file' => 'acl.php',
            'alias' => 'A',
            'settings' => [
                'max_session_time' => 9200,
                'logout_key' => 'exit',
                'preload_permission' => true,
                'only_logged_users' => true,
                'vars' => [
                    'get_token' => 'gettkn'
                ],
            ]
        ],
        'Event' => [
            'file' => 'event.php',
            'alias' => 'E',
            'settings' => [
                'send_to_console' => ['priority' => [1, 2, 3], 'events' => []],
                'console_url' => '37.187.237.237',
                'console_port' => '1883'
            ]
        ],
        'Installer' => [
            'file' => 'installer.php',
            'alias' => 'I',
            'settings' => [
                'auto_update' => true,
                'install_keys' => ['api' => 'install_api', 'plugin' => 'install_plugin'],
                'update_keys' => ['api' => 'update_api', 'plugin' => 'update_plugin'],
                'export_keys' => ['api' => 'export_api', 'plugin' => 'export_plugin', 'force_not_installed' => 'force']
            ]
        ],
        'Comunication' => [
            'file' => 'comunication.php',
            'alias' => 'C',
            'settings' => [
                'activeMethods' => [
                    'mail' => true,
                    'mqtt' => true,
                    'telegram' => true,
                    'facebook' => false,
                    'twitter' => false,
                ],
                'mail' => [
                    'isSMTP' => true,
                    'SMTPDebug ' => 4,
                    'Host' => 'in-v3.mailjet.com',
                    'SMTPAuth' => true,
                    'Username' => '2998d697ee8bb893c400b9a7b2316eaf', //'a23d237663bb4d0f568a4513824f3aba',
                    'Password' => '90c72f20392768d5b16fac182f15898a', //'b2145bddccdf2bde8961ce643e5e756b',
                    'Debugoutput' => 'error_log',
                    //'SMTPSecure' => 'tls',
                    //'Port' => 587,
                    'Port' => 25,
                ],
                'mqtt' => [
                    //'host' => '37.187.237.237',
                    'host' => 'signal.allerta-mi.it',
                    'port' => 2884,
                    'user' => 'appuser',
                    'password' => '123456789',
                    'clientName' => 'srvCondominium',
                    'topic' => 'condominium',
                ],
                'telegram' => [
                    'botName' => 'CondominiumReccoBot',
                    'apiKey' => '627068940:AAEla99frQmntnUDab-NX3JCW5fT1zzCDVg'
                ]
            ]
        ],
    ],
];
