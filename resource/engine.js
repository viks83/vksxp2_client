
function login() {
    var user = $('input[name=username]').val();
    var password = $('input[name=password]').val();
    return $.Engine.user.login(user, password);
}
$.Engine = {};

$.Engine.messages = {
    popup: function (title, description, type) {
        var data = {
            title: title,
            type: type,
            closeOnConfirm: true,
            confirmButtonText: "Ok",
        };
        if (description)
            data.text = description;
        swal(data);
    },
    confirm: function (title, description, type, callbacktrue) {
        title = (title) ? title : 'Sei sicuro?';
        description = (description) ? description : 'Non potrai tornare indietro!';
        type = (type) ? type : 'warning';
        swal({
            title: title,
            text: description,
            type: type,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continua!'
        }, function () {
            if (callbacktrue)
                callbacktrue();
        });
    }
}
$.Engine.data = {
    endPoint: 'http://api.hospital.vksxp.com/index.php',
    get: function (data, callback, method) {
        method = (method) ? method : 'GET';
        var url = $.Engine.data.endPoint;
        if ($.Engine.user.getTkn((data.hasOwnProperty('gettkn'))) && !data.hasOwnProperty('user'))
        {
            data.tkn = $.Engine.user.getTkn();
            url = $.Engine.data.endPoint + '?tkn=' + $.Engine.user.getTkn();
        }
        $.ajax({
            type: method,
            url: url,
            data: data,
            success: function (resp) {
                resp = (resp) ? JSON.parse(resp) : {};
                // console.log(resp);

                if (resp.hasOwnProperty('em') && resp.em == 'no logged' && $.Engine.routes.actual != 'login')
                    setTimeout(function () {
                        $.Engine.routes.go('login');
                    }, 1000);
                callback(resp);
            },
        });
    },
    send: function (w) {
        var form = $('#' + w);
        var data = form.serialize();
        //    console.log(data);
        var callback = form.attr('data-vks-callback');
        if (callback == 'default')
            callback = $.Engine.data.response;
        data.tkn = $.Engine.user.getTkn();
        $.ajax({
            type: "POST",
            url: $.Engine.data.endPoint + '?ref=' + $(form).attr('data-vks-ref') + '&name=' + $(form).attr('data-vks-name') + '&tkn=' + $.Engine.user.getTkn(),
            data: data,
            success: function (resp) {
                resp = (resp) ? JSON.parse(resp) : {};
                if (resp.hasOwnProperty('em') && resp.em == 'no logged' && $.Engine.routes.actual != 'login')
                    setTimeout(function () {
                        $.Engine.routes.go('login');
                    }, 1000);
                callback(resp, w);
            },
        });
    },
    response: function (resp, w) {
        if (resp.e || !resp.return) {
            $.Engine.messages.popup('Errore', resp.em, 'warning');
            return;
        }
        if (!$.Engine.data.temporalyMute)
            $.Engine.messages.popup('Perfetto', 'Operazione completata', 'success');
        $.Engine.data.temporalyMute = false;
        var postinsert = $('#' + w).find('#return');
        /*if (postinsert && (postinsert = $(postinsert).val()) != '0') {
         if (postinsert.indexOf(',') >= 0) {
         var args = postinsert.split(',');
         console.log(args.length)
         if (resp.return.hasOwnProperty(args[1]))
         $.Engine.routes.go(args[0], resp.return[args[1]]);
         else
         $.Engine.routes.go(args[0], args[1]);
         } else
         $.Engine.routes.go(postinsert);
         
         }*/
        if (postinsert && (postinsert = $(postinsert).val()) != '0') {
            if (postinsert.indexOf(',') >= 0) {
                var args = postinsert.split(',');
                var urlplus = '';
                for (a in args) {
                    if (a == 0)
                        continue;
                    if (a == 2) {
                        urlplus += '_';
                    } else if (a > 2) {
                        urlplus += '--';
                    }
                    var dato = (resp.return.hasOwnProperty(args[a])) ? resp.return[args[a]] : args[a];
                    urlplus += dato;
                }
                $.Engine.routes.go(args[0], urlplus);
            } else
                $.Engine.routes.go(postinsert);
        }
        if (w.indexOf('mod') !== 0)
            $('#' + w).trigger("reset");
    }
}

$.Engine.routes = {
    actual: null,
    routes: {
        'login': 'login.html',
        'home': 'index.html',
        'patients': 'patients.html',
        'addpatient': 'addpatient.html',
        'addpatientall': 'addpatientall.html',
        'modpatient': 'modpatient.html',
        'getpatient': 'getpatient.html',
        'addappointment': 'addappointment.html',
        'addappointmentnop': 'addappointmentnop.html',
        'addappointmentall': 'addappointmentall.html',
        'modappointment': 'modappointment.html',
        'appointments': 'appointments.html',
        'appointment': 'appointment.html',
        'reports': 'reports.html',
        'billings': 'billings.html',
        'modbilling': 'modbilling.html',
    },
    go: function (w, data) {
        if (data)
        {
            var type = typeof data;
            if (!(type == 'string' || type == 'number'))
                data = data.join('_');
            else
                data = '_' + data;
        } else
            data = '';
        location.href = $.Engine.routes.routes[w] + data;
    },
    prev: function () {
        location.href = document.referrer;
    },
    refresh: function () {
        location.reload();
    },
    init: function () {
        var m = location.href.toString().match(/.*\/(.+?)\./);
        $.Engine.routes.actual = 'home';
        if (m && m.length > 1)
            $.Engine.routes.actual = m[1];
        if ($.Engine.routes.actual != 'login' && !$.Engine.user.isLogged())
            return $.Engine.routes.go('login');
        $.Engine.data.get({"ref": 'tokens', "name": "getPreferences"}, $.Engine.routes.callback.preferences);
        $.Engine.routes.elaborateView();
    },
    callback: {
        preferences: function (resp) {
            $("body").removeClass(function (index, className) {
                return (className.match(/(^|\s)theme-\S+/g) || []).join(' ');
            });
            $('body').addClass(resp.return.theme);
        }
    },
    elaborateView: function () {
        var data = $.find('*[data-vks-place]');
        $.each(data, function (i, el) {
            var xx = $(el).attr('data-vks-place').split('.');
            var out = $.Engine;
            for (a in xx)
            {
                if (xx[a])
                    out = out[xx[a]];
                else
                    out = 'NONE';
            }
            $(el).html(out);
            $(el).removeAttr('data-vks-place');
        });
        var data = $.find('*[data-vks-loop]');
        $.each(data, function (i, el) {
            var xx = $(el).attr('data-vks-loop').split('.');
            var out = $.Engine;
            var tmpcallback = false;
            if ($(el).attr('data-vks-call-callback')) {
                tmpcallback = $(el).attr('data-vks-call-callback');
                $(el).removeAttr('data-vks-call-callback');
            }
            if (xx[0] == 'api') {
                var putdata = {"ref": xx[2], "name": xx[3]};
                if (xx[4]) {
                    putdata.id = xx[4];
                }

                $.Engine.data[xx[1]](putdata, function (resp) {
                    $.Engine.routes.loop(resp, $(el));
                    if (tmpcallback) {
                        eval(tmpcallback)(resp);
                    }
                });
            }
            $(el).removeAttr('data-vks-loop');
        });
        var data = $.find('*[data-vks-call]');
        $.each(data, function (i, el) {
            var xx = $(el).attr('data-vks-call').split('.');
            var tmpcallback = false;
            if ($(el).attr('data-vks-call-callback')) {
                tmpcallback = $(el).attr('data-vks-call-callback');
                $(el).removeAttr('data-vks-call-callback');
            }

            var out = $.Engine;
            var name = xx[0];

            if (xx[1] == 'api') {
                $.Engine.data[xx[2]]({"ref": xx[3], "name": xx[4], "id": xx[5]}, function (resp) {
                    $.Engine.routes.bind(resp, name);
                    if (tmpcallback) {
                        eval(tmpcallback)(resp, name);
                    }
                });
            }
            $(el).removeAttr('data-vks-call');
        });
        $('button[type=cancel]').on('click', function (e) {
            var form = $(this).parents('form');
            var id = $(this).parents('form').attr('id');
            if (id.indexOf('mod') === 0) {
                $.Engine.routes.prev();
            } else
                form.trigger("reset");
            e.preventDefault();
        });
    },
    loop: function (data, dom_el) {
        var template = $(dom_el).parent().html().toString();
        var parent = $(dom_el).parent();
        parent.html('');
        if (data.return) {
            $.each(data.return, function (i, el) {
                var tmp_template = template.toString();
                $.each(el, function (field, value) {
                    var pattern = "\{" + field + "\}";
                    re = new RegExp(pattern, "g");
                    tmp_template = tmp_template.replace(re, value);
                })
                $(parent).append(tmp_template);
            });
        }
    },
    bind: function (data, name) {
        //console.log(data);
        if (!data.return)
            data.return = {};
        var elements = $.find('*[data-vks-bind-call=' + name + ']');
        $.each(elements, function (i, el) {
            var elname = $(el).attr('data-vks-bind-value');
            if (data.return.hasOwnProperty(elname))
                var out = data.return[elname];
            else
                var out = '';
            if (out == null || out == 'null')
                out = '';
            var format = ($(el).attr('data-vks-bind-value-format')) ? $(el).attr('data-vks-bind-value-format') : '{data}';
            var out = format.replace('{data}', out);
            if ($(el).attr('data-vks-bind-value-attr'))
            {
                var att = $(el).attr('data-vks-bind-value-attr');
                $(el).attr(att, out);
                return;
            }

            var tag = $(el).prop("tagName");
            switch (tag.toLowerCase()) {
                case 'input':
                    if ($(el).attr('type') == 'checkbox') {
                        $(el).prop('checked', ((parseInt(out) == 1) ? true : false));
                        break;
                    }
                case 'select':
                case 'textarea':
                    $(el).val(out);
                    break;
                case 'holder':
                    $(el).replaceWith(out);
                    break;
                default:
                    $(el).html(out);
            }
        });

    }

}
$.Engine.user = {
    logoutTimer: null,
    data: {},
    isLogged: function (mute) {
        var now = new Date();
        if ($.Engine.user.data && $.Engine.user.data.tkn) {
            if ($.Engine.user.data.expireDate > (now.getTime() / 1000))
            {
                if ($.Engine.user.logoutTimer == null) {
                    $.Engine.user.logoutTimer = setTimeout(function () {
                        clearTimeout($.Engine.user.logoutTimer);
                        $.Engine.user.logoutTimer = null;
                        $.Engine.user.isLogged();
                    }, 20000);
                }
                return true;
            } else if (!mute)
                $.Engine.messages.popup('Sessione Scaduta', null, 'warning');
        }
        $.Engine.user.data = {};
        return false;
    },
    getTkn: function (mute) {
        if (!$.Engine.user.isLogged(mute))
            return false;
        return $.Engine.user.data.tkn;
    },
    login: function (user, password) {
        $.Engine.data.get({"user": user, "password": password, "gettkn": true}, $.Engine.user.callback.checkLogin);
    },
    logout: function () {
        $.Engine.user.data = {};
        $.Engine.user.save();
        $.Engine.data.get({"logout": "exit"}, $.Engine.routes.init);
    },
    callback: {
        checkLogin: function (resp) {
            if (resp.e) {
                $.Engine.user.data = {};
                $.Engine.user.save();
                $.Engine.messages.popup('Errore Login', resp.em, 'warning');
                return;
            }
            $.Engine.user.data = resp.return;
            $.Engine.user.save();
            $.Engine.routes.go('home');
        }
    },
    save: function () {
        $.Engine.storage.set('user', $.Engine.user.data);
    },
    init: function () {
        var user = $.Engine.storage.get('user');
        if (user)
        {
            $.Engine.user.data = user;
            if ($.Engine.user.data.hasOwnProperty('name'))
                if (['a', 'e'].indexOf($.Engine.user.data.name[$.Engine.user.data.name.length - 1]) >= 0)
                {
                    $('.admin-action-info span').text('Benvenuta');
                    $('.wellcome').text('Bentornata');
                }
        }
    }
};
$.Engine.storage = {
    storage: window.localStorage,
    get: function (w) {
        var data = $.Engine.storage.storage.getItem(w);
        if (data && data.indexOf('{') >= 0)
            return JSON.parse(data);
        return data;
    },
    set: function (w, data) {
        if (typeof data === 'object')
            data = JSON.stringify(data);
        $.Engine.storage.storage.setItem(w, data);
    },
    clear: function (w) {
        $.Engine.storage.storage.removeItem(w);
    }

};

    changeHospitalDayStatus: function (el) {
        $(el).toggleClass('selected');
    },
    setHospitalHours: function () {
        var datas = {"ref": 'settings', "name": "setHospitalHours"};
        datas.i_open_am = $('#i_open_am').val();
        datas.i_close_am = $('#i_close_am').val();
        datas.i_open_pm = $('#i_open_pm').val();
        datas.i_close_pm = $('#i_close_pm').val();
        datas.i_min_duration = $('#i_min_duration').val();
        datas.i_min_appointment_duration = $('#i_min_appointment_duration').val();
        var days = $.find('.daycheckable');
        $.each(days, function (i, el) {
            datas['i_' + $(el).text()] = (($(el).hasClass('selected')) ? 'true' : 'false');
        });
        $.Engine.data.get(datas, function (resp) {
            if (resp.return)
            {
                $.Engine.messages.popup('Perfetto', 'Modifica apportata.', 'success');
            } else
            {
                $.Engine.messages.popup('Errore', 'Si è verificato un errore.', 'warning');
            }
        }, 'POST');
    },
    check: function (resp) {
        if (!resp.return || !resp.return.hasOwnProperty('googleStatus'))
            return;
        if (!resp.return.googleStatus) {
            $('googlestatus').replaceWith('<a href="' + resp.return.googleLoginLink + '">Login Google</a>');
        } else {
            $('googlestatus').replaceWith('Connesso - <a href="javascript: settings.logoutGoogle()">LogOut</a>');
        }
        $('#googleCalendarStatus').prop('checked', resp.return.googleCalendarStatus);
        $('#autoLogOff').prop('checked', (resp.return.autoLogOff.value == 'true') ? true : false);
        if (resp.return.autoLogOff.value == 'true') {

            settings.autoLogOff();
        }
        settings.loadHospitalHours(resp.return.hospitalHours);


    },
    loadHospitalHours: function (data) {

        settings.getHospitalHours = {businessHours: [], start: '8:00', end: '20:00', duration: '30', min_appointment_duration: '60'};

        var dow = [];

        /* [
         {
         start: '8:00',
         end: '10:00',
         dow: [1, 2, 3, 4, 5]
         },
         {
         start: '12:00',
         end: '14:00',
         dow: [1, 2, 3, 4, 5]
         },
         ],*/
        $('#i_open_am').val(data.open_am);
        $('#i_close_am').val(data.close_am);
        $('#i_open_pm').val(data.open_pm);
        $('#i_close_pm').val(data.close_pm);
        $('#i_min_duration').val(data.min_duration);
        $('#i_min_appointment_duration').val(data.min_appointment_duration);
        var days = $.find('.daycheckable');
        $.each(days, function (i, el) {
            if (data[$(el).text()])
            {
                $(el).addClass('selected');
                dow.push(parseInt($(el).attr('data-value')))
            } else
                $(el).removeClass('selected');
        });
        var start = false, end = false;
        if (data.open_am && data.close_am) {
            start = data.open_am;
            end = data.close_am;
            settings.getHospitalHours.businessHours.push({start: data.open_am, end: data.close_am, dow: dow})
        }
        if (data.open_pm && data.close_pm) {
            if (!start)
                start = data.open_pm;
            end = data.close_pm;
            settings.getHospitalHours.businessHours.push({start: data.open_pm, end: data.close_pm, dow: dow})
        }
        settings.getHospitalHours.start = start;
        settings.getHospitalHours.end = end;
        settings.getHospitalHours.slots = parseInt(data.slots);
        settings.getHospitalHours.min_appointment_duration = parseInt(data.min_appointment_duration);

        var a = moment('01-01-2000 00:00:00', 'DD-MM-YYYY HH:mm:ss')
        a.add(parseInt(data.min_duration), 'm');
        settings.getHospitalHours.duration = a.format('HH:mm:ss');



    },
    getHospitalHours: null,
    temporaryDisableAutoLogOff: function () {
        $.Engine.storage.clear('logofftimer')
        clearTimeout(settings.autoLogOffTimeout);
        $('#timeoutlogin').hide();

    },
    autoLogOff: function () {
        settings.logOffTimer = $.Engine.storage.get('logofftimer');
        if (!settings.logOffTimer)
        {
            settings.logOffTimer = moment();
            settings.logOffTimer.add('25', 'm');
            $.Engine.storage.set('logofftimer', settings.logOffTimer.format('YYYY-MM-DDTHH:mm:ss.sss'));
        } else {
            var logOffTimer = moment(settings.logOffTimer, 'YYYY-MM-DDTHH:mm:ss.sss');
            var now = moment();
            var diff = moment.duration(now.diff(logOffTimer));
            if (-diff.get('minute') < 1 && -diff.get('seconds') < 30) {
                $('#timeoutlogin').show();
                $('#timeoutlogin').find('em').html(' ' + -diff.get('seconds') + ' sec al log-off');

            } else {
                $('#timeoutlogin').hide();
            }
            if (diff.get('milliseconds') > 1) {
                $.Engine.storage.clear('logofftimer')
                return $.Engine.user.logout();
            }
        }
        settings.autoLogOffTimeout = setTimeout(settings.autoLogOff, 1000);
    },
    logoutGoogle: function () {
        $.Engine.data.get({"ref": 'settings', "name": "logoutGoogle"}, function (resp) {
            if (resp.return)
                $.Engine.messages.popup('Perfetto', 'Modifica apportata.', 'success');
            else
                $.Engine.messages.popup('Errore', 'Si è verificato un errore.', 'warning');
        });
    },
    changeCalendarStore: function (el) {
        var status = $(el).is(':checked');
        $.Engine.data.get({"ref": 'settings', "name": "setGoogleSettings", 's_calendarStatus': status}, function (resp) {
            if (resp.return)
            {
                $.Engine.messages.popup('Perfetto', 'Modifica apportata.', 'success');
                $(el).prop('checked', status);
            } else
            {
                $.Engine.messages.popup('Errore', 'Si è verificato un errore.', 'warning');
                $(el).prop('checked', ((status) ? false : true));

            }
        });
    },
    changeAutoLogOff: function (el) {
        var status = $(el).is(':checked');
        if (!status) {
            clearTimeout(settings.autoLogOffTimeout);
        }
        $.Engine.data.get({"ref": 'settings', "name": "setLogOut", 's_autoLogOff': status}, function (resp) {
            if (resp.return)
            {
                $.Engine.messages.popup('Perfetto', 'Modifica apportata.', 'success');
                $(el).prop('checked', status);
            } else
            {
                $.Engine.messages.popup('Errore', 'Si è verificato un errore.', 'warning');
                $(el).prop('checked', ((status) ? false : true));

            }
        });
    },
    toggleSettings: function (id) {
        if ($('#' + id).is(':visible')) {
            $('#' + id).hide();
        } else
            $('#' + id).show();

    }
}

var utility = {
    toggle: function (id) {
        if ($('#' + id).is(":visible"))
            $('#' + id).hide();
        else
            $('#' + id).show();
    },
    clone: function (obj) {
        if (null == obj || "object" != typeof obj)
            return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
                copy[attr] = obj[attr];
        }
        return copy;
    },
    capitalize: function (el) {
        jQuery(document).ready(function ($) {
            $(el).change(function (event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });
        });

    },
	zerofilled: function(number, width) {
		width -= number.toString().length;
		if (width > 0)
			return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
		return number + "";
	}
}